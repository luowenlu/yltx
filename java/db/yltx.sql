/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 80032
Source Host           : localhost:3306
Source Database       : yltx

Target Server Type    : MYSQL
Target Server Version : 80032
File Encoding         : 65001

Date: 2023-04-16 09:13:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint NOT NULL,
  `SCHED_TIME` bigint NOT NULL,
  `PRIORITY` int NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', null, 'io.renren.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000187191E9F507874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672656E72656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint NOT NULL,
  `CHECKIN_INTERVAL` bigint NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RenrenScheduler', 'DESKTOP-CLHEIK91681570591223', '1681570834435', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint NOT NULL,
  `REPEAT_INTERVAL` bigint NOT NULL,
  `TIMES_TRIGGERED` bigint NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int DEFAULT NULL,
  `INT_PROP_2` int DEFAULT NULL,
  `LONG_PROP_1` bigint DEFAULT NULL,
  `LONG_PROP_2` bigint DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint DEFAULT NULL,
  `PREV_FIRE_TIME` bigint DEFAULT NULL,
  `PRIORITY` int DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint NOT NULL,
  `END_TIME` bigint DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', null, '1681572600000', '1681570800000', '5', 'WAITING', 'CRON', '1679753960000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000187191E9F507874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672656E72656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES ('1', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('2', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('3', '1', 'testTask', 'renren', '0', null, '0', '2023-03-26 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('4', '1', 'testTask', 'renren', '0', null, '2', '2023-03-26 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('5', '1', 'testTask', 'renren', '0', null, '2', '2023-03-26 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('6', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('7', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('8', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('9', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('10', '1', 'testTask', 'renren', '0', null, '2', '2023-03-26 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('11', '1', 'testTask', 'renren', '0', null, '4', '2023-03-26 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('12', '1', 'testTask', 'renren', '0', null, '4', '2023-03-26 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('13', '1', 'testTask', 'renren', '0', null, '3', '2023-03-26 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('14', '1', 'testTask', 'renren', '0', null, '2', '2023-03-26 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('15', '1', 'testTask', 'renren', '0', null, '2', '2023-03-26 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('16', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('17', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('18', '1', 'testTask', 'renren', '0', null, '1', '2023-03-26 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('19', '1', 'testTask', 'renren', '0', null, '1', '2023-04-01 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('20', '1', 'testTask', 'renren', '0', null, '2', '2023-04-01 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('21', '1', 'testTask', 'renren', '0', null, '1', '2023-04-01 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('22', '1', 'testTask', 'renren', '0', null, '2', '2023-04-01 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('23', '1', 'testTask', 'renren', '0', null, '1', '2023-04-01 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('24', '1', 'testTask', 'renren', '0', null, '3', '2023-04-01 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('25', '1', 'testTask', 'renren', '0', null, '1', '2023-04-01 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('26', '1', 'testTask', 'renren', '0', null, '1', '2023-04-08 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('27', '1', 'testTask', 'renren', '0', null, '1', '2023-04-08 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('28', '1', 'testTask', 'renren', '0', null, '4', '2023-04-08 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('29', '1', 'testTask', 'renren', '0', null, '4', '2023-04-08 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('30', '1', 'testTask', 'renren', '0', null, '2', '2023-04-15 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('31', '1', 'testTask', 'renren', '0', null, '1', '2023-04-15 18:30:00');
INSERT INTO `schedule_job_log` VALUES ('32', '1', 'testTask', 'renren', '0', null, '1', '2023-04-15 19:00:00');
INSERT INTO `schedule_job_log` VALUES ('33', '1', 'testTask', 'renren', '0', null, '1', '2023-04-15 19:30:00');
INSERT INTO `schedule_job_log` VALUES ('34', '1', 'testTask', 'renren', '0', null, '1', '2023-04-15 20:30:00');
INSERT INTO `schedule_job_log` VALUES ('35', '1', 'testTask', 'renren', '0', null, '0', '2023-04-15 21:00:00');
INSERT INTO `schedule_job_log` VALUES ('36', '1', 'testTask', 'renren', '0', null, '2', '2023-04-15 22:00:00');
INSERT INTO `schedule_job_log` VALUES ('37', '1', 'testTask', 'renren', '0', null, '1', '2023-04-15 23:00:00');

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统验证码';

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":31,\"parentId\":0,\"name\":\"财务系统\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"shouye\",\"orderNum\":0,\"list\":[]}]', '12', '0:0:0:0:0:0:0:1', '2023-03-26 07:57:32');
INSERT INTO `sys_log` VALUES ('2', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":32,\"parentId\":31,\"name\":\"发货表\",\"url\":\"cw/invoice\",\"type\":1,\"icon\":\"config\",\"orderNum\":6,\"list\":[]}]', '15', '0:0:0:0:0:0:0:1', '2023-03-26 08:03:27');
INSERT INTO `sys_log` VALUES ('3', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":42,\"parentId\":31,\"name\":\"数据字典\",\"url\":\"cw/dictionary\",\"type\":1,\"icon\":\"config\",\"orderNum\":6,\"list\":[]}]', '15', '0:0:0:0:0:0:0:1', '2023-04-08 14:08:54');
INSERT INTO `sys_log` VALUES ('4', 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":52,\"parentId\":31,\"name\":\"订单表\",\"url\":\"cw/salesinvoice\",\"type\":1,\"icon\":\"config\",\"orderNum\":6,\"list\":[]}]', '18', '0:0:0:0:0:0:0:1', '2023-04-08 14:09:20');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` bigint DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'system', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '管理员列表', 'sys/user', null, '1', 'admin', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'sys/role', null, '1', 'role', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'sys/menu', null, '1', 'menu', '3');
INSERT INTO `sys_menu` VALUES ('5', '1', 'SQL监控', 'http://localhost:8080/renren-fast/druid/sql.html', null, '1', 'sql', '4');
INSERT INTO `sys_menu` VALUES ('6', '1', '定时任务', 'job/schedule', null, '1', 'job', '5');
INSERT INTO `sys_menu` VALUES ('7', '6', '查看', null, 'sys:schedule:list,sys:schedule:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '6', '新增', null, 'sys:schedule:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('9', '6', '修改', null, 'sys:schedule:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '6', '删除', null, 'sys:schedule:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '6', '暂停', null, 'sys:schedule:pause', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '6', '恢复', null, 'sys:schedule:resume', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '6', '立即执行', null, 'sys:schedule:run', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '6', '日志列表', null, 'sys:schedule:log', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', 'sys/log', 'sys:log:list', '1', 'log', '7');
INSERT INTO `sys_menu` VALUES ('30', '1', '文件上传', 'oss/oss', 'sys:oss:all', '1', 'oss', '6');
INSERT INTO `sys_menu` VALUES ('31', '0', '财务系统', '', '', '0', 'shouye', '0');
INSERT INTO `sys_menu` VALUES ('32', '31', '发货表', 'cw/invoice', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('33', '32', '查看', null, 'cw:invoice:list,cw:invoice:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('34', '32', '新增', null, 'cw:invoice:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('35', '32', '修改', null, 'cw:invoice:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('36', '32', '删除', null, 'cw:invoice:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('37', '31', '发货回款记录表', 'cw/deliverypaymentrecord', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('38', '37', '查看', null, 'cw:deliverypaymentrecord:list,cw:deliverypaymentrecord:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('39', '37', '新增', null, 'cw:deliverypaymentrecord:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('40', '37', '修改', null, 'cw:deliverypaymentrecord:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('41', '37', '删除', null, 'cw:deliverypaymentrecord:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('42', '31', '数据字典', 'cw/dictionary', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('43', '42', '查看', null, 'cw:dictionary:list,cw:dictionary:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('44', '42', '新增', null, 'cw:dictionary:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('45', '42', '修改', null, 'cw:dictionary:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('46', '42', '删除', null, 'cw:dictionary:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('47', '31', '回款冲账表', 'cw/receipts', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('48', '47', '查看', null, 'cw:receipts:list,cw:receipts:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('49', '47', '新增', null, 'cw:receipts:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('50', '47', '修改', null, 'cw:receipts:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('51', '47', '删除', null, 'cw:receipts:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('52', '31', '订单表', 'cw/salesinvoice', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('53', '52', '查看', null, 'cw:salesinvoice:list,cw:salesinvoice:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('54', '52', '新增', null, 'cw:salesinvoice:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('55', '52', '修改', null, 'cw:salesinvoice:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('56', '52', '删除', null, 'cw:salesinvoice:delete', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('57', '31', '应收汇总表', 'cw/receivablesummary', null, '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('58', '57', '查看', null, 'cw:receivablesummary:list,cw:receivablesummary:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('59', '57', '新增', null, 'cw:receivablesummary:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('60', '57', '修改', null, 'cw:receivablesummary:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('61', '57', '删除', null, 'cw:receivablesummary:delete', '2', null, '6');

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文件上传';

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', '1', '1', '2016-11-11 11:11:11');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token` (
  `user_id` bigint NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统用户Token';

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES ('1', '19988fcfae06cee34b17144e06f0887b', '2023-04-16 10:56:56', '2023-04-15 22:56:56');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');

-- ----------------------------
-- Table structure for t_delivery_payment_record
-- ----------------------------
DROP TABLE IF EXISTS `t_delivery_payment_record`;
CREATE TABLE `t_delivery_payment_record` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `delivery_date` date DEFAULT NULL COMMENT '发货日期',
  `salesperson` varchar(50) DEFAULT NULL COMMENT '业务员',
  `customer_name` varchar(50) DEFAULT NULL COMMENT '客户名称',
  `sales_delivery_return` varchar(10) DEFAULT NULL COMMENT '销售发货/退货',
  `number` varchar(50) DEFAULT NULL COMMENT '编号',
  `product_name` varchar(50) DEFAULT NULL COMMENT '产品名称',
  `quantity` int DEFAULT NULL COMMENT '数量',
  `unit_price` float DEFAULT NULL COMMENT '单价',
  `invoice_amount` float DEFAULT NULL COMMENT '金额（开票）',
  `region` varchar(50) DEFAULT NULL COMMENT '所属区域',
  `invoice_company` varchar(50) DEFAULT NULL COMMENT '开票公司',
  `settlement_price` float DEFAULT NULL COMMENT '结算价',
  `guide_price` float DEFAULT NULL COMMENT '指导价',
  `expire_date` date DEFAULT NULL COMMENT '到期时间',
  `overdue_days` int DEFAULT NULL COMMENT '超期天数',
  `payment_bank` varchar(50) DEFAULT NULL COMMENT '回款银行',
  `payment_date` date DEFAULT NULL COMMENT '回款日期',
  `public_payment_amount` float DEFAULT NULL COMMENT '对公回款金额',
  `unit_receivable` float DEFAULT NULL COMMENT '单位应收款',
  `personal_payment_date` date DEFAULT NULL COMMENT '回款日期（个人其他）',
  `personal_unit_receivable` float DEFAULT NULL COMMENT '单位应收（个人）',
  `salesperson_payment_date` date DEFAULT NULL COMMENT '回款日期（业务员）',
  `salesperson_unit_receivable` float DEFAULT NULL COMMENT '单位应收(业务员）',
  `exchange_rate` float DEFAULT NULL COMMENT '汇率',
  `usd` float DEFAULT NULL COMMENT '美元',
  `exchange_rate_conversion` float DEFAULT NULL COMMENT '汇兑',
  `created_by` varchar(50) DEFAULT NULL COMMENT '录入人',
  `created_at` datetime DEFAULT NULL COMMENT '录入时间',
  `is_valid` tinyint(1) DEFAULT '1' COMMENT '数据有效性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='发货回款记录表';

-- ----------------------------
-- Records of t_delivery_payment_record
-- ----------------------------

-- ----------------------------
-- Table structure for t_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `t_dictionary`;
CREATE TABLE `t_dictionary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `dict_name` varchar(50) NOT NULL COMMENT '字典名称',
  `code` varchar(50) NOT NULL COMMENT '代码',
  `value` varchar(50) NOT NULL COMMENT '值',
  `created_at` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_dictionary
-- ----------------------------
INSERT INTO `t_dictionary` VALUES ('1', 'document_type', '1', '销售发货', '2023-03-25 23:14:45');
INSERT INTO `t_dictionary` VALUES ('2', 'document_type', '2', '销售退货', '2023-03-25 23:14:45');
INSERT INTO `t_dictionary` VALUES ('3', 'delivery_type', '1', '正常发货', '2023-03-25 23:14:45');
INSERT INTO `t_dictionary` VALUES ('4', 'delivery_type', '2', '加急发货', '2023-03-25 23:14:45');
INSERT INTO `t_dictionary` VALUES ('5', 'invoice_tax_type', '1', '普通增值税发票', '2023-03-25 23:19:34');
INSERT INTO `t_dictionary` VALUES ('6', 'invoice_tax_type', '2', '电子发票', '2023-03-25 23:19:34');
INSERT INTO `t_dictionary` VALUES ('7', 'invoice_tax_type', '3', '增值税专用发票', '2023-03-25 23:19:34');
INSERT INTO `t_dictionary` VALUES ('8', 'invoice_status', '1', '未开票', '2023-03-25 23:19:34');
INSERT INTO `t_dictionary` VALUES ('9', 'invoice_status', '2', '已开票', '2023-03-25 23:19:34');
INSERT INTO `t_dictionary` VALUES ('10', 'invoice_status', '3', '待开票', '2023-03-25 23:19:34');

-- ----------------------------
-- Table structure for t_invoice
-- ----------------------------
DROP TABLE IF EXISTS `t_invoice`;
CREATE TABLE `t_invoice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `delivery_date` varchar(50) NOT NULL COMMENT '发货日期',
  `salesman` varchar(50) NOT NULL COMMENT '业务员',
  `customer_name` varchar(100) NOT NULL COMMENT '客户全名',
  `document_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '单据类型代码',
  `product_code` varchar(50) NOT NULL COMMENT '产品代号',
  `product_name` varchar(100) NOT NULL COMMENT '名称',
  `delivery_quantity` int NOT NULL COMMENT '发货数量',
  `local_currency_price` decimal(10,2) NOT NULL COMMENT '本币含税单价',
  `local_currency_amount` decimal(10,2) NOT NULL COMMENT '本币含税金额',
  `region` varchar(50) DEFAULT NULL COMMENT '所属区域',
  `delivery_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '发货类型',
  `settlement_price` decimal(10,2) DEFAULT NULL COMMENT '结算价',
  `created_by` varchar(50) NOT NULL COMMENT '录入人',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '录入时间',
  `is_valid` tinyint(1) DEFAULT '1' COMMENT '数据有效性',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_invoice
-- ----------------------------

-- ----------------------------
-- Table structure for t_receipts
-- ----------------------------
DROP TABLE IF EXISTS `t_receipts`;
CREATE TABLE `t_receipts` (
  `receipt_id` int NOT NULL AUTO_INCREMENT COMMENT '回款ID',
  `receipt_date` date NOT NULL COMMENT '回款日期',
  `salesman` varchar(50) NOT NULL COMMENT '业务员',
  `remitter` varchar(50) NOT NULL COMMENT '汇款人',
  `customer_name` varchar(100) NOT NULL COMMENT '客户名称',
  `monthly_receipt` decimal(10,2) DEFAULT '0.00' COMMENT '当月回款',
  `year_to_date_receipt` decimal(10,2) DEFAULT '0.00' COMMENT '本年累计回款',
  `receipt_bank` varchar(100) NOT NULL COMMENT '回款银行',
  `to_be_offset` decimal(10,2) DEFAULT '0.00' COMMENT '待冲金额',
  `last_month_to_be_offset` decimal(10,2) DEFAULT '0.00' COMMENT '上月待冲',
  `already_offset` decimal(10,2) DEFAULT '0.00' COMMENT '已冲金额',
  `remarks` text COMMENT '备注',
  `created_by` varchar(50) NOT NULL COMMENT '录入人',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '录入时间',
  `is_valid` tinyint(1) NOT NULL DEFAULT '1' COMMENT '有效性验证',
  PRIMARY KEY (`receipt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='回款冲账表';

-- ----------------------------
-- Records of t_receipts
-- ----------------------------

-- ----------------------------
-- Table structure for t_receivable_summary
-- ----------------------------
DROP TABLE IF EXISTS `t_receivable_summary`;
CREATE TABLE `t_receivable_summary` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `shipping_date` date NOT NULL COMMENT '发货日期',
  `salesman` varchar(50) NOT NULL COMMENT '业务员',
  `customer_name` varchar(100) NOT NULL COMMENT '客户名称',
  `number` varchar(50) NOT NULL COMMENT '编号',
  `product_name` varchar(100) NOT NULL COMMENT '产品名称',
  `document_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '销售发货/退货',
  `quantity` int NOT NULL COMMENT '数量',
  `unit_price` decimal(10,2) NOT NULL COMMENT '单价',
  `invoice_amount` decimal(10,2) DEFAULT NULL COMMENT '金额_开票',
  `region` varchar(50) DEFAULT NULL COMMENT '所属区域',
  `invoice_company` varchar(100) DEFAULT NULL COMMENT '开票公司',
  `settlement_price` decimal(10,2) DEFAULT NULL COMMENT '结算价',
  `guide_price` decimal(10,2) DEFAULT NULL COMMENT '指导价',
  `commission` decimal(10,2) DEFAULT NULL COMMENT '提成',
  `due_date` date DEFAULT NULL COMMENT '到期时间',
  `overdue_days` int DEFAULT NULL COMMENT '超期天数',
  `bank` varchar(50) DEFAULT NULL COMMENT '回款银行',
  `payment_date` date DEFAULT NULL COMMENT '回款日期',
  `public_payment_amount` decimal(10,2) DEFAULT NULL COMMENT '对公回款金额',
  `unit_receivable` decimal(10,2) DEFAULT NULL COMMENT '单位应收款',
  `salesman_payment_date` date DEFAULT NULL COMMENT '业务员回款日期',
  `personal_others` varchar(200) DEFAULT NULL COMMENT '个人其他',
  `salesman_payment` decimal(10,2) DEFAULT NULL COMMENT '业务员回款',
  `salesman_receivable` decimal(10,2) DEFAULT NULL COMMENT '业务员应收',
  `salesman_advance` decimal(10,2) DEFAULT NULL COMMENT '业务员代垫',
  `charge_amount` decimal(10,2) DEFAULT NULL COMMENT '冲账金额',
  `advance_date` date DEFAULT NULL COMMENT '代垫日期',
  `exchange_rate` decimal(10,2) DEFAULT NULL COMMENT '汇率',
  `exchange` decimal(10,2) DEFAULT NULL COMMENT '汇兑',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1273 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='应收汇总表';

-- ----------------------------
-- Records of t_receivable_summary
-- ----------------------------
INSERT INTO `t_receivable_summary` VALUES ('1', '2022-12-26', '谭永昌', '深圳市华泰动物药业有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '100', '20.00', '2000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('2', '2022-12-26', '黎永华', '石家庄于淼', '01D214', '奇力宝(仔猪用)', '销售发货', '300', '14.00', '4200.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('3', '2022-12-26', '林雪', '台湾', '04D061', '甘氨酸锌21%', '销售发货', '5000', '27.81', '139035.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('4', '2022-12-26', '林雪', '台湾', '01D005', '甘氨酸铁17%', '销售发货', '1000', '23.39', '23391.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('5', '2022-12-26', '林雪', '泰国（出口）', '04D061', '甘氨酸锌21%', '销售发货', '2000', '16.73', '33460.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('6', '2022-12-26', '林雪', '泰国（出口）', '01D460', '天科宝  Tankebaal天馨乐', '销售发货', '500', '76.65', '38325.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('7', '2022-12-26', '林雪', '泰国（出口）', '01D457', '天科宝鱼腥调味料', '销售发货', '5000', '17.36', '86800.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('8', '2022-12-26', '林雪', '泰国（出口）', '01D451', 'K-10（出口）', '销售发货', '5000', '29.33', '146650.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('9', '2022-12-26', '何海涛', '沈阳新纪元牧业有限公司', '01D402', '抗氧灵', '销售发货', '1000', '13.50', '13500.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('10', '2022-12-26', '王林', '广东丰信饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '1000', '12.00', '12000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('11', '2022-12-26', '王林', '珠海孙宝宝', '01D781', '维稳宝', '销售发货', '25', '28.00', '700.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('12', '2022-12-26', '王林', '张天佑', '01D075', '奇力锌', '销售发货', '10', '43.00', '430.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('13', '2022-12-26', '纳生', '南宁市百禾生物科技有限公司(广西金陵农牧)', '01E740', '舒柠500', '销售发货', '500', '46.00', '23000.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('14', '2022-12-27', '邱桂雄', '广州市天河联华农业科技有限公司', '02D345', '亚硒酸钠维生素E预混剂', '销售发货', '500', '4.00', '2000.00', '动保事业部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('15', '2022-12-27', '邱桂雄', '广州市天河联华农业科技有限公司', '01D770', '多维葡萄糖', '销售发货', '300', '8.80', '2640.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('16', '2022-12-27', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '10.10', '303000.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('17', '2022-12-27', '谭永昌', '肇庆市华饲鱼粉有限公司', '01D402', '抗氧灵', '销售发货', '300', '12.00', '3600.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('18', '2022-12-27', '袁小波', '成都德力元商贸有限公司(成都铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '2100', '10.65', '22365.00', '华西市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('19', '2022-12-27', '龚波', '正大预混料（沈阳）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售退货', '-25', '20.90', '-522.50', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('20', '2022-12-27', '罗辉', '嘉吉饲料(佛山)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '17.50', '875.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('21', '2022-12-27', '罗辉', '嘉吉饲料(南宁)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '17.50', '875.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('22', '2022-12-27', '林雪', '巴基斯坦', '05D008', '禽宝（禽用）', '销售发货', '3000', '10.01', '30015.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('23', '2022-12-27', '杨锋', '江西三同生物科技有限公司', '01E634', '微速达（畜禽用）', '销售发货', '500', '14.00', '7000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('24', '2022-12-27', '杨锋', '厦门渔雄越生物科技有限公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '1', '17.00', '17.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('25', '2022-12-27', '刘云', '湖南鑫百优生物科技有限公司', '01E751', '奇力宝（仔猪用）', '销售发货', '2000', '14.80', '29600.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('26', '2022-12-27', '何海涛', '艾地盟动物保健及营养（南京）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '15.50', '775.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('27', '2022-12-27', '何海涛', '英联饲料(辽宁)有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '1000', '13.30', '13300.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('28', '2022-12-27', '何海涛', '辽宁波尔莱特农牧实业有限公司', '01D402', '抗氧灵', '销售发货', '4000', '13.50', '54000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('29', '2022-12-27', '何海涛', '英联饲料(辽宁)有限公司（赠送）', '01D267', '奇力宝（蛋壳素）', '销售发货', '1000', '10.00', '10000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('30', '2022-12-27', '何翔飞', '广州昭品饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '1000', '2.85', '2850.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('31', '2022-12-27', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '01D297', '奇力宝（甲鱼用-)', '销售发货', '5000', '3.85', '19250.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('32', '2022-12-27', '王林', '珠海市德海生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '5000', '16.00', '80000.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('33', '2022-12-27', '黄坤', '江苏敖众生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('34', '2022-12-27', '黄正月', '昆明思泊特饲料科技有限责任公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.60', '8600.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('35', '2022-12-28', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('36', '2022-12-28', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('37', '2022-12-28', '宁会', '莒县新特瑞饲料有限公司', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '100', '16.00', '1600.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('38', '2022-12-28', '宁会', '莒县新特瑞饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '12.00', '6000.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('39', '2022-12-28', '杨锋', '高安猪太傅饲料有限公司', '01D207', '母猪用微量元素预混合饲料', '销售发货', '2000', '18.70', '37400.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('40', '2022-12-28', '杨荣国', '河南广安生物科技股份有限公司', '01D501', '强味酸(B）', '销售发货', '1200', '23.00', '27600.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('41', '2022-12-28', '王林', '广东中山卢均文', '01D317', '微速达（水产用）', '销售发货', '200', '14.10', '2820.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('42', '2022-12-28', '张树军', '郑州博大牧业科技有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '2000', '11.50', '23000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('43', '2022-12-28', '何海涛', '辽宁九州生物科技有限公司', '01D402', '抗氧灵', '销售发货', '1500', '13.49', '20235.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('44', '2022-12-28', '杨小乐', '内蒙古博鑫农牧业科技有限公司（廊坊嘉吉）', '01D616', '美多(反刍)', '销售发货', '200', '15.20', '3040.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('45', '2022-12-29', '邱桂雄', '广东省医学实验动物中心', '01E704', '实验动物用', '销售发货', '500', '21.50', '10750.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('46', '2022-12-29', '谭永昌', '广州领鲜生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '9.00', '4500.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('47', '2022-12-29', '袁小波', '成都德力元商贸有限公司（大理傲农黑尔农牧）', '04D214', '奇力宝（仔猪用）', '销售发货', '1500', '10.65', '15975.00', '华西市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('48', '2022-12-29', '张明', '江门市恒胜实业有限公司', '04D313', '奇力宝（淡水鱼用）B', '销售发货', '15000', '14.60', '219000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('49', '2022-12-29', '张明', '广东新粮实业有限公司新粮饲料厂', '01D402', '抗氧灵', '销售发货', '2000', '9.50', '19000.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('50', '2022-12-29', '杨锋', '福州江炜（福建省福清华龙饲料有限公司）', '01D402', '抗氧灵', '销售发货', '500', '16.00', '8000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('51', '2022-12-29', '杨锋', '福州江炜（福建省福清华龙饲料有限公司（赠送））', '01D402', '抗氧灵', '销售发货', '75', '9.50', '712.50', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('52', '2022-12-29', '刘云', '吉林省金沃农牧科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '200', '17.00', '3400.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('53', '2022-12-29', '刘云', '吉林省金沃农牧科技有限公司', '01D213', '天籁（仔猪用）', '销售发货', '100', '28.00', '2800.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('54', '2022-12-29', '杨荣国', '西安青松生物科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '200', '18.00', '3600.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('55', '2022-12-29', '张树军', '河南旭康牧业科技有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '2900', '11.50', '33350.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('56', '2022-12-29', '张树军', '河南旭康牧业科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '12.30', '12300.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('57', '2022-12-29', '杨平', '山东汇农生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '8.50', '17000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('58', '2022-12-29', '周斌', '江门市英海饲料有限公司', '04E008', '寶乐酸', '销售发货', '1000', '15.00', '15000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('59', '2022-12-29', '纳生', '新沂益客大地饲料有限公司', '01D464', '赛肥素I', '销售发货', '250', '59.00', '14750.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('60', '2022-12-29', '纳生', '宿迁益客旭阳饲料有限公司', '01D464', '赛肥素I', '销售发货', '200', '59.00', '11800.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('61', '2022-12-30', '邱桂雄', '广州市天河联华农业科技有限公司', '02D347', '硫酸黏菌素预混剂10%', '销售发货', '1860', '23.50', '43710.00', '动保事业部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('62', '2022-12-30', '邱桂雄', '广州市天河联华农业科技有限公司', '01D772', '水溶性电解多维预混饲料', '销售发货', '1500', '18.00', '27000.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('63', '2022-12-30', '邱桂雄', '广州市天河联华农业科技有限公司', '01D474', '天科宝', '销售发货', '40', '18.30', '732.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('64', '2022-12-30', '谭永昌', '广州市汇邦动物药业有限公司', '01D005', '甘氨酸铁17%', '销售发货', '50', '14.00', '700.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('65', '2022-12-30', '袁小波', '四川省眉山万家好饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '3000', '8.50', '25500.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('66', '2022-12-30', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '12000', '13.35', '160200.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('67', '2022-12-30', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '20000', '12.95', '259000.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('68', '2022-12-30', '黎永华', '内蒙古宏泰大地饲料科技有限责任公司', '01D611', '美多－Ⅱ（禽用）', '销售发货', '200', '22.99', '4598.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('69', '2022-12-30', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '100', '15.50', '1550.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('70', '2022-12-30', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '100', '15.50', '1550.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('71', '2022-12-30', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '3000', '14.80', '44400.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('72', '2022-12-30', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('73', '2022-12-30', '龚波', '正大预混料（天津）有限公司', '01D403', '抗氧灵', '销售发货', '3000', '12.10', '36300.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('74', '2022-12-30', '罗辉', '上海惠泰立贸易有限公司（上海保斯利）', '01D402', '抗氧灵', '销售发货', '550', '11.00', '6050.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('75', '2022-12-30', '刘云', '沈阳乐农氏动物营养有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('76', '2022-12-30', '刘云', '湖南鑫百优生物科技有限公司', '01E751', '奇力宝（仔猪用）', '销售发货', '3000', '14.80', '44400.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('77', '2022-12-30', '何海涛', '辽宁众友饲料有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.50', '27000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('78', '2022-12-30', '鲁刚', '哈尔滨远大牧业有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '5000', '21.10', '105500.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('79', '2022-12-30', '李军勇', '福建大北农华有水产科技集团有限公司', '01D333', '微速强', '销售发货', '25', '8.90', '222.50', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('80', '2022-12-30', '李军勇', '福建大北农华有水产科技集团有限公司', '01D333', '微速强', '销售退货', '-25', '8.90', '-222.50', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('81', '2022-12-30', '李军勇', '福清市励治饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '10000', '3.40', '34000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('82', '2022-12-30', '曲光', '山农农牧（泰安）有限公司', '01E745', '澳美维', '销售发货', '1000', '14.90', '14900.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('83', '2022-12-30', '纳生', '宿迁益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '700', '59.00', '41300.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('84', '2022-12-30', '纳生', '临沂众客饲料有限公司', '01D464', '赛肥素I', '销售发货', '700', '59.00', '41300.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('85', '2022-12-30', '纳生', '南宁市百禾生物科技有限公司(广西金陵农牧)', '01E740', '舒柠500', '销售发货', '1200', '46.00', '55200.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('86', '2022-12-31', '郑云瀚', '土耳其', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '1000', '24.43', '24426.00', '外贸部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('87', '2023-01-02', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '10.10', '303000.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('88', '2023-01-02', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '9.05', '271500.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('89', '2023-01-02', '谭永昌', '广州市众望饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '13.00', '6500.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('90', '2023-01-02', '谭永昌', '广州金水动物保健品有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1000', '22.70', '22700.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('91', '2023-01-02', '谭永昌', '广州金水动物保健品有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '26.00', '2600.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('92', '2023-01-02', '谭永昌', '广州金水动物保健品有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '500', '38.50', '19250.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('93', '2023-01-02', '谭永昌', '广州金水动物保健品有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1500', '16.70', '25050.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('94', '2023-01-02', '张明', '佛山市顺德区兄弟德力饲料实业有限公司', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '500', '16.90', '8450.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('95', '2023-01-02', '张明', '佛山市顺德区兄弟德力饲料实业有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '2500', '10.50', '26250.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('96', '2023-01-02', '宁会', '山东惠邦生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '25', '10.20', '255.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('97', '2023-01-02', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '2000', '17.00', '34000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('98', '2023-01-02', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '04D291', '奇力宝（鳗鱼用）', '销售发货', '8000', '3.90', '31200.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('99', '2023-01-02', '李军勇', '福建大北农华有水产科技集团有限公司', '01D321', '奇力宝（淡水甲壳动物', '销售发货', '3000', '10.00', '30000.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('100', '2023-01-02', '杨平', '莒县于丽珍', '01D779', '澳美维', '销售发货', '200', '35.00', '7000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('101', '2023-01-02', '杨锋黎永华', '泰高营养科技（湖南）有限公司', '04D061', '甘氨酸锌21%', '销售退货', '-300', '13.50', '-4050.00', '华北市场', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('102', '2023-01-02', '纳生', '临沂众客饲料有限公司', '01D464', '赛肥素I', '销售发货', '500', '59.00', '29500.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('103', '2023-01-02', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '2000', '10.10', '20200.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('104', '2023-01-02', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '25000', '10.10', '252500.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('105', '2023-01-02', '高碧', '北票温氏农牧有限公司', '01D005', '甘氨酸铁17%', '销售发货', '8000', '7.75', '62000.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('106', '2023-01-03', '邱桂雄', '武汉民族科技饲料有限公司', '01E787', '奇力宝（蛋禽用）', '销售发货', '3000', '11.02', '33060.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('107', '2023-01-03', '邱桂雄', '武汉民族科技饲料有限公司', '01E755', '奇力宝（蛋禽用-颗粒型）', '销售发货', '3000', '9.90', '29700.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('108', '2023-01-03', '黎永华', '北京九州大地生物技术集团股份有限公司天津分公司', '01D611', '美多－Ⅱ（禽用）', '销售发货', '200', '22.99', '4598.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('109', '2023-01-03', '黎永华', '沧州及焕齐', '01D274', '奇力宝（蛋禽用）', '销售发货', '250', '13.00', '3250.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('110', '2023-01-03', '黎永华', '沧州及焕齐', '01D267', '奇力宝（蛋壳素）', '销售发货', '250', '13.00', '3250.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('111', '2023-01-03', '张明', '佛山市顺德区旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '4000', '14.80', '59200.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('112', '2023-01-03', '林雪', '台湾', '01D460', '天科宝  Tankebaal天馨乐', '销售发货', '200', '80.64', '16128.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('113', '2023-01-03', '杨锋', '福建省漳州市华龙饲料有限公司', '01D402', '抗氧灵', '销售发货', '3500', '16.00', '56000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('114', '2023-01-03', '刘云', '沈阳乐农氏动物营养有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.50', '6750.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('115', '2023-01-03', '刘云', '沈阳乐农氏动物营养有限公司', '01D092', '奇力铬（蛋氨酸铬001）', '销售发货', '150', '11.00', '1650.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('116', '2023-01-03', '刘云', '沈阳乐农氏动物营养有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '18.00', '1800.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('117', '2023-01-03', '刘云', '沈阳乐农氏动物营养有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '50', '35.00', '1750.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('118', '2023-01-03', '何海涛', '辽宁牧华合美生物科技有限公司', '01D409', '抗氧灵-T', '销售发货', '1000', '22.00', '22000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('119', '2023-01-03', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '12.30', '12300.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('120', '2023-01-03', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('121', '2023-01-03', '王林', '福建莆田李宜贤', '04D303', '奇力宝（海水鱼用）', '销售发货', '100', '10.10', '1010.00', '水产市场', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('122', '2023-01-03', '王林', '福建莆田李宜贤', '01D781', '维稳宝', '销售发货', '100', '27.80', '2780.00', '水产市场', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('123', '2023-01-03', '王林', '广东恒兴饲料实业股份有限公司遂溪分公司', '01D100', '天安硒', '销售发货', '3000', '56.50', '169500.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('124', '2023-01-03', '郑云瀚', '土耳其', '04D061', '甘氨酸锌21%', '销售发货', '10000', '15.46', '154560.00', '外贸部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('125', '2023-01-03', '郑云瀚', '土耳其', '01D042', '天锰乐（G/Mn-220）', '销售发货', '3500', '17.87', '62548.50', '外贸部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('126', '2023-01-03', '郑云瀚', '土耳其', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '1500', '33.47', '50197.50', '外贸部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('127', '2023-01-03', '郑云瀚', '土耳其', '01D005', '甘氨酸铁17%', '销售发货', '9000', '10.76', '96876.00', '外贸部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('128', '2023-01-03', '杨平', '山东三兴禽业有限公司', '01D779', '澳美维', '销售发货', '1000', '50.00', '50000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('129', '2023-01-03', '杨平', '山东中巴农牧发展有限公司', '01D779', '澳美维', '销售发货', '400', '50.00', '20000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('130', '2023-01-03', '杨小乐', '内蒙古博鑫农牧业科技有限公司（现代商河）', '01D620', '美多（反刍动物用）', '销售发货', '2000', '16.90', '33800.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('131', '2023-01-03', '杨小乐', '内蒙古博鑫农牧业科技有限公司（现代商河）', '01D616', '美多(反刍)', '销售发货', '1000', '14.40', '14400.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('132', '2023-01-04', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售退货', '-150', '9.05', '-1357.50', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('133', '2023-01-04', '谭永昌', '广州德沅生物饲料有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '100', '24.50', '2450.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('134', '2023-01-04', '谭永昌', '广州德沅生物饲料有限公司', '01D420', '不来霉', '销售发货', '500', '11.00', '5500.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('135', '2023-01-04', '谭永昌', '广州德沅生物饲料有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '300', '17.00', '5100.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('136', '2023-01-04', '袁小波', '成都德力元商贸有限公司(重庆铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '7000', '10.65', '74550.00', '华西市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('137', '2023-01-04', '黎永华', '河北恒派生物科技有限公司', '01D272', '禽宝', '销售发货', '3000', '10.00', '30000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('138', '2023-01-04', '张明', '佛山市顺德区容大饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '100', '13.50', '1350.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('139', '2023-01-04', '张明', '佛山市顺德区容大饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.50', '4250.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('140', '2023-01-04', '罗辉', '溧阳正昌饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '7.80', '15600.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('141', '2023-01-04', '林雪', '台湾（簡晹晃）', '01D473', '天科宝（奶香型）', '销售发货', '30', '80.00', '2400.00', '外贸部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('142', '2023-01-04', '何翔飞', '广州昭品饲料有限公司', '01D782', '澳美维（水产用）', '销售发货', '45', '26.20', '1179.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('143', '2023-01-04', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('144', '2023-01-04', '王林', '佛山市顺德区丰华饲料实业有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '500', '12.00', '6000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('145', '2023-01-04', '张树军', '河南正本清源科技发展股份有限公司', '01D217', '奇力宝-仔猪用', '销售发货', '2000', '22.00', '44000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('146', '2023-01-04', '张树军', '河南安信畜牧科技发展有限公司', '01D777', '澳美维', '销售发货', '250', '35.50', '8875.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('147', '2023-01-04', '张树军', '河南安信畜牧科技发展有限公司', '01D618', '美多-II（畜禽用）', '销售发货', '250', '22.00', '5500.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('148', '2023-01-04', '李军勇', '福建大昌盛饲料有限公司', '04D323', '奇力宝（鳗鱼用）', '销售发货', '10000', '4.15', '41500.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('149', '2023-01-04', '李军勇', '福建大昌盛饲料有限公司', '04D302', '奇力宝（海水鱼用）', '销售发货', '20000', '4.50', '90000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('150', '2023-01-04', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '1000', '10.60', '10600.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('151', '2023-01-04', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '1000', '10.60', '10600.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('152', '2023-01-04', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '1000', '10.60', '10600.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('153', '2023-01-04', '黄坤', '扬州源泉饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '400', '19.00', '7600.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('154', '2023-01-04', '黄坤', '扬州源泉饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '200', '41.00', '8200.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('155', '2023-01-04', '曲光', '山东莘县张广伟', '04D061', '甘氨酸锌21%', '销售发货', '100', '13.30', '1330.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('156', '2023-01-04', '曲光', '山东莘县张广伟', '01D274', '奇力宝（蛋禽用）', '销售发货', '250', '14.00', '3500.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('157', '2023-01-04', '曲光', '山东莘县张广伟', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '50', '34.00', '1700.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('158', '2023-01-04', '曲光', '山东莘县张广伟', '01D005', '甘氨酸铁17%', '销售发货', '100', '8.20', '820.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('159', '2023-01-04', '曲光', '山东莘县王增', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '15.00', '15000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('160', '2023-01-05', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '9.05', '271500.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('161', '2023-01-05', '谭永昌', '广州王少华', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '120', '27.00', '3240.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('162', '2023-01-05', '谭永昌', '广州王少华', '01D005', '甘氨酸铁17%', '销售发货', '75', '8.30', '622.50', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('163', '2023-01-05', '袁小波', '成都德力元商贸有限公司', '05D103', '美多（地羊型）', '销售发货', '1000', '16.00', '16000.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('164', '2023-01-05', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('165', '2023-01-05', '张明', '佛山市顺德区旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '5000', '14.80', '74000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('166', '2023-01-05', '张明', '佛山市顺德区冠羽实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.00', '14000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('167', '2023-01-05', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '300', '15.50', '4650.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('168', '2023-01-05', '龚波', '正大预混料（柳州）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '1200', '20.90', '25080.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('169', '2023-01-05', '龚波', '正大预混料（柳州）有限公司', '01D403', '抗氧灵', '销售发货', '850', '12.10', '10285.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('170', '2023-01-05', '罗辉', '江苏雅博动物健康科技有限责任公司', '01D005', '甘氨酸铁17%', '销售发货', '2850', '13.80', '39330.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('171', '2023-01-05', '罗辉', '上海大冠饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售退货', '-184', '8.50', '-1564.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('172', '2023-01-05', '林雪', '越南（出口）', '05D003', '奇力宝（仔猪用）', '销售发货', '4000', '11.39', '45540.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('173', '2023-01-05', '林雪', '越南（出口）', '04D061', '甘氨酸锌21%', '销售发货', '2000', '12.14', '24288.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('174', '2023-01-05', '林雪', '越南（出口）', '01D042', '天锰乐（G/Mn-220）', '销售发货', '2000', '12.77', '25530.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('175', '2023-01-05', '林雪', '越南（出口）', '01D005', '甘氨酸铁17%', '销售发货', '7000', '7.73', '54096.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('176', '2023-01-05', '刘云', '浏阳普泰农牧发展有限公司', '01D100', '天安硒', '销售发货', '200', '50.00', '10000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('177', '2023-01-05', '刘云', '湖南中科贝隆生物科技有限公司', '01D458', '诱特妙（水产诱食促长剂）', '销售发货', '200', '40.00', '8000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('178', '2023-01-05', '刘云', '湖南中科贝隆生物科技有限公司', '01D402', '抗氧灵', '销售发货', '100', '12.50', '1250.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('179', '2023-01-05', '刘云', '湖南中科贝隆生物科技有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '200', '35.00', '7000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('180', '2023-01-05', '何翔飞', '佛山市顺德区成冠饲料有限公司', '04D291', '奇力宝（鳗鱼用）', '销售发货', '3000', '3.90', '11700.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('181', '2023-01-05', '王林', '新会于荣文', '01D782', '澳美维（水产用）', '销售发货', '180', '26.20', '4716.00', '水产市场', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('182', '2023-01-05', '王林', '新会于荣文', '01D317', '微速达（水产用）', '销售发货', '325', '14.10', '4582.50', '水产市场', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('183', '2023-01-05', '张树军', '河南旭康牧业科技有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '500', '11.50', '5750.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('184', '2023-01-05', '张树军', '河南旭康牧业科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '300', '8.20', '2460.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('185', '2023-01-05', '李军勇', '共鳞实业（深圳）有限公司', '04D312', '奇力宝（淡水鱼用）A', '销售发货', '5000', '12.50', '62500.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('186', '2023-01-05', '曲光', '赛孚特生物科技（潍坊）有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '14.00', '14000.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('187', '2023-01-05', '曲光', '山农农牧（泰安）有限公司', '01E745', '澳美维', '销售发货', '3000', '14.90', '44700.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('188', '2023-01-05', '纳生', '南宁市百禾生物科技有限公司(广西金陵农牧)', '01E740', '舒柠500', '销售发货', '1300', '46.00', '59800.00', '直属区域', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('189', '2023-01-05', '张树军', '郑州桦星牧业有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '500', '14.50', '7250.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('190', '2023-01-06', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '10.10', '303000.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('191', '2023-01-06', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '33000', '9.05', '298650.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('192', '2023-01-06', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '8000', '13.35', '106800.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('193', '2023-01-06', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '10000', '12.95', '129500.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('194', '2023-01-06', '罗辉', '嘉吉饲料（合肥）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('195', '2023-01-06', '罗辉', '上海迦盟生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '184', '8.50', '1564.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('196', '2023-01-06', '林雪', '印度', '05D017', '奇力宝（鸡用）', '销售发货', '20000', '9.66', '193200.00', '外贸部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('197', '2023-01-06', '杨锋', '厦门渔雄越生物科技有限公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '1', '17.00', '17.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('198', '2023-01-06', '何海涛', '鞍山兴达伟业矿物质科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.50', '6750.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('199', '2023-01-06', '何海涛', '鞍山兴达伟业矿物质科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.50', '4250.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('200', '2023-01-06', '何翔飞', '佛山市顺德区广添饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '6000', '2.85', '17100.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('201', '2023-01-06', '何翔飞', '广州市和生堂动物药业有限公司', '01D043', '奇力锰（蛋氨酸锰150）', '销售发货', '40', '46.00', '1840.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('202', '2023-01-06', '鲁刚', '哈尔滨市联丰饲料有限公司', '01D402', '抗氧灵', '销售发货', '1000', '11.30', '11300.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('203', '2023-01-06', '鲁刚', '哈尔滨市联丰饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '20.50', '20500.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('204', '2023-01-06', '王林', '新会区睦洲镇万华饲料厂', '01D420', '不来霉', '销售发货', '250', '7.50', '1875.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('205', '2023-01-06', '王林', '新会区睦洲镇万华饲料厂', '01D402', '抗氧灵', '销售发货', '250', '9.50', '2375.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('206', '2023-01-06', '王林', '新会区睦洲镇万华饲料厂', '01D307', '奇力宝淡水甲壳动物用', '销售发货', '2000', '4.03', '8060.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('207', '2023-01-06', '王林', '中山市东升镇渔乐圈饲料经销店', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('208', '2023-01-06', '王林', '珠海一村农业发展有限公司', '04D791', '一村·多维', '销售发货', '500', '34.50', '17250.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('209', '2023-01-06', '王林', '珠海一村农业发展有限公司', '04D337', '一村多矿', '销售发货', '1000', '14.50', '14500.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('210', '2023-01-06', '李军勇', '福州航盛饲料有限公司', '04D312', '奇力宝（淡水鱼用）A', '销售发货', '2000', '8.00', '16000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('211', '2023-01-06', '李军勇', '福州航盛饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '1000', '13.00', '13000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('212', '2023-01-06', '李军勇', '福州航盛饲料有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '2000', '12.50', '25000.00', '水产市场', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('213', '2023-01-06', '曲光', '潍坊爱菲德饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '100', '13.30', '1330.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('214', '2023-01-07', '邱桂雄', '广州市天河联华农业科技有限公司', '02D503', '奇力考星', '销售发货', '100', '113.00', '11300.00', '动保事业部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('215', '2023-01-07', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '40000', '9.05', '362000.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('216', '2023-01-07', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '11000', '10.10', '111100.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('217', '2023-01-07', '刘云', '沈阳乐农氏动物营养有限公司', '04D061', '甘氨酸锌21%', '销售发货', '400', '13.50', '5400.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('218', '2023-01-07', '何海涛', '公主岭新康饲料有限公司', '01D501', '强味酸(B）', '销售发货', '1000', '22.00', '22000.00', '华北市场', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('219', '2023-01-09', '邱桂雄', '广州市天河联华农业科技有限公司', '02D503', '奇力考星', '销售发货', '300', '113.00', '33900.00', '动保事业部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('220', '2023-01-09', '邱桂雄', '广州市天河联华农业科技有限公司', '02D347', '硫酸黏菌素预混剂10%', '销售发货', '4140', '23.50', '97290.00', '动保事业部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('221', '2023-01-09', '邱桂雄', '广州市天河联华农业科技有限公司', '01E736', '种多宝', '销售发货', '300', '19.00', '5700.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('222', '2023-01-09', '邱桂雄', '广州市天河联华农业科技有限公司', '01D901', '排毒宁', '销售发货', '300', '10.80', '3240.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('223', '2023-01-09', '谭永昌', '广州领鲜生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '9.00', '9000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('224', '2023-01-09', '王林', '广东恒兴饲料实业股份有限公司遂溪分公司', '01D214', '奇力宝(仔猪用)', '销售发货', '5000', '23.50', '117500.00', '水产市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('225', '2023-01-09', '曹志静', '广州兴兴物流有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '150', '9.05', '1357.50', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('226', '2023-01-09', '刘云', '沈阳乐农氏动物营养有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('227', '2023-01-09', '何海涛', '辽宁九州生物科技有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.49', '26980.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('228', '2023-01-10', '邱桂雄', '广东科邦饲料科技有限公司', '01D230', '奇力宝（生长猪用）', '销售发货', '2500', '9.50', '23750.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('229', '2023-01-10', '邱桂雄', '广东科邦饲料科技有限公司', '01D225', '天籁（母猪用）', '销售发货', '1000', '23.50', '23500.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('230', '2023-01-10', '谭永昌', '广州农信饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '9.00', '18000.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('231', '2023-01-10', '袁小波', '成都申利行生物科技有限公司', '01E724', '奇力宝（母猪用-颗粒型）', '销售发货', '1000', '9.00', '9000.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('232', '2023-01-10', '袁小波', '成都申利行生物科技有限公司', '01E711', '奇力宝（仔猪用-颗粒型）', '销售发货', '5000', '10.50', '52500.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('233', '2023-01-10', '何海涛', '鞍山市精艺农牧科技有限公司', '01D757', '天籁', '销售发货', '600', '30.00', '18000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('234', '2023-01-11', '邱桂雄', '广州市天河联华农业科技有限公司', '02D415', '阿莫西林', '销售发货', '100', '96.00', '9600.00', '动保事业部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('235', '2023-01-11', '邱桂雄', '广州市天河联华农业科技有限公司', '01D772', '水溶性电解多维预混饲料', '销售发货', '645', '18.00', '11610.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('236', '2023-01-11', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '9.05', '271500.00', '大客户部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('237', '2023-01-11', '杨锋', '厦门渔雄越生物科技有限公司', '01D324', '微速强', '销售发货', '500', '9.50', '4750.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('238', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D779', '澳美维', '销售发货', '40', '31.00', '1240.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('239', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D456', '天科宝（F-05-120）', '销售发货', '40', '15.50', '620.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('240', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D402', '抗氧灵', '销售发货', '50', '9.50', '475.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('241', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '75', '10.00', '750.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('242', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D262', '禽宝', '销售发货', '1000', '13.00', '13000.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('243', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '100', '14.00', '1400.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('244', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '50', '12.30', '615.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('245', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '100', '9.60', '960.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('246', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '180', '26.50', '4770.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('247', '2023-01-11', '何海涛', '沈阳腾和商贸有限公司', '01D011', '奇力铁预混料', '销售发货', '275', '8.20', '2255.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('248', '2023-01-11', '杨小乐', '内蒙古博鑫农牧业科技有限公司（郑州嘉吉）', '01D616', '美多(反刍)', '销售发货', '25', '19.20', '480.00', '华西市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('249', '2023-01-12', '邱桂雄', '广州市天河联华农业科技有限公司', '01E736', '种多宝', '销售发货', '100', '19.00', '1900.00', '直属区域', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('250', '2023-01-12', '刘云', '沈阳惠普饲料有限公司', '01D505', '幼添宝', '销售发货', '250', '13.00', '3250.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('251', '2023-01-12', '刘云', '沈阳惠普饲料有限公司', '01D225', '天籁（母猪用）', '销售发货', '50', '28.00', '1400.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('252', '2023-01-12', '刘云', '沈阳惠普饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '1000', '12.50', '12500.00', '华南市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('253', '2023-01-12', '何海涛', '辽宁美龙饲料科技有限公司', '01D011', '奇力铁预混料', '销售发货', '300', '8.50', '2550.00', '华北市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('254', '2023-01-13', '邱桂雄', '广州市天河联华农业科技有限公司', '02D350', '卡巴匹林钙粉', '销售退货', '-12', '34.70', '-416.40', '直属区域', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('255', '2023-01-13', '曲光', '山农农牧（泰安）有限公司', '01E745', '澳美维', '销售发货', '2000', '14.90', '29800.00', '华中市场', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('256', '2023-01-30', '邱桂雄', '广州市天河联华农业科技有限公司', '02D345', '亚硒酸钠维生素E预混剂', '销售退货', '-1', '4.00', '-4.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('257', '2023-01-30', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '1000', '9.70', '9700.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('258', '2023-01-30', '龚波', '潍坊天普阳光饲料科技有限公司', '01D610', '美多-II（畜禽用）', '销售发货', '1000', '17.30', '17300.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('259', '2023-01-30', '龚波', '正大预混料（南通）有限公司', '01D403', '抗氧灵', '销售发货', '200', '12.10', '2420.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('260', '2023-01-30', '罗辉', '嘉吉动物营养饲料（济宁）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('261', '2023-01-30', '黄坤', '南京禾嘉农牧科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '100', '13.30', '1330.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('262', '2023-01-30', '黄坤', '南京禾嘉农牧科技有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '16.00', '1600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('263', '2023-01-30', '黄坤', '南京禾嘉农牧科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '300', '8.20', '2460.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('264', '2023-01-30', '纳生', '宿迁益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '700', '59.00', '41300.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('265', '2023-01-30', '杨小乐', '内蒙古博鑫农牧业科技有限公司（北京中粮）', '01D620', '美多（反刍动物用）', '销售发货', '3300', '16.90', '55770.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('266', '2023-01-30', '杨小乐', '内蒙古博鑫农牧业科技有限公司（宜春嘉吉）', '01D616', '美多(反刍)', '销售发货', '300', '14.90', '4470.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('267', '2023-01-31', '谭永昌', '广州德沅生物饲料有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '200', '24.50', '4900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('268', '2023-01-31', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '500', '15.50', '7750.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('269', '2023-01-31', '罗辉', '宿迁市立华牧业有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '1200', '25.10', '30120.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('270', '2023-01-31', '何翔飞', '广州昭品饲料有限公司', '01A301', '一水硫酸镁', '销售发货', '250', '5.00', '1250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('271', '2023-01-31', '王林', '广东中山卢均文', '01D317', '微速达（水产用）', '销售发货', '500', '14.10', '7050.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('272', '2023-01-31', '黄坤', '江苏牧佳生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1000', '13.30', '13300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('273', '2023-01-31', '黄坤', '江苏牧佳生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.20', '8200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('274', '2023-01-31', '杨平', '莒南县众盛兽药有限公司', '01D779', '澳美维', '销售发货', '200', '45.00', '9000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('275', '2023-01-31', '杨平', '莒南县众盛兽药有限公司(赠送)', '01D779', '澳美维', '销售发货', '40', '31.00', '1240.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('276', '2023-01-31', '杨平', '蒙阴益嘉养殖场', '01D779', '澳美维', '销售发货', '60', '35.00', '2100.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('277', '2023-01-31', '纳生', '邢台益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '150', '59.00', '8850.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('278', '2023-02-01', '刘云', '沈阳良宇', '01D092', '奇力铬（蛋氨酸铬001）', '销售发货', '100', '9.00', '900.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('279', '2023-02-01', '刘云', '沈阳良宇', '01D011', '奇力铁预混料', '销售发货', '175', '8.20', '1435.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('280', '2023-02-01', '刘云', '沈阳良宇', '04D061', '甘氨酸锌21%', '销售发货', '150', '13.30', '1995.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('281', '2023-02-02', '黎永华', '石家庄海贝饲料科技有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '2000', '13.50', '27000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('282', '2023-02-02', '黄正月', '重庆积善农牧有限公司', '01D779', '澳美维', '销售发货', '100', '38.00', '3800.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('283', '2023-02-02', '杨平', '山东菲亚宠物食品有限公司', '01D402', '抗氧灵', '销售发货', '1000', '12.00', '12000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('284', '2023-02-02', '杨平', '山东菲亚宠物食品有限公司', '01D420', '不来霉', '销售发货', '1000', '12.30', '12300.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('285', '2023-02-02', '李军勇', '福建昌琪生物工程有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '3000', '3.40', '10200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('286', '2023-02-02', '曲光', '青岛北海鲲生物科技有限公司', '01D324', '微速强', '销售发货', '575', '10.90', '6267.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('287', '2023-02-02', '曲光', '青岛北海鲲生物科技有限公司', '01D324', '微速强', '销售发货', '1425', '10.90', '15532.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('288', '2023-02-02', '黄坤', '扬州绿多邦生物有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('289', '2023-02-02', '黄坤', '徐州渔师傅生物科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '20', '24.00', '480.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('290', '2023-02-02', '黄坤', '徐州渔师傅生物科技有限公司', '01D454', '天科宝', '销售发货', '20', '24.00', '480.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('291', '2023-02-02', '黄坤', '徐州渔师傅生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '17.50', '17500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('292', '2023-02-02', '周斌', '鹤山市科牧源动物药业有限公司', '01D317', '微速达（水产用）', '销售发货', '25', '14.10', '352.50', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('293', '2023-02-02', '张树军', '河南安信畜牧科技发展有限公司', '01D777', '澳美维', '销售发货', '175', '35.50', '6212.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('294', '2023-02-02', '张树军', '河南安信畜牧科技发展有限公司', '01D618', '美多-II（畜禽用）', '销售发货', '400', '22.00', '8800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('295', '2023-02-02', '曲光', '青岛北海鲲生物科技有限公司', '01D781', '维稳宝', '销售发货', '500', '32.50', '16250.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('296', '2023-02-02', '何海涛', '大成万达(天津)有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '12.54', '25080.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('297', '2023-02-02', '何海涛', '四川大成农牧科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '12.54', '25080.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('298', '2023-02-02', '周斌', '肇庆端州张建辉', '04E026', '酸满宝', '销售发货', '30', '14.00', '420.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('299', '2023-02-03', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D226', '奇力宝（母猪用）', '销售发货', '600', '13.35', '8010.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('300', '2023-02-03', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '9925', '10.65', '105701.25', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('301', '2023-02-03', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '2675', '10.65', '28488.75', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('302', '2023-02-03', '谭永昌', '佛山市斯科沃生物科技有限公司', '01D318', '微速达（猪用）', '销售发货', '500', '15.00', '7500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('303', '2023-02-03', '王林', '广东丰信饲料有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '800', '15.50', '12400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('304', '2023-02-03', '王林', '广东丰信饲料有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '700', '15.50', '10850.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('305', '2023-02-03', '邱桂雄', '广州市天河联华农业科技有限公司', '02D330', '盐酸多西环素可溶性粉', '销售发货', '100', '63.00', '6300.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('306', '2023-02-03', '邱桂雄', '广州市天河联华农业科技有限公司', '02D345', '亚硒酸钠维生素E预混剂', '销售发货', '500', '4.00', '2000.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('307', '2023-02-03', '邱桂雄', '广州市天河联华农业科技有限公司', '01D900', '霍香散', '销售发货', '300', '19.00', '5700.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('308', '2023-02-03', '谭永昌', '广州市众望饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '13.00', '6500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('309', '2023-02-03', '罗辉', '嘉吉动物营养饲料（济宁）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('310', '2023-02-03', '罗辉', '嘉吉饲料(佛山)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '18.00', '900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('311', '2023-02-03', '罗辉', '嘉吉饲料（泰安）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '75', '18.00', '1350.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('312', '2023-02-03', '罗辉', '嘉吉饲料（天津）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '8000', '12.30', '98400.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('313', '2023-02-03', '罗辉', '嘉吉饲料（漳州）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '500', '18.00', '9000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('314', '2023-02-03', '罗辉', '嘉吉饲料(长沙)有限公司汉川分公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('315', '2023-02-03', '杨平', '莒县张龙家庭农场', '01D267', '奇力宝（蛋壳素）', '销售发货', '250', '11.00', '2750.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('316', '2023-02-03', '刘云', '浏阳市湘宏生物科技饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '1000', '11.00', '11000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('317', '2023-02-03', '黄坤', '南京众成正源生物技术有限公司', '01D100', '天安硒', '销售发货', '20', '60.00', '1200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('318', '2023-02-03', '杨小乐', '内蒙古博鑫农牧业科技有限公司（廊坊嘉吉）', '01D616', '美多(反刍)', '销售发货', '150', '15.40', '2310.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('319', '2023-02-03', '杨小乐', '内蒙古博鑫农牧业科技有限公司（郑州嘉吉）', '01D616', '美多(反刍)', '销售发货', '25', '19.20', '480.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('320', '2023-02-03', '曲光', '山东邹平同裕饲料有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '125', '17.00', '2125.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('321', '2023-02-03', '曲光', '山东邹平同裕饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '450', '15.50', '6975.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('322', '2023-02-03', '曲光', '山东邹平同裕饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '200', '8.20', '1640.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('323', '2023-02-03', '黄坤', '扬州康立达生物科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '100', '17.50', '1750.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('324', '2023-02-03', '黄坤', '扬州涌泉生物技术有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '200', '24.00', '4800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('325', '2023-02-03', '黄坤', '扬州涌泉生物技术有限公司', '01D324', '微速强', '销售发货', '1000', '11.00', '11000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('326', '2023-02-03', '黄坤', '扬州涌泉生物技术有限公司', '01D317', '微速达（水产用）', '销售发货', '400', '18.00', '7200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('327', '2023-02-03', '黄坤', '扬州涌泉生物技术有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '2000', '3.60', '7200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('328', '2023-02-03', '黄正月', '重庆市铜梁区养殖户张杰', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '14.00', '7000.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('329', '2023-02-03', '王林', '珠海杨华彬', '01D334', '微速强', '销售发货', '100', '11.00', '1100.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('330', '2023-02-03', '王林', '珠海杨华彬', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('331', '2023-02-03', '何海涛', '葫芦岛众友饲料有限公司', '01D402', '抗氧灵', '销售发货', '600', '13.50', '8100.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('332', '2023-02-04', '何翔飞', '广州昭品饲料有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '10.10', '10100.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('333', '2023-02-04', '袁小波', '礼蓝（四川）动物保健有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '300', '64.00', '19200.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('334', '2023-02-04', '黄坤', '上海市奉贤区汪瑞水产服务部', '01D334', '微速强', '销售发货', '1540', '11.00', '16940.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('335', '2023-02-04', '黄坤', '上海市奉贤区汪瑞水产服务部', '01D782', '澳美维（水产用）', '销售发货', '510', '26.20', '13362.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('336', '2023-02-04', '黄坤', '泰州海兔生物科技有限公司', '01D333', '微速强', '销售发货', '1000', '11.00', '11000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('337', '2023-02-04', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D402', '抗氧灵', '销售发货', '1000', '9.50', '9500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('338', '2023-02-06', '谭永昌', '广州市丰收饲料添加剂有限公司', '01D621', '美多-II（禽用）', '销售发货', '200', '22.00', '4400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('339', '2023-02-06', '黎永华', '邢台梦成饲料有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '200', '31.50', '6300.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('340', '2023-02-06', '黎永华', '邢台梦成饲料有限公司', '01D043', '奇力锰（蛋氨酸锰150）', '销售发货', '400', '31.50', '12600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('341', '2023-02-06', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('342', '2023-02-06', '罗辉', '宿迁市立华牧业有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '2800', '25.10', '70280.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('343', '2023-02-06', '何翔飞', '广州市科迪生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '50', '8.20', '410.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('344', '2023-02-06', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1500', '12.30', '18450.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('345', '2023-02-06', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '6000', '7.50', '45000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('346', '2023-02-06', '王林', '珠海市德海生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '5000', '16.00', '80000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('347', '2023-02-06', '王林', '珠海市德海生物科技有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '1000', '34.00', '34000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('348', '2023-02-06', '王林', '珠海一村农业发展有限公司', '04D791', '一村·多维', '销售发货', '500', '34.50', '17250.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('349', '2023-02-06', '王林', '珠海一村农业发展有限公司', '04D337', '一村多矿', '销售发货', '1500', '14.50', '21750.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('350', '2023-02-07', '袁小波', '成都德力元商贸有限公司(重庆铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '8000', '10.65', '85200.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('351', '2023-02-07', '袁小波', '成都德力元商贸有限公司(成都铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '2000', '10.65', '21300.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('352', '2023-02-07', '罗辉', '嘉吉动物营养（南宁）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '10000', '12.30', '123000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('353', '2023-02-07', '罗辉', '嘉吉动物营养（南宁）有限公司', '01D014', '奇力铁', '销售发货', '2000', '7.80', '15600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('354', '2023-02-07', '罗辉', '嘉吉饲料（嘉兴）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '18.00', '1800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('355', '2023-02-07', '罗辉', '嘉吉饲料（嘉兴）有限公司', '01D014', '奇力铁', '销售发货', '150', '7.80', '1170.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('356', '2023-02-07', '杨荣国', '乌鲁木齐市新华锐饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '300', '13.50', '4050.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('357', '2023-02-07', '杨荣国', '乌鲁木齐市新华锐饲料有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '200', '18.00', '3600.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('358', '2023-02-07', '杨荣国', '乌鲁木齐市新华锐饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.50', '4250.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('359', '2023-02-07', '何翔飞', '广州昭品饲料有限公司（江门市桐美饲料有限公司）', '04D320', '奇力宝（水产用）', '销售发货', '5000', '4.15', '20750.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('360', '2023-02-07', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D624', '美多-CR', '销售发货', '40', '18.80', '752.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('361', '2023-02-07', '王林', '广州昭品饲料有限公司', '01D402', '抗氧灵', '销售发货', '100', '9.50', '950.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('362', '2023-02-07', '王林', '无锡汇择生物科技有限公司（江阴港虹饲料）', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '1000', '11.00', '11000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('363', '2023-02-07', '王林', '无锡汇择生物科技有限公司（江阴港虹饲料）', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '10.50', '10500.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('364', '2023-02-07', '王林', '新会于荣文', '01D782', '澳美维（水产用）', '销售发货', '225', '26.20', '5895.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('365', '2023-02-07', '张树军', '河南立赛饲料有限公司', '01E634', '微速达（畜禽用）', '销售发货', '500', '9.10', '4550.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('366', '2023-02-07', '张树军', '潍坊爱菲德饲料有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '200', '14.00', '2800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('367', '2023-02-07', '张树军', '潍坊爱菲德饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '300', '12.30', '3690.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('368', '2023-02-07', '谭永昌', '广东华红饲料科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.40', '6700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('369', '2023-02-07', '谭永昌', '广东华红饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '8.80', '17600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('370', '2023-02-07', '何海涛', '沈阳波音饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.50', '8500.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('371', '2023-02-07', '何海涛', '沈阳东正牧业有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '16.00', '16000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('372', '2023-02-07', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('373', '2023-02-07', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('374', '2023-02-08', '黎永华', '石家庄禾源生物科技有限公司', '01D777', '澳美维', '销售发货', '250', '33.00', '8250.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('375', '2023-02-08', '张明', '顺德杨先生', '01D334', '微速强', '销售发货', '40', '11.00', '440.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('376', '2023-02-08', '罗辉', '上海大冠饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.20', '8200.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('377', '2023-02-08', '杨锋', '福州江炜（天赐饲料）', '01D402', '抗氧灵', '销售发货', '500', '9.50', '4750.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('378', '2023-02-08', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '11250', '12.30', '138375.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('379', '2023-02-08', '鲁刚', '长春市环农饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '2000', '29.15', '58300.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('380', '2023-02-08', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '14000', '7.50', '105000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('381', '2023-02-08', '王林', '旺记药店', '01D309', '奇力宝海水甲壳动物用', '销售发货', '1000', '4.45', '4450.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('382', '2023-02-08', '王林', '江门市蓬江区海豚水族有限公司', '01D299', '奇力宝杂食性鱼用', '销售发货', '1000', '7.30', '7300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('383', '2023-02-08', '王林', '江门市蓬江区海豚水族有限公司', '01D296', '奇力宝－甲鱼用', '销售发货', '1000', '5.70', '5700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('384', '2023-02-08', '王林', '广东百安然生物工程有限公司', '01D317', '微速达（水产用）', '销售发货', '2000', '16.00', '32000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('385', '2023-02-08', '黄坤', '宝兰（南京）生物技术有限公司', '01D324', '微速强', '销售发货', '2000', '11.00', '22000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('386', '2023-02-08', '黄坤', '盐城渔乐乐生物科技有限公司', '01E733', '渔乐乐', '销售发货', '1000', '16.00', '16000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('387', '2023-02-08', '杨平', '山东常林饲料有限公司', '01D378', '奇力宝（肉牛.羊用）', '销售发货', '500', '13.00', '6500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('388', '2023-02-08', '黄正月', '四川金微健农牧科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '16.50', '16500.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('389', '2023-02-09', '邱桂雄', '潍坊中基饲料有限公司', '01D402', '抗氧灵', '销售发货', '3000', '12.80', '38400.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('390', '2023-02-09', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '9.05', '271500.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('391', '2023-02-09', '黎永华', '北京同力兴科农业科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '300', '13.80', '4140.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('392', '2023-02-09', '黎永华', '北京同力兴科农业科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '8.80', '17600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('393', '2023-02-09', '黎永华', '邢台熙轩商贸有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '25', '11.50', '287.50', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('394', '2023-02-09', '罗辉', '上海迦盟生物科技有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '500', '29.20', '14600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('395', '2023-02-09', '何翔飞', '广州海森沙农业发展有限公司', '01D317', '微速达（水产用）', '销售发货', '100', '14.10', '1410.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('396', '2023-02-09', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D420', '不来霉', '销售发货', '600', '7.50', '4500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('397', '2023-02-09', '王林', '佛山市顺德区丰华饲料实业有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '500', '12.00', '6000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('398', '2023-02-09', '王林', '广东丰信饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '2000', '12.00', '24000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('399', '2023-02-09', '张树军', '河南满意农牧科技有限公司', '01D262', '禽宝', '销售发货', '1000', '15.00', '15000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('400', '2023-02-09', '黄坤', '淮安市益牛饲料有限责任公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '240', '33.00', '7920.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('401', '2023-02-09', '黄坤', '淮安市益牛饲料有限责任公司', '01D043', '奇力锰（蛋氨酸锰150）', '销售发货', '300', '33.00', '9900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('402', '2023-02-09', '杨平', '莱芜非凡养殖场', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '14.00', '14000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('403', '2023-02-09', '黄正月', '南充市嘉陵区大台农饲料经营部', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '100', '18.50', '1850.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('404', '2023-02-09', '黄正月', '南充市嘉陵区大台农饲料经营部', '01D214', '奇力宝(仔猪用)', '销售发货', '400', '16.50', '6600.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('405', '2023-02-09', '杨小乐', '内蒙古优刍乐商贸有限公司', '01D624', '美多-CR', '销售发货', '1000', '19.50', '19500.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('406', '2023-02-10', '邱桂雄', '广州市天河联华农业科技有限公司', '01D792', '联华美多宝', '销售发货', '75', '18.20', '1365.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('407', '2023-02-10', '邱桂雄', '广州市天河联华农业科技有限公司', '01D772', '水溶性电解多维预混饲料', '销售发货', '1500', '18.00', '27000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('408', '2023-02-10', '邱桂雄', '广州市天河联华农业科技有限公司', '01D772', '水溶性电解多维预混饲料', '销售发货', '855', '18.00', '15390.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('409', '2023-02-10', '杨锋', '龙岩市百特饲料科技有限公司', '01D402', '抗氧灵', '销售发货', '3500', '16.00', '56000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('410', '2023-02-10', '何海涛', '英联饲料(辽宁)有限公司', '01D255', '奇力宝－蛋禽用', '销售发货', '600', '25.00', '15000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('411', '2023-02-10', '何海涛', '辽宁波尔莱特农牧实业有限公司', '01D402', '抗氧灵', '销售发货', '4000', '13.50', '54000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('412', '2023-02-10', '王林', '广东恒兴饲料实业股份有限公司遂溪分公司', '01D408', '抗氧灵', '销售发货', '5000', '12.50', '62500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('413', '2023-02-10', '王林', '中山市东升镇渔乐圈饲料经销店', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('414', '2023-02-10', '杨小乐', '内蒙古博鑫农牧业科技有限公司（北京三元）', '01D620', '美多（反刍动物用）', '销售发货', '5000', '16.90', '84500.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('415', '2023-02-10', '杨小乐', '内蒙古博鑫农牧业科技有限公司（北京三元）', '01D616', '美多(反刍)', '销售发货', '5000', '14.40', '72000.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('416', '2023-02-11', '邱桂雄', '武汉民族科技饲料有限公司', '01E756', '奇力宝（蛋禽用）', '销售发货', '1200', '13.30', '15960.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('417', '2023-02-11', '邱桂雄', '武汉民族科技饲料有限公司', '01E755', '奇力宝（蛋禽用-颗粒型）', '销售发货', '1200', '9.85', '11820.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('418', '2023-02-11', '袁小波', '重庆优宝生物技术股份有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '3000', '33.50', '100500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('419', '2023-02-11', '黎永华', '石家庄于淼', '01D214', '奇力宝(仔猪用)', '销售发货', '500', '13.50', '6750.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('420', '2023-02-11', '张明', '广东腾骏生物营养科技有限公司', '01D060', 'G/Zn-210', '销售发货', '200', '28.00', '5600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('421', '2023-02-11', '张明', '广东腾骏生物营养科技有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '200', '32.00', '6400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('422', '2023-02-11', '杨锋', '厦门渔雄越生物科技有限公司', '01D324', '微速强', '销售发货', '200', '9.50', '1900.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('423', '2023-02-11', '何海涛', '辽宁鑫祥跃生物科技有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.20', '26400.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('424', '2023-02-11', '何海涛', '辽宁鑫祥跃生物科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '14.30', '14300.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('425', '2023-02-11', '王林', '新会区睦洲镇万华饲料厂', '01D307', '奇力宝淡水甲壳动物用', '销售发货', '3000', '4.03', '12090.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('426', '2023-02-11', '王林', '珠海一村农业发展有限公司', '04D791', '一村·多维', '销售发货', '500', '34.50', '17250.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('427', '2023-02-11', '王林', '珠海一村农业发展有限公司', '04D337', '一村多矿', '销售发货', '500', '14.50', '7250.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('428', '2023-02-11', '张树军', '河南诺尔饲料科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '3000', '12.30', '36900.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('429', '2023-02-11', '张树军', '河南立赛饲料有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '500', '14.00', '7000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('430', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D781', '维稳宝', '销售发货', '250', '30.00', '7500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('431', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D317', '微速达（水产用）', '销售发货', '100', '18.00', '1800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('432', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D309', '奇力宝海水甲壳动物用', '销售发货', '1000', '5.80', '5800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('433', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D307', '奇力宝淡水甲壳动物用', '销售发货', '1000', '5.50', '5500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('434', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D060', 'G/Zn-210', '销售发货', '50', '34.00', '1700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('435', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D041', '天锰乐', '销售发货', '50', '33.00', '1650.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('436', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D034', '奇力铜（甘氨酸铜210）', '销售发货', '50', '63.00', '3150.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('437', '2023-02-11', '黄坤', '扬州涌泉生物技术有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '50', '33.00', '1650.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('438', '2023-02-11', '杨平', '山东中巴农牧发展有限公司', '01D779', '澳美维', '销售发货', '400', '50.00', '20000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('439', '2023-02-11', '曲光', '青岛嘉明牧业科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '250', '18.50', '4625.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('440', '2023-02-11', '曲光', '青岛嘉明牧业科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '250', '16.50', '4125.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('441', '2023-02-11', '吴春蕊', '广州昭品饲料有限公司', '01A262', '七水硫酸亚铁', '销售发货', '50', '2.00', '100.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('442', '2023-02-13', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '27375', '9.05', '247743.75', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('443', '2023-02-13', '谭永昌', '广州海生沅生物科技有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '200', '32.00', '6400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('444', '2023-02-13', '谭永昌', '广州海生沅生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '300', '8.20', '2460.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('445', '2023-02-13', '谭永昌', '海南海利发畜牧科技有限公司', '01D793', '澳美维（母猪用）', '销售发货', '200', '26.00', '5200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('446', '2023-02-13', '谭永昌', '海南海利发畜牧科技有限公司', '01D789', '澳美维（猪用）', '销售发货', '400', '26.00', '10400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('447', '2023-02-13', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '12000', '13.00', '156000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('448', '2023-02-13', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '20000', '12.60', '252000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('449', '2023-02-13', '黎永华', '河北新恒鑫饲料有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '300', '11.50', '3450.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('450', '2023-02-13', '罗辉', '嘉吉饲料（天津）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '15000', '12.30', '184500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('451', '2023-02-13', '杨锋', '安徽和明生农贸有限公司（安徽申亚）', '01D005', '甘氨酸铁17%', '销售发货', '5000', '8.00', '40000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('452', '2023-02-13', '刘云', '沈阳乐农氏动物营养有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.50', '6750.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('453', '2023-02-13', '刘云', '沈阳乐农氏动物营养有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.20', '8200.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('454', '2023-02-13', '何海涛', '艾地盟动物保健及营养（南京）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '15.50', '775.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('455', '2023-02-13', '何翔飞', '广州昭品饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '2000', '2.85', '5700.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('456', '2023-02-13', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '600', '11.50', '6900.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('457', '2023-02-13', '纳生', '新沂益客大地饲料有限公司', '01D464', '赛肥素I', '销售发货', '150', '59.00', '8850.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('458', '2023-02-14', '谭永昌', '肇庆端州张建辉', '04E026', '酸满宝', '销售发货', '30', '14.00', '420.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('459', '2023-02-14', '磨勤', '南宁市百禾生物科技有限公司(广西金陵农牧)', '01E740', '舒柠500', '销售发货', '1500', '46.00', '69000.00', '市场管理部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('460', '2023-02-14', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('461', '2023-02-14', '刘云', '吉林省金沃农牧科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '1000', '17.00', '17000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('462', '2023-02-14', '刘云', '昌图鹏程牧业有限公司', '01D505', '幼添宝', '销售发货', '1000', '12.00', '12000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('463', '2023-02-14', '刘云', '昌图鹏程牧业有限公司 （赠送）', '01D505', '幼添宝', '销售发货', '25', '11.80', '295.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('464', '2023-02-14', '鲁刚', '哈尔滨博微饲料制造有限责任公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '1000', '14.00', '14000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('465', '2023-02-14', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '3000', '10.60', '31800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('466', '2023-02-14', '李军勇', '泉州玛塔新材料有限公司', '01D100', '天安硒', '销售发货', '20', '50.00', '1000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('467', '2023-02-14', '李军勇', '泉州玛塔新材料有限公司', '01D060', 'G/Zn-210', '销售发货', '75', '30.00', '2250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('468', '2023-02-14', '李军勇', '泉州玛塔新材料有限公司', '01D041', '天锰乐', '销售发货', '50', '30.00', '1500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('469', '2023-02-14', '李军勇', '泉州玛塔新材料有限公司', '01D034', '奇力铜（甘氨酸铜210）', '销售发货', '25', '65.00', '1625.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('470', '2023-02-14', '李军勇', '泉州玛塔新材料有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '25', '28.00', '700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('471', '2023-02-14', '杨平', '山东佰士特宠物食品有限公司', '01D402', '抗氧灵', '销售发货', '500', '11.00', '5500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('472', '2023-02-14', '黄正月', '重庆积善农牧有限公司', '01D779', '澳美维', '销售发货', '100', '38.00', '3800.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('473', '2023-02-15', '邱桂雄', '广州市天河联华农业科技有限公司', '02D415', '阿莫西林', '销售发货', '200', '96.00', '19200.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('474', '2023-02-15', '邱桂雄', '广州市天河联华农业科技有限公司', '02D415', '阿莫西林', '销售发货', '100', '96.00', '9600.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('475', '2023-02-15', '龚波', '武汉正大有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '5000', '20.90', '104500.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('476', '2023-02-15', '龚波', '正大预混料（柳州）有限公司', '01D403', '抗氧灵', '销售发货', '1000', '12.10', '12100.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('477', '2023-02-15', '林雪', '巴基斯坦', '01D475', '天佳乐', '销售发货', '200', '0.00', '0.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('478', '2023-02-15', '林雪', '巴基斯坦', '01D461', '天佳乐', '销售发货', '3700', '45.23', '167336.47', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('479', '2023-02-15', '林雪', '越南（出口）', '04D061', '甘氨酸锌21%', '销售发货', '10000', '12.40', '123950.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('480', '2023-02-15', '杨锋', '江西一领药业有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.50', '4250.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('481', '2023-02-15', '杨锋', '江西三同生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.50', '4250.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('482', '2023-02-15', '何翔飞', '广东利健生物药业有限公司', '01D452', '天科宝（A-04-414）', '销售发货', '1000', '27.00', '27000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('483', '2023-02-15', '何翔飞', '广州市和生堂动物药业有限公司', '01D005', '甘氨酸铁17%', '销售发货', '100', '14.50', '1450.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('484', '2023-02-15', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '3000', '17.00', '51000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('485', '2023-02-15', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '04D291', '奇力宝（鳗鱼用）', '销售发货', '7000', '3.90', '27300.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('486', '2023-02-15', '杨平', '临沂宏牧饲料有限公司', '01D402', '抗氧灵', '销售发货', '500', '11.00', '5500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('487', '2023-02-15', '曲光', '济南农哈哈兽药有限公司', '01E758', '奇力宝（蛋壳素-颗粒型）', '销售发货', '25', '18.50', '462.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('488', '2023-02-15', '纳生', '宿迁益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '500', '59.00', '29500.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('489', '2023-02-15', '杨小乐', '内蒙古博鑫农牧业科技有限公司（巴盟根虎牧业）', '01D624', '美多-CR', '销售发货', '520', '18.40', '9568.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('490', '2023-02-16', '邱桂雄', '武汉民族科技饲料有限公司', '01E756', '奇力宝（蛋禽用）', '销售发货', '3800', '13.30', '50540.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('491', '2023-02-16', '邱桂雄', '武汉民族科技饲料有限公司', '01E755', '奇力宝（蛋禽用-颗粒型）', '销售发货', '3800', '9.85', '37430.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('492', '2023-02-16', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '9.05', '271500.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('493', '2023-02-16', '谭永昌', '广州快大饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '500', '36.50', '18250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('494', '2023-02-16', '谭永昌', '广州市猪王饲料有限公司', '01D211', '奇力宝猪用', '销售发货', '1000', '6.30', '6300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('495', '2023-02-16', '袁小波', '成都德力元商贸有限公司（昆明铁骑）', '04D214', '奇力宝（仔猪用）', '销售发货', '2000', '10.65', '21300.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('496', '2023-02-16', '袁小波', '成都德力元商贸有限公司(攀枝花铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '4000', '10.65', '42600.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('497', '2023-02-16', '黎永华', '北京百世腾饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.20', '4100.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('498', '2023-02-16', '黎永华', '石家庄厚森生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '200', '8.20', '1640.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('499', '2023-02-16', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '2000', '14.80', '29600.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('500', '2023-02-16', '龚波', '正大预混料（杭州）有限公司', '01D403', '抗氧灵', '销售发货', '2000', '12.10', '24200.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('501', '2023-02-16', '罗辉', '卜蜂（广东）生物科技有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '1000', '12.80', '12800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('502', '2023-02-16', '罗辉', '宿迁市立华牧业有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '1500', '25.00', '37500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('503', '2023-02-16', '杨锋', '南昌杨俊', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '50', '32.00', '1600.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('504', '2023-02-16', '刘云', '沈阳乐农氏动物营养有限公司', '04D061', '甘氨酸锌21%', '销售发货', '250', '13.50', '3375.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('505', '2023-02-16', '刘云', '沈阳乐农氏动物营养有限公司', '01D092', '奇力铬（蛋氨酸铬001）', '销售发货', '75', '11.00', '825.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('506', '2023-02-16', '刘云', '沈阳乐农氏动物营养有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '75', '18.00', '1350.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('507', '2023-02-16', '刘云', '沈阳乐农氏动物营养有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '25', '35.00', '875.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('508', '2023-02-16', '刘云', '吉林省金沃农牧科技有限公司', '01D228', '荘元红', '销售发货', '300', '26.00', '7800.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('509', '2023-02-16', '刘云', '吉林省金沃农牧科技有限公司', '01D213', '天籁（仔猪用）', '销售发货', '400', '28.00', '11200.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('510', '2023-02-16', '王林', '广东恒兴饲料实业股份有限公司遂溪分公司', '01D214', '奇力宝(仔猪用)', '销售发货', '5000', '23.50', '117500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('511', '2023-02-16', '张树军', '河南立赛饲料有限公司', '01E634', '微速达（畜禽用）', '销售发货', '125', '9.10', '1137.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('512', '2023-02-16', '张树军', '河南立赛饲料有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '125', '21.60', '2700.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('513', '2023-02-16', '李军勇', '东莞市银华生物科技有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '5000', '3.60', '18000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('514', '2023-02-16', '李军勇', '东莞市银华生物科技有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '100', '11.30', '1130.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('515', '2023-02-16', '黄坤', '福康田生物科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '40', '18.00', '720.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('516', '2023-02-16', '曲光', '姚元勇', '01D611', '美多－Ⅱ（禽用）', '销售发货', '250', '21.00', '5250.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('517', '2023-02-16', '曲光', '姚元勇', '01D274', '奇力宝（蛋禽用）', '销售发货', '250', '14.80', '3700.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('518', '2023-02-16', '纳生', '邢台益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '150', '59.00', '8850.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('519', '2023-02-17', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '1575', '12.60', '19845.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('520', '2023-02-17', '杨锋', '安徽和明生农贸有限公司（奎硕饲料厂）', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.20', '4100.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('521', '2023-02-17', '杨锋', '安徽和明生农贸有限公司(安徽宏亮)', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.20', '8200.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('522', '2023-02-17', '何海涛', '沈阳新华康饲料有限公司', '01D505', '幼添宝', '销售发货', '100', '12.50', '1250.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('523', '2023-02-17', '何海涛', '沈阳新华康饲料有限公司', '01D501', '强味酸(B）', '销售发货', '1000', '22.00', '22000.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('524', '2023-02-17', '何海涛', '鞍山市精艺农牧科技有限公司', '01D757', '天籁', '销售发货', '600', '30.00', '18000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('525', '2023-02-17', '何翔飞', '广州昭品饲料有限公司', '01A301', '一水硫酸镁', '销售发货', '250', '5.00', '1250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('526', '2023-02-17', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1000', '12.35', '12350.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('527', '2023-02-17', '鲁刚', '长春市环农饲料有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '1000', '12.90', '12900.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('528', '2023-02-17', '鲁刚', '长春市环农饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '500', '29.20', '14600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('529', '2023-02-17', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '3000', '7.55', '22650.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('530', '2023-02-17', '鲁刚', '哈尔滨远大牧业有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '5000', '22.30', '111500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('531', '2023-02-17', '王林', '中山苏的良', '01D782', '澳美维（水产用）', '销售发货', '75', '26.20', '1965.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('532', '2023-02-17', '王林', '中山苏的良', '01D317', '微速达（水产用）', '销售发货', '250', '14.10', '3525.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('533', '2023-02-17', '张树军', '河南优创生物技术有限公司', '01D787', '全乐美', '销售发货', '300', '43.00', '12900.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('534', '2023-02-17', '张树军', '河南优创生物技术有限公司', '01D778', '全乐美', '销售发货', '700', '43.00', '30100.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('535', '2023-02-17', '黄坤', '常州紫金生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('536', '2023-02-18', '袁小波', '礼蓝（四川）动物保健有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '100', '62.00', '6200.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('537', '2023-02-18', '罗辉', '嘉吉饲料（宜春）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '15000', '12.30', '184500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('538', '2023-02-18', '刘云', '武汉艾立生物科技有限公司', '01D779', '澳美维', '销售发货', '60', '36.00', '2160.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('539', '2023-02-18', '何翔飞', '江门市西瑞动物保健科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '3000', '10.10', '30300.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('540', '2023-02-18', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D781', '维稳宝', '销售发货', '25', '27.80', '695.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('541', '2023-02-18', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D781', '维稳宝', '销售发货', '25', '27.80', '695.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('542', '2023-02-18', '王林', '博罗朱文广', '01D295', '奇力宝-甲鱼用', '销售发货', '1500', '3.00', '4500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('543', '2023-02-18', '黄坤', '山西夏辰生物科技有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '20', '18.00', '360.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('544', '2023-02-18', '黄坤', '广东罗定市陈继成', '01D005', '甘氨酸铁17%', '销售发货', '25', '8.20', '205.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('545', '2023-02-18', '黄坤', '丰县普华农牧科技有限公司', '01D275', '奇力宝（肉禽用）', '销售发货', '2000', '11.50', '23000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('546', '2023-02-18', '杨平', '临沂金翠兽药有限公司', '01D777', '澳美维', '销售发货', '100', '38.00', '3800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('547', '2023-02-18', '杨平', '日照金鑫生态农业科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '13.00', '6500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('548', '2023-02-18', '纳生', '临沂众客饲料有限公司', '01D464', '赛肥素I', '销售发货', '300', '59.00', '17700.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('549', '2023-02-20', '邱桂雄', '广东科邦饲料科技有限公司', '01D230', '奇力宝（生长猪用）', '销售发货', '1000', '9.50', '9500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('550', '2023-02-20', '何海涛', '沈阳波音饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.50', '8500.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('551', '2023-02-20', '王林', '成都凯利来生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '14.10', '14100.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('552', '2023-02-20', '王林', '江门市新会区辽海饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '1550', '11.50', '17825.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('553', '2023-02-20', '王林', '江门市新会区辽海饲料有限公司', '01D331', '奇力宝（淡水鱼用）', '销售发货', '1800', '7.50', '13500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('554', '2023-02-20', '郑云瀚', '土耳其', '04D061', '甘氨酸锌21%', '销售发货', '10000', '15.01', '150080.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('555', '2023-02-20', '郑云瀚', '土耳其', '01D042', '天锰乐（G/Mn-220）', '销售发货', '8000', '17.35', '138824.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('556', '2023-02-20', '郑云瀚', '土耳其', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '2000', '32.50', '64990.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('557', '2023-02-20', '郑云瀚', '土耳其', '01D005', '甘氨酸铁17%', '销售发货', '4000', '10.45', '41808.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('558', '2023-02-20', '张树军', '潍坊爱菲德饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '1000', '9.60', '9600.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('559', '2023-02-20', '李军勇', '淮安渔富兴生物科技有限公司', '01D321', '奇力宝（淡水甲壳动物', '销售发货', '500', '8.00', '4000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('560', '2023-02-20', '黄正月', '四川金微健农牧科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '400', '18.50', '7400.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('561', '2023-02-20', '杨小乐', '内蒙古博鑫农牧业科技有限公司（宜春嘉吉）', '01D616', '美多(反刍)', '销售发货', '200', '15.15', '3030.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('562', '2023-02-21', '袁小波', '成都德力元商贸有限公司(广元铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '7000', '10.37', '72590.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('563', '2023-02-21', '张明', '佛山市顺德区容大饲料有限公司', '01D100', '天安硒', '销售发货', '20', '47.00', '940.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('564', '2023-02-21', '林雪', '马来西亚', '04D061', '甘氨酸锌21%', '销售发货', '4500', '18.63', '83817.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('565', '2023-02-21', '林雪', '马来西亚', '01D040', '奇力锰 甘氨酸锰22.0%', '销售发货', '1000', '24.59', '24589.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('566', '2023-02-21', '林雪', '马来西亚', '01D010', '奇力铁 甘氨酸铁16.5%', '销售发货', '4500', '13.40', '60300.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('567', '2023-02-21', '何海涛', '辽宁九州生物科技有限公司', '01D402', '抗氧灵', '销售发货', '1500', '13.49', '20235.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('568', '2023-02-21', '何海涛', '保定奥佰康饲料制造有限公司', '01D402', '抗氧灵', '销售发货', '600', '13.50', '8100.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('569', '2023-02-21', '何海涛', '山西波尔莱特农牧有限公司', '01D402', '抗氧灵', '销售发货', '600', '13.50', '8100.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('570', '2023-02-21', '何翔飞', '广州昭品饲料有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '25', '10.10', '252.50', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('571', '2023-02-21', '何翔飞', '广州昭品饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '500', '2.85', '1425.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('572', '2023-02-21', '何翔飞', '江门市桐美饲料有限公司', '04D320', '奇力宝（水产用）', '销售发货', '2000', '7.10', '14200.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('573', '2023-02-21', '王林', '新会于荣文', '01D782', '澳美维（水产用）', '销售发货', '375', '26.20', '9825.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('574', '2023-02-21', '张树军', '河南立赛饲料有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '500', '9.00', '4500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('575', '2023-02-21', '张树军', '潍坊爱菲德饲料有限公司', '01D420', '不来霉', '销售发货', '200', '7.50', '1500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('576', '2023-02-21', '张树军', '潍坊爱菲德饲料有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '200', '11.50', '2300.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('577', '2023-02-21', '张树军', '潍坊爱菲德饲料有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '200', '9.00', '1800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('578', '2023-02-21', '张树军', '山西争跃化工药业有限公司', '01D324', '微速强', '销售发货', '500', '13.00', '6500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('579', '2023-02-21', '杨平', '临沂鸿源饲料有限公司', '01D275', '奇力宝（肉禽用）', '销售发货', '500', '13.00', '6500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('580', '2023-02-21', '杨平', '临沂鸿源饲料有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '500', '16.00', '8000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('581', '2023-02-21', '杨平', '日照万顺禽业公司', '01D779', '澳美维', '销售发货', '40', '31.00', '1240.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('582', '2023-02-21', '何海涛', '辽宁波尔莱特农牧实业有限公司', '01D402', '抗氧灵', '销售发货', '2100', '13.50', '28350.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('583', '2023-02-21', '何海涛', '陕西波尔莱特农牧有限公司', '01D402', '抗氧灵', '销售发货', '200', '13.50', '2700.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('584', '2023-02-21', '何海涛', '吉林波尔莱特饲料有限公司', '01D402', '抗氧灵', '销售发货', '150', '13.50', '2025.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('585', '2023-02-21', '何海涛', '北京波尔莱特饲料有限公司', '01D402', '抗氧灵', '销售发货', '150', '13.50', '2025.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('586', '2023-02-21', '何海涛', '辽宁波尔莱特生物科技有限公司', '01D402', '抗氧灵', '销售发货', '100', '13.50', '1350.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('587', '2023-02-21', '何海涛', '黑龙江波尔莱特饲料有限公司', '01D402', '抗氧灵', '销售发货', '200', '13.50', '2700.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('588', '2023-02-21', '何海涛', '山东波尔莱特饲料有限公司', '01D402', '抗氧灵', '销售发货', '100', '13.50', '1350.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('589', '2023-02-22', '邱桂雄', '广州市天河联华农业科技有限公司', '02D413', '环丙氨嗪预混剂1%', '销售退货', '-190', '7.50', '-1425.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('590', '2023-02-22', '龚波', '正大预混料（天津）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '12700', '20.90', '265430.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('591', '2023-02-22', '何海涛', '沈阳腾和商贸有限公司', '01D262', '禽宝', '销售发货', '1500', '13.00', '19500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('592', '2023-02-22', '何海涛', '沈阳腾和商贸有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '125', '14.00', '1750.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('593', '2023-02-22', '何海涛', '沈阳腾和商贸有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '125', '12.30', '1537.50', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('594', '2023-02-22', '何海涛', '沈阳腾和商贸有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '150', '9.60', '1440.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('595', '2023-02-22', '何海涛', '沈阳腾和商贸有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '260', '26.50', '6890.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('596', '2023-02-22', '何海涛', '沈阳腾和商贸有限公司', '01D011', '奇力铁预混料', '销售发货', '175', '8.20', '1435.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('597', '2023-02-22', '何海涛', '沈阳丰美生物技术有限公司', '01D090', '蛋氨酸铬3.0%', '销售发货', '15', '74.00', '1110.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('598', '2023-02-22', '何翔飞', '广州市和生堂动物药业有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '18.50', '9250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('599', '2023-02-22', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D402', '抗氧灵', '销售发货', '1000', '9.50', '9500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('600', '2023-02-22', '王林', '新会杨华斌', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('601', '2023-02-22', '王林', '无锡汇择生物科技有限公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '100', '17.00', '1700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('602', '2023-02-22', '王林', '龙阳动物药业', '01D317', '微速达（水产用）', '销售发货', '500', '14.10', '7050.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('603', '2023-02-22', '张树军', '潍坊爱菲德饲料有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '300', '14.00', '4200.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('604', '2023-02-22', '张树军', '潍坊爱菲德饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '9.60', '4800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('605', '2023-02-22', '李军勇', '惠州市华宝饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.50', '8500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('606', '2023-02-22', '李军勇', '百达（广州）生物科技有限公司', '01E720', '肝美多', '销售发货', '50', '10.00', '500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('607', '2023-02-22', '李军勇', '广东归渔生物科技有限公司', '01D333', '微速强', '销售发货', '250', '9.00', '2250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('608', '2023-02-22', '黄坤', '扬州涌泉生物技术有限公司', '01D100', '天安硒', '销售发货', '100', '60.00', '6000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('609', '2023-02-22', '黄坤', '扬州绿多邦生物有限公司', '01E753', '维稳宝（Ⅱ型）', '销售发货', '360', '14.00', '5040.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('610', '2023-02-22', '杨小乐', '内蒙古博鑫农牧业科技有限公司（现代商河）', '01D620', '美多（反刍动物用）', '销售发货', '1000', '16.90', '16900.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('611', '2023-02-22', '杨小乐', '内蒙古博鑫农牧业科技有限公司（现代商河）', '01D616', '美多(反刍)', '销售发货', '1500', '14.40', '21600.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('612', '2023-02-23', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '30000', '9.05', '271500.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('613', '2023-02-23', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '1500', '9.35', '14025.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('614', '2023-02-23', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D226', '奇力宝（母猪用）', '销售发货', '600', '13.35', '8010.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('615', '2023-02-23', '袁小波', '礼蓝（四川）动物保健有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '200', '62.00', '12400.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('616', '2023-02-23', '袁小波', '礼蓝（四川）动物保健有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '300', '33.00', '9900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('617', '2023-02-23', '黎永华', '河北中贝佳美生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '13.00', '6500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('618', '2023-02-23', '黎永华', '天津天富莱溢佳生物科技有限公司武清分公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '100', '32.00', '3200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('619', '2023-02-23', '黎永华', '天津天富莱溢佳生物科技有限公司武清分公司', '01D043', '奇力锰（蛋氨酸锰150）', '销售发货', '100', '32.00', '3200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('620', '2023-02-23', '黎永华', '天津天富莱溢佳生物科技有限公司武清分公司', '01D005', '甘氨酸铁17%', '销售发货', '300', '8.20', '2460.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('621', '2023-02-23', '罗辉', '宿迁市立华牧业有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '2500', '25.00', '62500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('622', '2023-02-23', '杨荣国', '河南广安生物科技股份有限公司', '01D402', '抗氧灵', '销售发货', '1000', '12.00', '12000.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('623', '2023-02-23', '何海涛', '鞍山市腾鳌东鹏饲料有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '100', '13.30', '1330.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('624', '2023-02-23', '何海涛', '大连韩伟养鸡有限公司旅顺分公司', '01D409', '抗氧灵-T', '销售发货', '25', '20.00', '500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('625', '2023-02-23', '李军勇', '厦门海之润生物技术有限公司', '01D452', '天科宝（A-04-414）', '销售发货', '200', '25.00', '5000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('626', '2023-02-23', '杨平', '山东中巴农牧发展有限公司', '01D779', '澳美维', '销售发货', '100', '50.00', '5000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('627', '2023-02-23', '黄正月', '成都丰华饲料有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '500', '27.50', '13750.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('628', '2023-02-23', '黄正月', '成都丰华饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.20', '4100.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('629', '2023-02-23', '曲光', '安丘市春雨养殖场', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '14.80', '14800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('630', '2023-02-23', '曲光', '滨州市科正饲料有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '14.80', '14800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('631', '2023-02-24', '谭永昌', '肇庆市华饲鱼粉有限公司', '01D402', '抗氧灵', '销售发货', '500', '12.00', '6000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('632', '2023-02-24', '袁小波', '成都德力元商贸有限公司（绥化御咖牧业）', '04D214', '奇力宝（仔猪用）', '销售发货', '2000', '10.37', '20740.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('633', '2023-02-24', '何翔飞', '佛山市顺德区广添饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '1000', '3.00', '3000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('634', '2023-02-24', '何翔飞', '广州昭品饲料有限公司', '01D420', '不来霉', '销售发货', '300', '7.50', '2250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('635', '2023-02-24', '何翔飞', '广州昭品饲料有限公司', '01D402', '抗氧灵', '销售发货', '400', '9.50', '3800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('636', '2023-02-24', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '13000', '12.30', '159900.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('637', '2023-02-24', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '250', '12.30', '3075.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('638', '2023-02-24', '鲁刚', '长春市环农饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '1000', '29.15', '29150.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('639', '2023-02-24', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '13000', '7.50', '97500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('640', '2023-02-24', '王林', '台山市海师傅环境科技有限公司', '01D317', '微速达（水产用）', '销售发货', '200', '16.50', '3300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('641', '2023-02-24', '王林', '江门市新会区辽海饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '1450', '11.50', '16675.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('642', '2023-02-24', '王林', '江门市新会区辽海饲料有限公司', '01D331', '奇力宝（淡水鱼用）', '销售发货', '1200', '7.50', '9000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('643', '2023-02-24', '王林', '湛江安美生物科技有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '40', '28.00', '1120.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('644', '2023-02-24', '王林', '湛江安美生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '500', '17.00', '8500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('645', '2023-02-24', '黄坤', '江苏合纵思远生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('646', '2023-02-24', '黄坤', '江苏合纵思远生物科技有限公司（赠送）', '01D317', '微速达（水产用）', '销售发货', '50', '14.10', '705.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('647', '2023-02-24', '黄坤', '徐州中远生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '300', '12.80', '3840.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('648', '2023-02-25', '龚波', '正大预混料（天津）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '7300', '20.90', '152570.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('649', '2023-02-25', '杨锋', '福建省漳州市华龙饲料有限公司', '01D402', '抗氧灵', '销售发货', '3000', '16.00', '48000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('650', '2023-02-25', '杨锋', '江西锦汉药用辅料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '50', '9.00', '450.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('651', '2023-02-25', '何翔飞', '佛山市南海禅泰动物药业有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '25.00', '1250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('652', '2023-02-25', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '15.00', '1500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('653', '2023-02-27', '谭永昌', '广州海生沅生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '25', '13.30', '332.50', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('654', '2023-02-27', '谭永昌', '广州市百山制药有限公司', '01D317', '微速达（水产用）', '销售发货', '25', '15.00', '375.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('655', '2023-02-27', '谭永昌', '广州汉坤生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '25', '15.00', '375.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('656', '2023-02-27', '黎永华', '中粮（北京）饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '5000', '7.40', '37000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('657', '2023-02-27', '罗辉', '上海迦盟生物科技有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '500', '29.20', '14600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('658', '2023-02-27', '林雪', '越南（出口）', '05D003', '奇力宝（仔猪用）', '销售发货', '4000', '11.06', '44220.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('659', '2023-02-27', '林雪', '越南（出口）', '04D061', '甘氨酸锌21%', '销售发货', '5000', '12.40', '61975.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('660', '2023-02-27', '林雪', '越南（出口）', '01D042', '天锰乐（G/Mn-220）', '销售发货', '3000', '12.40', '37185.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('661', '2023-02-27', '林雪', '越南（出口）', '01D005', '甘氨酸铁17%', '销售发货', '8000', '8.04', '64320.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('662', '2023-02-27', '杨锋', '福建金海顺生物科技有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '40', '21.00', '840.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('663', '2023-02-27', '杨锋', '福建金海顺生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '250', '18.00', '4500.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('664', '2023-02-27', '杨锋', '福建海合生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '400', '14.10', '5640.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('665', '2023-02-27', '何海涛', '天津九州大地饲料有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.49', '26980.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('666', '2023-02-27', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '2000', '12.30', '24600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('667', '2023-02-27', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '4000', '7.50', '30000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('668', '2023-02-27', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('669', '2023-02-27', '黄坤', '江苏三仪动物营养科技有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '200', '33.50', '6700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('670', '2023-02-27', '黄坤', '扬州海丰生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1500', '18.00', '27000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('671', '2023-02-27', '黄坤', '江苏合纵思远生物科技有限公司', '01D781', '维稳宝', '销售发货', '500', '31.00', '15500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('672', '2023-02-27', '杨平', '莒南县奥美特饲料有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '1000', '10.00', '10000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('673', '2023-02-27', '曲光', '滨州海迈德动物营养有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '18.00', '9000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('674', '2023-02-27', '曲光', '济南杨庆良', '01D274', '奇力宝（蛋禽用）', '销售发货', '100', '13.80', '1380.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('675', '2023-02-28', '谭永昌', '广州纳欣生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.30', '6650.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('676', '2023-02-28', '谭永昌', '广州纳欣生物科技有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '500', '15.50', '7750.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('677', '2023-02-28', '谭永昌', '广州纳欣生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.50', '8500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('678', '2023-02-28', '谭永昌', '四会市惠诚动物保健有限公司', '01D779', '澳美维', '销售发货', '100', '31.00', '3100.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('679', '2023-02-28', '谭永昌', '罗定市牧丰兽药经营部', '01D779', '澳美维', '销售发货', '60', '31.00', '1860.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('680', '2023-02-28', '黎永华', '石家庄靖农生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '11.50', '5750.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('681', '2023-02-28', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('682', '2023-02-28', '龚波', '正大预混料（柳州）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '1500', '20.90', '31350.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('683', '2023-02-28', '宁会', '莒县新特瑞饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '12.00', '6000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('684', '2023-02-28', '杨锋', '赣州澳德饲料科技有限公司', '01D206', '奇力宝（母猪用）', '销售发货', '2000', '17.80', '35600.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('685', '2023-02-28', '杨锋', '赣州澳德饲料科技有限公司（赠送）', '01D206', '奇力宝（母猪用）', '销售发货', '50', '15.00', '750.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('686', '2023-02-28', '何海涛', '鞍山兴达伟业矿物质科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '600', '13.50', '8100.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('687', '2023-02-28', '何海涛', '鞍山兴达伟业矿物质科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1200', '8.50', '10200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('688', '2023-02-28', '王林', '湛江帮成生物', '01D454', '天科宝', '销售发货', '100', '19.00', '1900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('689', '2023-02-28', '张树军', '河南安信畜牧科技发展有限公司', '01D777', '澳美维', '销售发货', '100', '35.50', '3550.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('690', '2023-02-28', '张树军', '河南安信畜牧科技发展有限公司', '01D618', '美多-II（畜禽用）', '销售发货', '400', '22.00', '8800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('691', '2023-02-28', '黄坤', '扬州绿多邦生物有限公司', '01E753', '维稳宝（Ⅱ型）', '销售发货', '640', '14.00', '8960.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('692', '2023-02-28', '杨平', '山东中巴农牧发展有限公司', '01D779', '澳美维', '销售发货', '700', '50.00', '35000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('693', '2023-02-28', '杨小乐', '内蒙古博鑫农牧业科技有限公司（长春博瑞科技）', '01D616', '美多(反刍)', '销售发货', '250', '14.88', '3720.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('694', '2023-03-01', '张明', '佛山市顺德区旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '3000', '14.80', '44400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('695', '2023-03-01', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '14.80', '14800.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('696', '2023-03-01', '罗辉', '嘉吉饲料（阳江）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('697', '2023-03-01', '林雪', '印度', '05D017', '奇力宝（鸡用）', '销售发货', '20000', '9.05', '180900.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('698', '2023-03-01', '刘云', '长沙湘如意生物科技有限公司', '01D779', '澳美维', '销售发货', '40', '36.00', '1440.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('699', '2023-03-01', '何海涛', '沈阳波音饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '2000', '8.50', '17000.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('700', '2023-03-01', '王林', '珠海孙宝宝', '01D334', '微速强', '销售发货', '20', '11.00', '220.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('701', '2023-03-01', '张树军', '河南立赛饲料有限公司', '01D277', '奇力宝（蛋壳素）', '销售发货', '500', '10.80', '5400.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('702', '2023-03-01', '张树军', '河南立赛饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '200', '8.20', '1640.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('703', '2023-03-01', '黄坤', '江苏双环农牧科技有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '1000', '9.80', '9800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('704', '2023-03-01', '黄坤', '南通快诚生物科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '200', '16.00', '3200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('705', '2023-03-01', '黄坤', '南通快诚生物科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '300', '15.00', '4500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('706', '2023-03-01', '黄坤', '扬州海丰生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1500', '18.00', '27000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('707', '2023-03-01', '黄坤', '江苏合纵思远生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('708', '2023-03-01', '黄坤', '江苏合纵思远生物科技有限公司（赠送）', '01D317', '微速达（水产用）', '销售发货', '50', '14.10', '705.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('709', '2023-03-02', '谭永昌', '广东华红饲料科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1000', '13.40', '13400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('710', '2023-03-02', '谭永昌', '广东华红饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '8.80', '17600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('711', '2023-03-02', '袁小波', '成都德力元商贸有限公司（宁夏铁骑）', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '1000', '9.35', '9350.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('712', '2023-03-02', '黎永华', '河北中贝佳美生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1500', '13.00', '19500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('713', '2023-03-02', '张明', '茂名海大生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '100', '7.80', '780.01', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('714', '2023-03-02', '张明', '云南海瑞生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '7.80', '7800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('715', '2023-03-02', '何海涛', '辽宁九州生物科技有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.49', '26980.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('716', '2023-03-02', '何海涛', '辽宁众友饲料有限公司', '01D402', '抗氧灵', '销售发货', '1000', '13.50', '13500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('717', '2023-03-02', '王林', '广东恒兴饲料实业股份有限公司遂溪分公司', '01D100', '天安硒', '销售发货', '3160', '56.50', '178540.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('718', '2023-03-02', '王林', '广东东牧海洋生物技术开发有限公司', '01D324', '微速强', '销售发货', '1000', '8.80', '8800.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('719', '2023-03-02', '王林', '江门年丰生态渔业有限公司', '01D324', '微速强', '销售发货', '1000', '15.00', '15000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('720', '2023-03-02', '王林', '江门年丰生态渔业有限公司', '01D324', '微速强', '销售发货', '500', '15.00', '7500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('721', '2023-03-02', '黄坤', '江苏富裕达粮食制品股份有限公司', '04D061', '甘氨酸锌21%', '销售发货', '2000', '15.00', '30000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('722', '2023-03-02', '黄坤', '江苏富裕达粮食制品股份有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '1500', '35.00', '52500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('723', '2023-03-02', '黄坤', '徐州中远生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '700', '12.80', '8960.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('724', '2023-03-02', '黄坤', '徐州中远生物科技有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '1000', '9.80', '9800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('725', '2023-03-02', '曲光', '安丘市春雨养殖场', '01D274', '奇力宝（蛋禽用）', '销售发货', '2000', '14.80', '29600.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('726', '2023-03-02', '曲光', '赛孚特生物科技（潍坊）有限公司', '01D402', '抗氧灵', '销售发货', '500', '10.50', '5250.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('727', '2023-03-03', '邱桂雄', '武汉民族科技饲料有限公司', '01E756', '奇力宝（蛋禽用）', '销售发货', '4000', '13.30', '53200.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('728', '2023-03-03', '邱桂雄', '武汉民族科技饲料有限公司', '01E755', '奇力宝（蛋禽用-颗粒型）', '销售发货', '6000', '9.85', '59100.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('729', '2023-03-03', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D214', '奇力宝（仔猪用）', '销售发货', '5000', '10.37', '51850.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('730', '2023-03-03', '袁小波', '成都德力元商贸有限公司（昆明铁骑）', '04D214', '奇力宝（仔猪用）', '销售发货', '2000', '10.37', '20740.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('731', '2023-03-03', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D226', '奇力宝（母猪用）', '销售发货', '1000', '13.35', '13350.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('732', '2023-03-03', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '13000', '10.37', '134810.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('733', '2023-03-03', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '6000', '13.00', '78000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('734', '2023-03-03', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '26000', '12.60', '327600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('735', '2023-03-03', '张明', '茂名市福中福生物科技有限公司', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '500', '16.90', '8450.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('736', '2023-03-03', '龚波', '武汉正大有限公司', '01D409', '抗氧灵-T', '销售发货', '1000', '22.90', '22900.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('737', '2023-03-03', '龚波', '北京正大饲料有限公司', '01D409', '抗氧灵-T', '销售发货', '2000', '22.90', '45800.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('738', '2023-03-03', '何海涛', '四川大成农牧科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '12.14', '24280.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('739', '2023-03-03', '何海涛', '大成万达(天津)有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '12.14', '24280.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('740', '2023-03-03', '何海涛', '沈阳市农祥牧业科技有限公司', '01D420', '不来霉', '销售发货', '1000', '8.50', '8500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('741', '2023-03-03', '王林', '福建莆田李宜贤', '04D303', '奇力宝（海水鱼用）', '销售发货', '100', '10.10', '1010.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('742', '2023-03-03', '王林', '福建莆田李宜贤', '01D781', '维稳宝', '销售发货', '100', '27.80', '2780.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('743', '2023-03-03', '张树军', '南阳锦鼎饲料有限公司', '01D402', '抗氧灵', '销售发货', '1000', '11.00', '11000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('744', '2023-03-03', '李军勇', '诏安县小纪水产药品经营部', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('745', '2023-03-03', '杨平', '莒县张龙家庭农场（杨平赠送）', '01D267', '奇力宝（蛋壳素）', '销售发货', '25', '10.00', '250.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('746', '2023-03-03', '杨平', '莒县张龙家庭农场', '01D267', '奇力宝（蛋壳素）', '销售发货', '300', '11.00', '3300.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('747', '2023-03-03', '杨小乐', '内蒙古博鑫农牧业科技有限公司（宜春嘉吉）', '01D616', '美多(反刍)', '销售发货', '600', '14.40', '8640.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('748', '2023-03-04', '邱桂雄', '广州市天河联华农业科技有限公司', '02D330', '盐酸多西环素可溶性粉', '销售发货', '380', '63.00', '23940.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('749', '2023-03-04', '邱桂雄', '广州市天河联华农业科技有限公司', '02D328', '虫净', '销售发货', '160', '38.00', '6080.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('750', '2023-03-04', '邱桂雄', '广东科邦饲料科技有限公司', '01D230', '奇力宝（生长猪用）', '销售发货', '1500', '9.50', '14250.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('751', '2023-03-04', '邱桂雄', '广东科邦饲料科技有限公司', '01D213', '天籁（仔猪用）', '销售发货', '200', '24.50', '4900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('752', '2023-03-04', '罗辉', '淮安美标饲料有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '500', '15.50', '7750.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('753', '2023-03-04', '罗辉', '淮安美标饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '7.80', '15600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('754', '2023-03-04', '杨锋', '高安猪太傅饲料有限公司', '01D207', '母猪用微量元素预混合饲料', '销售发货', '2000', '18.70', '37400.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('755', '2023-03-04', '杨荣国', '河南广安生物科技股份有限公司', '01D501', '强味酸(B）', '销售发货', '1000', '23.00', '23000.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('756', '2023-03-04', '何翔飞', '佛山市顺德区广添饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '5000', '3.00', '15000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('757', '2023-03-04', '何翔飞', '佛山市南海禅泰动物药业有限公司', '04D061', '甘氨酸锌21%', '销售发货', '100', '19.50', '1950.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('758', '2023-03-04', '何翔飞', '佛山市南海禅泰动物药业有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '50', '37.00', '1850.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('759', '2023-03-04', '王林', '珠海一村农业发展有限公司', '04D791', '一村·多维', '销售发货', '500', '34.50', '17250.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('760', '2023-03-04', '王林', '珠海一村农业发展有限公司', '04D337', '一村多矿', '销售发货', '1000', '14.50', '14500.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('761', '2023-03-04', '黄坤', '江苏合纵思远生物科技有限公司', '01D781', '维稳宝', '销售发货', '500', '31.00', '15500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('762', '2023-03-04', '杨平', '山东三兴禽业有限公司', '01D779', '澳美维', '销售发货', '800', '50.00', '40000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('763', '2023-03-06', '黎永华', '石家庄海贝饲料科技有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '1000', '13.50', '13500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('764', '2023-03-06', '黎永华', '北京九州大地生物技术集团股份有限公司天津分公司', '01D090', '蛋氨酸铬3.0%', '销售发货', '150', '51.99', '7798.50', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('765', '2023-03-06', '黎永华', '北京九州大地生物技术集团股份有限公司天津分公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '1000', '15.00', '15000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('766', '2023-03-06', '龚波', '武汉正大有限公司', '01D409', '抗氧灵-T', '销售发货', '3000', '22.90', '68700.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('767', '2023-03-06', '林雪', '乌拉圭', '04D061', '甘氨酸锌21%', '销售发货', '1000', '24.05', '24053.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('768', '2023-03-06', '林雪', '乌拉圭', '01D450', '天味乐', '销售发货', '195', '114.24', '22275.83', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('769', '2023-03-06', '杨锋', '江西中杰生物科技有限公司', '01E634', '微速达（畜禽用）', '销售发货', '200', '14.00', '2800.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('770', '2023-03-06', '杨锋', '江西中杰生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.20', '8200.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('771', '2023-03-06', '何海涛', '辽宁波尔莱特农牧实业有限公司', '01D402', '抗氧灵', '销售发货', '5000', '13.50', '67500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('772', '2023-03-06', '何翔飞', '佛山市顺德区成冠饲料有限公司', '04D291', '奇力宝（鳗鱼用）', '销售发货', '5000', '3.90', '19500.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('773', '2023-03-06', '何翔飞', '广州昭品饲料有限公司（光辉水产）', '04D291', '奇力宝（鳗鱼用）', '销售发货', '5000', '3.90', '19500.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('774', '2023-03-06', '何翔飞', '广州昭品饲料有限公司（光辉水产）', '01D781', '维稳宝', '销售发货', '2000', '28.00', '56000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('775', '2023-03-06', '王林', '中山市家辉饲料有限公司', '01D309', '奇力宝海水甲壳动物用', '销售发货', '1000', '5.20', '5200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('776', '2023-03-06', '李军勇', '武汉高龙饲料有限公司', '01D296', '奇力宝－甲鱼用', '销售发货', '3000', '5.00', '15000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('777', '2023-03-06', '杨平', '五莲县鑫福饲料有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '200', '15.00', '3000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('778', '2023-03-06', '曲光', '姚元勇', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '14.80', '14800.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('779', '2023-03-06', '纳生', '宿迁益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '900', '58.50', '52650.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('780', '2023-03-06', '纳生', '南宁市百禾生物科技有限公司（广西大富华农牧）', '01E740', '舒柠500', '销售发货', '1200', '46.00', '55200.00', '市场管理部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('781', '2023-03-06', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.20', '8200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('782', '2023-03-06', '鲁刚', '长春市环农饲料有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '1000', '12.85', '12850.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('783', '2023-03-06', '鲁刚', '长春市环农饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '2000', '29.15', '58300.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('784', '2023-03-06', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '3000', '7.50', '22500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('785', '2023-03-07', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '25000', '9.05', '226250.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('786', '2023-03-07', '谭永昌', '广州澳升农业科技有限公司', '01D456', '天科宝（F-05-120）', '销售发货', '40', '18.00', '720.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('787', '2023-03-07', '谭永昌', '广州澳升农业科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '100', '21.00', '2100.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('788', '2023-03-07', '谭永昌', '广州澳升农业科技有限公司', '01D092', '奇力铬（蛋氨酸铬001）', '销售发货', '50', '11.00', '550.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('789', '2023-03-07', '谭永昌', '广州金水动物保健品有限公司', '04D061', '甘氨酸锌21%', '销售发货', '800', '22.70', '18160.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('790', '2023-03-07', '谭永昌', '广州金水动物保健品有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '300', '26.00', '7800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('791', '2023-03-07', '谭永昌', '广州金水动物保健品有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '500', '38.50', '19250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('792', '2023-03-07', '谭永昌', '广州金水动物保健品有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '16.70', '16700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('793', '2023-03-07', '袁小波', '成都德力元商贸有限公司(攀枝花铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '3000', '10.37', '31110.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('794', '2023-03-07', '袁小波', '成都申利行生物科技有限公司', '01D402', '抗氧灵', '销售发货', '5000', '9.50', '47500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('795', '2023-03-07', '张明', '佛山市顺德区兄弟德力饲料实业有限公司', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '1000', '16.90', '16900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('796', '2023-03-07', '罗辉', '卜蜂（河北）生物科技有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '200', '12.80', '2560.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('797', '2023-03-07', '罗辉', '卜蜂（河北）生物科技有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '300', '29.00', '8700.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('798', '2023-03-07', '罗辉', '卜蜂（河北）生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '7.50', '15000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('799', '2023-03-07', '林雪', '巴基斯坦', '05D025', '奇力宝（反刍动物用）', '销售发货', '2800', '11.19', '31329.20', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('800', '2023-03-07', '林雪', '巴基斯坦', '01D461', '天佳乐', '销售发货', '3000', '45.23', '135675.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('801', '2023-03-07', '林雪', '秘鲁（出口）', '02D310', '高乐威50%', '销售发货', '4000', '253.13', '1012504.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('802', '2023-03-07', '杨锋', '厦门渔雄越生物科技有限公司', '01E802', '速达康', '销售发货', '1100', '9.50', '10450.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('803', '2023-03-07', '杨锋', '厦门渔雄越生物科技有限公司', '01D454', '天科宝', '销售发货', '20', '20.00', '400.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('804', '2023-03-07', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '04D061', '甘氨酸锌21%', '销售发货', '200', '13.50', '2700.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('805', '2023-03-07', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '150', '15.50', '2325.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('806', '2023-03-07', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '50', '32.00', '1600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('807', '2023-03-07', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '01D011', '奇力铁预混料', '销售发货', '200', '8.20', '1640.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('808', '2023-03-07', '何海涛', '张家口九州大地饲料有限公司', '01D402', '抗氧灵', '销售发货', '100', '13.49', '1349.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('809', '2023-03-07', '何海涛', '鞍山市精艺农牧科技有限公司', '01D757', '天籁', '销售发货', '480', '30.00', '14400.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('810', '2023-03-07', '鲁刚', '哈尔滨市联丰饲料有限公司', '01D402', '抗氧灵', '销售发货', '1000', '11.30', '11300.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('811', '2023-03-07', '鲁刚', '哈尔滨市联丰饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1500', '20.50', '30750.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('812', '2023-03-07', '王林', '珠海市德海生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '5000', '16.00', '80000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('813', '2023-03-07', '王林', '广东明辉饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '1500', '10.50', '15750.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('814', '2023-03-07', '王林', '广东明辉饲料有限公司', '01D331', '奇力宝（淡水鱼用）', '销售发货', '2800', '7.00', '19600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('815', '2023-03-07', '王林', '广东明辉饲料有限公司', '01D305', '奇力宝（蛙用）', '销售发货', '500', '14.50', '7250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('816', '2023-03-07', '黄正月', '成都蜀星饲料有限公司', '01D043', '奇力锰（蛋氨酸锰150）', '销售发货', '500', '25.70', '12850.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('817', '2023-03-07', '杨小乐', '内蒙古博鑫农牧业科技有限公司（内蒙古隆华金源）', '01D624', '美多-CR', '销售发货', '1000', '18.40', '18400.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('818', '2023-03-08', '龚波', '吉林正大实业有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '1000', '20.70', '20700.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('819', '2023-03-08', '龚波', '吉林正大实业有限公司', '01D403', '抗氧灵', '销售发货', '1500', '12.10', '18150.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('820', '2023-03-08', '鲁刚', '哈尔滨远大牧业有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '10000', '22.30', '223000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('821', '2023-03-08', '王林', '珠海孙宝宝', '01D781', '维稳宝', '销售发货', '25', '28.00', '700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('822', '2023-03-08', '黄坤', '扬州市绿健生物技术有限公司', '01D317', '微速达（水产用）', '销售发货', '500', '18.00', '9000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('823', '2023-03-08', '黄坤', '南京禾嘉农牧科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '100', '13.30', '1330.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('824', '2023-03-08', '黄坤', '南京禾嘉农牧科技有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '150', '16.00', '2400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('825', '2023-03-08', '黄坤', '南京禾嘉农牧科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '250', '8.20', '2050.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('826', '2023-03-08', '曲光', '青岛山美生态农业有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '15.50', '15500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('827', '2023-03-08', '纳生', '临沂众客饲料有限公司', '01D464', '赛肥素I', '销售发货', '400', '58.50', '23400.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('828', '2023-03-08', '罗辉', '宿迁市立华牧业有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '4000', '24.50', '98000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('829', '2023-03-08', '杨小乐', '内蒙古博鑫农牧业科技有限公司（北京三元）', '01D620', '美多（反刍动物用）', '销售发货', '5000', '16.90', '84500.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('830', '2023-03-08', '杨小乐', '内蒙古博鑫农牧业科技有限公司（北京三元）', '01D616', '美多(反刍)', '销售发货', '5000', '14.40', '72000.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('831', '2023-03-08', '杨小乐', '内蒙古博鑫农牧业科技有限公司（郑州嘉吉）', '01D616', '美多(反刍)', '销售发货', '50', '16.80', '840.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('832', '2023-03-09', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '200', '15.50', '3100.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('833', '2023-03-09', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '2000', '14.80', '29600.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('834', '2023-03-09', '张明', '营口三宝生物科技有限公司', '01D333', '微速强', '销售发货', '2000', '8.90', '17800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('835', '2023-03-09', '张明', '广东诚吉邦生物科技有限公司', '01D333', '微速强', '销售发货', '200', '8.90', '1780.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('836', '2023-03-09', '张明', '广东诚吉邦生物科技有限公司', '01D060', 'G/Zn-210', '销售发货', '25', '22.20', '555.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('837', '2023-03-09', '张明', '广东诚吉邦生物科技有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '15.00', '375.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('838', '2023-03-09', '张明', '广东诚吉邦生物科技有限公司', '01D034', '奇力铜（甘氨酸铜210）', '销售发货', '25', '50.50', '1262.50', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('839', '2023-03-09', '张明', '广东诚吉邦生物科技有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '25', '21.60', '540.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('840', '2023-03-09', '龚波', '正大预混料（杭州）有限公司', '01D409', '抗氧灵-T', '销售发货', '300', '22.90', '6870.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('841', '2023-03-09', '何海涛', '艾地盟动物营养（漳州）有限公司', '01D456', '天科宝（F-05-120）', '销售发货', '60', '16.50', '990.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('842', '2023-03-09', '李军勇', '福州天凯生物科技有限责任公司', '01D340', '奇力宝（海水贝类动物用', '销售发货', '1000', '14.50', '14500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('843', '2023-03-09', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '6000', '10.60', '63600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('844', '2023-03-09', '黄坤', '渔润生物技术（淮安）有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('845', '2023-03-09', '纳生', '新沂益客大地饲料有限公司', '01D464', '赛肥素I', '销售发货', '150', '58.50', '8775.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('846', '2023-03-10', '高碧', '常山温氏畜牧有限公司', '01D503', '强味酸', '销售发货', '15000', '11.90', '178500.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('847', '2023-03-10', '谭永昌', '珠海陈溢志', '01D621', '美多-II（禽用）', '销售发货', '100', '20.30', '2030.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('848', '2023-03-10', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '2000', '9.35', '18700.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('849', '2023-03-10', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D226', '奇力宝（母猪用）', '销售发货', '600', '13.00', '7800.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('850', '2023-03-10', '张明', '江门市旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1500', '14.80', '22200.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('851', '2023-03-10', '龚波', '正大预混料（柳州）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '1500', '20.70', '31050.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('852', '2023-03-10', '龚波', '正大预混料（柳州）有限公司', '01D403', '抗氧灵', '销售发货', '1500', '12.10', '18150.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('853', '2023-03-10', '罗辉', '嘉吉动物营养（南宁）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '5000', '12.30', '61500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('854', '2023-03-10', '罗辉', '嘉吉饲料（陕西）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '18.00', '900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('855', '2023-03-10', '罗辉', '嘉吉饲料(重庆)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '18.00', '1800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('856', '2023-03-10', '罗辉', '嘉吉饲料(佛山)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '18.00', '1800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('857', '2023-03-10', '罗辉', '嘉吉动物营养（开封）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '18.00', '900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('858', '2023-03-10', '罗辉', '嘉吉饲料（漳州）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '400', '18.00', '7200.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('859', '2023-03-10', '罗辉', '嘉吉饲料(南宁)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '250', '18.00', '4500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('860', '2023-03-10', '罗辉', '嘉吉饲料（吉林）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('861', '2023-03-10', '刘云', '浏阳市湘宏生物科技饲料有限公司', '01D402', '抗氧灵', '销售发货', '300', '11.50', '3450.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('862', '2023-03-10', '王林', '佛山市顺德区丰华饲料实业有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '500', '12.00', '6000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('863', '2023-03-10', '王林', '海南恒兴饲料实业有限公司', '01D408', '抗氧灵', '销售发货', '50', '12.50', '625.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('864', '2023-03-10', '王林', '广东恒兴饲料实业股份有限公司遂溪分公司', '01D100', '天安硒', '销售发货', '1840', '56.50', '103960.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('865', '2023-03-10', '郑云瀚', '土耳其', '04D061', '甘氨酸锌21%', '销售发货', '10000', '15.01', '150080.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('866', '2023-03-10', '郑云瀚', '土耳其', '01D042', '天锰乐（G/Mn-220）', '销售发货', '6000', '17.35', '104118.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('867', '2023-03-10', '郑云瀚', '土耳其', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '2000', '32.50', '64990.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('868', '2023-03-10', '郑云瀚', '土耳其', '01D005', '甘氨酸铁17%', '销售发货', '6000', '10.45', '62712.00', '营销事业四部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('869', '2023-03-10', '张树军', '山西争跃化工药业有限公司', '01D324', '微速强', '销售发货', '1000', '13.00', '13000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('870', '2023-03-10', '杨小乐', '内蒙古博鑫农牧业科技有限公司（长春博瑞科技）', '01D616', '美多(反刍)', '销售发货', '450', '14.70', '6615.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('871', '2023-03-11', '高碧', '望江温氏畜牧有限公司', '01D503', '强味酸', '销售发货', '7500', '11.90', '89250.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('872', '2023-03-11', '谭永昌', '广州市中富饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '10.20', '10200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('873', '2023-03-11', '谭永昌', '广州快大饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.50', '8500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('874', '2023-03-11', '鲁刚', '哈尔滨远大牧业有限公司', '01D618', '美多-II（畜禽用）', '销售发货', '1000', '28.20', '28200.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('875', '2023-03-11', '王林', '新会区睦洲镇万华饲料厂', '01D420', '不来霉', '销售发货', '250', '7.50', '1875.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('876', '2023-03-11', '王林', '新会区睦洲镇万华饲料厂', '01D307', '奇力宝淡水甲壳动物用', '销售发货', '3000', '4.03', '12090.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('877', '2023-03-11', '黄坤', '扬州绿多邦生物有限公司', '01E753', '维稳宝（Ⅱ型）', '销售发货', '1000', '14.00', '14000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('878', '2023-03-11', '纳生', '邢台益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '100', '58.50', '5850.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('879', '2023-03-13', '高碧', '开平温氏农牧有限公司', '01D503', '强味酸', '销售发货', '7500', '11.90', '89250.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('880', '2023-03-13', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '25000', '9.05', '226250.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('881', '2023-03-13', '袁小波', '成都德力元商贸有限公司（绥化御咖牧业）', '04D214', '奇力宝（仔猪用）', '销售发货', '2000', '10.37', '20740.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('882', '2023-03-13', '袁小波', '成都申利行生物科技有限公司', '01E711', '奇力宝（仔猪用-颗粒型）', '销售发货', '2000', '10.50', '21000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('883', '2023-03-13', '黎永华', '石家庄海贝饲料科技有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '1000', '13.50', '13500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('884', '2023-03-13', '龚波', '潍坊天普阳光饲料科技有限公司', '01D610', '美多-II（畜禽用）', '销售发货', '1000', '17.30', '17300.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('885', '2023-03-13', '罗辉', '嘉吉饲料（宜春）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '20000', '12.30', '246000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('886', '2023-03-13', '杨锋', '福建省邵武市华龙饲料有限公司', '01D402', '抗氧灵', '销售发货', '2000', '16.00', '32000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('887', '2023-03-13', '刘云', '武汉汇康源生物科技有限公司', '01D786', '肝美多', '销售发货', '1200', '15.00', '18000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('888', '2023-03-13', '刘云', '武汉汇康源生物科技有限公司', '01D785', '澳美维（水溶型）', '销售发货', '2400', '15.50', '37200.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('889', '2023-03-13', '何翔飞', '佛山市南海禅泰动物药业有限公司', '01D060', 'G/Zn-210', '销售发货', '100', '32.00', '3200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('890', '2023-03-13', '刘志诚', '潍坊爱菲德饲料有限公司', '01D611', '美多－Ⅱ（禽用）', '销售发货', '50', '19.00', '950.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('891', '2023-03-13', '刘志诚', '潍坊爱菲德饲料有限公司', '01D420', '不来霉', '销售发货', '200', '7.50', '1500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('892', '2023-03-13', '刘志诚', '潍坊爱菲德饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '250', '12.30', '3075.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('893', '2023-03-13', '张树军', '漯河雅来动物营养饲料有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '2000', '14.50', '29000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('894', '2023-03-13', '黄坤', '南京禾嘉农牧科技有限公司', '01D092', '奇力铬（蛋氨酸铬001）', '销售发货', '25', '11.00', '275.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('895', '2023-03-13', '黄坤', '泰州通源生物科技有限公司', '01D781', '维稳宝', '销售发货', '500', '32.00', '16000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('896', '2023-03-13', '杨平', '山东兴彬宠物食品有限公司', '01D402', '抗氧灵', '销售发货', '500', '11.00', '5500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('897', '2023-03-14', '谭永昌', '广州澳升农业科技有限公司', '01D333', '微速强', '销售发货', '725', '12.00', '8700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('898', '2023-03-14', '袁小波', '成都德力元商贸有限公司（武威铁骑）', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '2000', '9.35', '18700.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('899', '2023-03-14', '袁小波', '成都德力元商贸有限公司（武威铁骑）', '04D214', '奇力宝（仔猪用）', '销售发货', '2000', '10.37', '20740.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('900', '2023-03-14', '张明', '佛山市顺德区旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '3000', '14.80', '44400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('901', '2023-03-14', '龚波', '武汉正大有限公司', '01D409', '抗氧灵-T', '销售发货', '2000', '22.90', '45800.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('902', '2023-03-14', '何海涛', '沈阳新华康饲料有限公司', '01D501', '强味酸(B）', '销售发货', '1000', '22.00', '22000.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('903', '2023-03-14', '何海涛', '鞍山市腾鳌东鹏饲料有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '100', '13.30', '1330.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('904', '2023-03-14', '何海涛', '阜新科威生物科技有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '600', '16.00', '9600.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('905', '2023-03-14', '何翔飞', '江门市桐美饲料有限公司', '04D320', '奇力宝（水产用）', '销售发货', '2000', '7.10', '14200.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('906', '2023-03-14', '何翔飞', '广州昭品饲料有限公司（江门市桐美饲料有限公司）', '04D320', '奇力宝（水产用）', '销售发货', '3000', '4.15', '12450.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('907', '2023-03-14', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '1000', '14.00', '14000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('908', '2023-03-14', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '12.30', '12300.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('909', '2023-03-14', '李军勇', '漳州惠科生物科技有限公司', '01D788', '澳美维-水产用', '销售发货', '500', '27.00', '13500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('910', '2023-03-14', '李军勇', '漳州惠科生物科技有限公司', '01D321', '奇力宝（淡水甲壳动物', '销售发货', '3000', '10.00', '30000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('911', '2023-03-14', '黄坤', '东台市科健牧业有限公司', '01D779', '澳美维', '销售发货', '400', '33.50', '13400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('912', '2023-03-14', '黄坤', '东台市科健牧业有限公司', '01D777', '澳美维', '销售发货', '250', '33.00', '8250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('913', '2023-03-14', '黄坤', '扬州天健生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('914', '2023-03-14', '黄坤', '常州科鑫生物科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '60', '24.00', '1440.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('915', '2023-03-14', '黄坤', '山西夏辰生物科技有限公司', '01D324', '微速强', '销售发货', '1000', '9.00', '9000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('916', '2023-03-14', '曲光', '禹城市启航农业科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '8.20', '8200.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('917', '2023-03-14', '曲光', '山东莘县张广伟', '04D061', '甘氨酸锌21%', '销售发货', '200', '13.30', '2660.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('918', '2023-03-14', '曲光', '山东莘县张广伟', '01D401', '抗氧灵（Ⅱ）', '销售发货', '75', '14.00', '1050.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('919', '2023-03-14', '曲光', '山东莘县张广伟', '01D274', '奇力宝（蛋禽用）', '销售发货', '125', '16.00', '2000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('920', '2023-03-14', '曲光', '山东莘县张广伟', '01D005', '甘氨酸铁17%', '销售发货', '100', '8.20', '820.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('921', '2023-03-14', '杨广达', '孟加拉', '01D461', '天佳乐', '销售发货', '1000', '60.52', '60520.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('922', '2023-03-15', '邱桂雄', '广州市天河联华农业科技有限公司', '02D350', '卡巴匹林钙粉', '销售发货', '100', '34.70', '3470.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('923', '2023-03-15', '邱桂雄', '广州市天河联华农业科技有限公司', '01E578', '美多', '销售发货', '360', '23.50', '8460.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('924', '2023-03-15', '谭永昌', '广州威邦得生物科技有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '40', '26.50', '1060.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('925', '2023-03-15', '谭永昌', '广州威邦得生物科技有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '15.00', '750.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('926', '2023-03-15', '谭永昌', '广州威邦得生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '25', '8.20', '205.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('927', '2023-03-15', '袁小波', '成都德力元商贸有限公司(重庆铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '5000', '10.37', '51850.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('928', '2023-03-15', '袁小波', '成都德力元商贸有限公司(成都铁骑)', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '500', '9.35', '4675.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('929', '2023-03-15', '袁小波', '成都德力元商贸有限公司(成都铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '2500', '10.37', '25925.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('930', '2023-03-15', '袁小波', '成都申利行生物科技有限公司（成都蓝海惠隆科技有限公司）', '01D402', '抗氧灵', '销售发货', '2000', '9.50', '19000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('931', '2023-03-15', '罗辉', '卜蜂（广东）生物科技有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '1000', '12.80', '12800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('932', '2023-03-15', '林雪', '巴基斯坦', '05D008', '禽宝（禽用）', '销售发货', '3000', '9.72', '29145.00', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('933', '2023-03-15', '刘云', '长沙湘如意生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.20', '4100.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('934', '2023-03-15', '刘云', '广西立腾农牧发展有限公司', '01E793', '澳美维', '销售发货', '80', '29.00', '2320.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('935', '2023-03-15', '刘云', '广西立发农牧有限公司', '01E793', '澳美维', '销售发货', '140', '29.00', '4060.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('936', '2023-03-15', '刘云', '广西立腾育种农牧科技有限公司', '01E793', '澳美维', '销售发货', '240', '29.00', '6960.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('937', '2023-03-15', '刘云', '广西百色立腾食品有限公司', '01E793', '澳美维', '销售发货', '180', '29.00', '5220.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('938', '2023-03-15', '张树军', '郑州盛世泰弘生物科技有限公司', '01D378', '奇力宝（肉牛.羊用）', '销售发货', '100', '12.00', '1200.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('939', '2023-03-15', '张树军', '郑州盛世泰弘生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '25', '9.00', '225.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('940', '2023-03-15', '李军勇', '福建上一饲料有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '1000', '12.50', '12500.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('941', '2023-03-15', '李军勇', '福建大北农华有水产科技集团有限公司', '01D321', '奇力宝（淡水甲壳动物', '销售发货', '8000', '10.00', '80000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('942', '2023-03-15', '黄坤', '盐城渔乐乐生物科技有限公司', '01E733', '渔乐乐', '销售发货', '1000', '16.00', '16000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('943', '2023-03-15', '黄坤', '无锡绿金科生物技术有限公司', '01D317', '微速达（水产用）', '销售发货', '500', '14.50', '7250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('944', '2023-03-15', '黄坤', '南京宏远生物', '01D452', '天科宝（A-04-414）', '销售发货', '100', '25.00', '2500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('945', '2023-03-15', '黄坤', '徐州中远生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '300', '12.80', '3840.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('946', '2023-03-15', '黄坤', '徐州中远生物科技有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '300', '11.80', '3540.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('947', '2023-03-15', '罗辉', '江苏天成科技集团南通饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '2000', '11.60', '23200.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('948', '2023-03-16', '黎永华', '河北科尔斯勒生物科技有限公司', '01D375', '奇力宝-舔砖专用', '销售发货', '125', '24.00', '3000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('949', '2023-03-16', '龚波', '正大预混料（广汉）有限公司', '01D409', '抗氧灵-T', '销售发货', '1500', '22.90', '34350.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('950', '2023-03-16', '何海涛', '沈阳波音饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1000', '13.30', '13300.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('951', '2023-03-16', '何海涛', '沈阳新华康饲料有限公司', '01D505', '幼添宝', '销售发货', '100', '12.50', '1250.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('952', '2023-03-16', '何海涛', '公主岭新康饲料有限公司', '01D501', '强味酸(B）', '销售发货', '1000', '22.00', '22000.00', '市场销售-部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('953', '2023-03-16', '鲁刚', '长春市环农饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '1000', '28.80', '28800.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('954', '2023-03-16', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '10000', '7.20', '72000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('955', '2023-03-16', '王林', '江门市蓬江区海豚水族有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '50', '17.30', '865.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('956', '2023-03-16', '王林', '江门市蓬江区海豚水族有限公司', '01D296', '奇力宝－甲鱼用', '销售发货', '1000', '5.70', '5700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('957', '2023-03-16', '王林', '珠海一村农业发展有限公司', '04D791', '一村·多维', '销售发货', '500', '34.50', '17250.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('958', '2023-03-16', '王林', '江门市海众饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '2000', '10.50', '21000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('959', '2023-03-16', '王林', '中渔生物科技公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '20', '17.00', '340.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('960', '2023-03-16', '张树军', '山西晋龙集团饲料有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '200', '30.00', '6000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('961', '2023-03-16', '纳生', '南宁市百禾生物科技有限公司（广西大富华农牧）', '01E740', '舒柠500', '销售发货', '800', '46.00', '36800.00', '市场管理部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('962', '2023-03-17', '龚波', '正大预混料（南通）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '5000', '20.70', '103500.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('963', '2023-03-17', '龚波', '正大预混料（南通）有限公司', '01D409', '抗氧灵-T', '销售发货', '3000', '22.90', '68700.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('964', '2023-03-17', '龚波', '正大预混料（南通）有限公司', '01D403', '抗氧灵', '销售发货', '200', '12.10', '2420.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('965', '2023-03-17', '罗辉', '江苏雅博动物健康科技有限责任公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '40', '48.00', '1920.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('966', '2023-03-17', '罗辉', '江苏雅博动物健康科技有限责任公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '35.00', '875.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('967', '2023-03-17', '杨锋', '杨锋（厦门渔雄越）', '04D459', '诱特妙（水产诱食剂）', '销售发货', '20', '17.00', '340.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('968', '2023-03-17', '王林', '江门市蓬江区海豚水族有限公司', '01D299', '奇力宝杂食性鱼用', '销售发货', '1000', '7.30', '7300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('969', '2023-03-17', '王林', '福州卓林生态农业发展有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '200', '18.00', '3600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('970', '2023-03-17', '黄坤', '象山智恩水质改良剂经营部', '01D782', '澳美维（水产用）', '销售发货', '60', '30.00', '1800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('971', '2023-03-17', '黄坤', '象山智恩水质改良剂经营部', '01D334', '微速强', '销售发货', '500', '11.00', '5500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('972', '2023-03-17', '黄坤', '扬州海丰生物科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '80', '24.00', '1920.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('973', '2023-03-18', '谭永昌', '清远金沣生物药品有限公司', '04D061', '甘氨酸锌21%', '销售发货', '300', '22.70', '6810.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('974', '2023-03-18', '谭永昌', '清远金沣生物药品有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '16.70', '16700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('975', '2023-03-18', '黎永华', '石家庄靖农生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.20', '4100.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('976', '2023-03-18', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '300', '15.50', '4650.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('977', '2023-03-18', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '2000', '14.80', '29600.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('978', '2023-03-18', '龚波', '正大预混料（柳州）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '500', '20.70', '10350.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('979', '2023-03-18', '龚波', '正大预混料（柳州）有限公司', '01D409', '抗氧灵-T', '销售发货', '25', '22.90', '572.50', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('980', '2023-03-18', '宁会', '莒县新特瑞饲料有限公司', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '200', '16.00', '3200.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('981', '2023-03-18', '罗辉', '芜湖市宏缘禽业专业合作社', '01D267', '奇力宝（蛋壳素）', '销售发货', '2000', '12.80', '25600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('982', '2023-03-18', '杨锋', '龙岩市百特饲料科技有限公司', '01D402', '抗氧灵', '销售发货', '3500', '16.00', '56000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('983', '2023-03-18', '刘云', '吉林省金沃农牧科技有限公司', '01D402', '抗氧灵', '销售发货', '500', '13.50', '6750.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('984', '2023-03-18', '王林', '湖北帮成生物工程有限公司', '01D454', '天科宝', '销售发货', '100', '19.00', '1900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('985', '2023-03-18', '黄坤', '扬州涌泉生物技术有限公司', '01D324', '微速强', '销售发货', '1000', '11.00', '11000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('986', '2023-03-18', '黄坤', '扬州涌泉生物技术有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('987', '2023-03-20', '邱桂雄', '广州市天河联华农业科技有限公司', '02D503', '奇力考星', '销售发货', '300', '113.00', '33900.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('988', '2023-03-20', '邱桂雄', '广州市天河联华农业科技有限公司', '01E736', '种多宝', '销售发货', '400', '19.00', '7600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('989', '2023-03-20', '邱桂雄', '武汉民族科技饲料有限公司', '01E756', '奇力宝（蛋禽用）', '销售发货', '3000', '13.30', '39900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('990', '2023-03-20', '邱桂雄', '武汉民族科技饲料有限公司', '01E755', '奇力宝（蛋禽用-颗粒型）', '销售发货', '5000', '9.85', '49250.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('991', '2023-03-20', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '25000', '9.05', '226250.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('992', '2023-03-20', '谭永昌', '广州市迦璟生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '350', '13.30', '4655.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('993', '2023-03-20', '谭永昌', '广州市迦璟生物科技有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '350', '15.00', '5250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('994', '2023-03-20', '谭永昌', '广州市迦璟生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '250', '8.20', '2050.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('995', '2023-03-20', '谭永昌', '广州澳升农业科技有限公司', '01D333', '微速强', '销售发货', '275', '12.00', '3300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('996', '2023-03-20', '谭永昌', '深圳安佑康牧科技有限公司东莞分公司', '01D090', '蛋氨酸铬3.0%', '销售发货', '210', '55.00', '11550.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('997', '2023-03-20', '黎永华', '北京九州大地生物技术集团股份有限公司天津分公司', '01D611', '美多－Ⅱ（禽用）', '销售发货', '300', '22.99', '6897.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('998', '2023-03-20', '黎永华', '石家庄靖农生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '13.00', '6500.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('999', '2023-03-20', '王林', '广东广信饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '5000', '9.50', '47500.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1000', '2023-03-20', '王林', '湛江市粤凯生物科技有限公司', '01D777', '澳美维', '销售发货', '1500', '30.80', '46200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1001', '2023-03-20', '王林', '湛江市粤凯生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '2000', '14.30', '28600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1002', '2023-03-20', '张树军', '山西蓝晶商贸有限公司', '01D779', '澳美维', '销售发货', '100', '37.00', '3700.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1003', '2023-03-20', '张树军', '山西蓝晶商贸有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '16.00', '8000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1004', '2023-03-20', '黄坤', '丰县普华农牧科技有限公司', '01D275', '奇力宝（肉禽用）', '销售发货', '3000', '11.00', '33000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1005', '2023-03-20', '黄坤', '徐州中远生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '700', '12.80', '8960.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1006', '2023-03-20', '黄坤', '徐州中远生物科技有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '700', '11.80', '8260.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1007', '2023-03-20', '杨平', '日照金鑫生态农业科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '13.00', '13000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1008', '2023-03-20', '曲光', '山东鑫粮农牧科技有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '10.30', '5150.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1009', '2023-03-20', '曲光', '姚元勇', '01D274', '奇力宝（蛋禽用）', '销售发货', '500', '14.80', '7400.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1010', '2023-03-20', '纳生', '南宁市百禾生物科技有限公司（湖南广安动保公司）', '01D467', '赛肥素', '销售发货', '500', '35.00', '17500.00', '市场管理部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1011', '2023-03-20', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D402', '抗氧灵', '销售发货', '2000', '9.70', '19400.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1012', '2023-03-20', '何翔飞', '广州昭品饲料有限公司', '01A301', '一水硫酸镁', '销售发货', '250', '5.00', '1250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1013', '2023-03-21', '邱桂雄', '广州市天河联华农业科技有限公司', '02D415', '阿莫西林', '销售发货', '140', '96.00', '13440.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1014', '2023-03-21', '邱桂雄', '广州市天河联华农业科技有限公司', '02D345', '亚硒酸钠维生素E预混剂', '销售发货', '750', '4.00', '3000.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1015', '2023-03-21', '邱桂雄', '广州市天河联华农业科技有限公司', '01D771', '澳美维', '销售发货', '75', '18.20', '1365.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1016', '2023-03-21', '袁小波', '成都德力元商贸有限公司(攀枝花铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '5000', '10.37', '51850.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1017', '2023-03-21', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '04D459', '诱特妙（水产诱食剂）', '销售发货', '2000', '17.00', '34000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1018', '2023-03-21', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '04D291', '奇力宝（鳗鱼用）', '销售发货', '6000', '3.90', '23400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1019', '2023-03-21', '张树军', '河南正本清源科技发展股份有限公司', '01D217', '奇力宝-仔猪用', '销售发货', '2000', '22.00', '44000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1020', '2023-03-21', '黄坤', '宝兰（南京）生物技术有限公司', '01D324', '微速强', '销售发货', '2000', '11.00', '22000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1021', '2023-03-21', '黄坤', '盐城渔乐乐生物科技有限公司', '01E733', '渔乐乐', '销售发货', '500', '16.00', '8000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1022', '2023-03-21', '杨平', '莒县利群饲料厂', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '14.00', '14000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1023', '2023-03-21', '杨小乐', '内蒙古博鑫农牧业科技有限公司（宜春嘉吉）', '01D616', '美多(反刍)', '销售发货', '1000', '14.40', '14400.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1024', '2023-03-21', '杨小乐', '内蒙古博鑫农牧业科技有限公司（昆明嘉吉）', '01D616', '美多(反刍)', '销售发货', '50', '17.70', '885.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1025', '2023-03-22', '邱桂雄', '广州市天河联华农业科技有限公司', '01D770', '多维葡萄糖', '销售退货', '-15', '8.80', '-132.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1026', '2023-03-22', '袁小波', '成都申利行生物科技有限公司（万家好）', '01E711', '奇力宝（仔猪用-颗粒型）', '销售发货', '3000', '10.50', '31500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1027', '2023-03-22', '袁小波', '成都申利行生物科技有限公司（万家好）', '01D005', '甘氨酸铁17%', '销售发货', '3000', '7.90', '23700.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1028', '2023-03-22', '黎永华', '河北菁瑞源生物科技有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '500', '14.00', '7000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1029', '2023-03-22', '王林', '诺可信（厦门）生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '200', '14.50', '2900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1030', '2023-03-22', '王林', '中山苏的良', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1031', '2023-03-22', '王林', '中山苏的良', '01D317', '微速达（水产用）', '销售发货', '250', '14.10', '3525.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1032', '2023-03-22', '张树军', '河南诺尔饲料科技有限公司', '01D456', '天科宝（F-05-120）', '销售发货', '100', '15.50', '1550.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1033', '2023-03-22', '张树军', '河南诺尔饲料科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '3000', '12.30', '36900.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1034', '2023-03-22', '李军勇', '福建大昌盛饲料有限公司', '04D323', '奇力宝（鳗鱼用）', '销售发货', '5000', '4.00', '20000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1035', '2023-03-22', '李军勇', '东莞市普洲饲料有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '500', '12.50', '6250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1036', '2023-03-22', '黄坤', '扬州涌泉生物技术有限公司', '01D458', '诱特妙（水产诱食促长剂）', '销售发货', '100', '40.00', '4000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1037', '2023-03-22', '杨平', '临沂峰范商贸有限公司', '01D267', '奇力宝（蛋壳素）', '销售发货', '300', '11.00', '3300.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1038', '2023-03-22', '杨平', '临沂峰范商贸公司（杨平赠送）', '01D267', '奇力宝（蛋壳素）', '销售发货', '25', '10.00', '250.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1039', '2023-03-22', '曲光', '菏泽牧源生物科技有限公司', '01D611', '美多－Ⅱ（禽用）', '销售发货', '1500', '21.00', '31500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1040', '2023-03-23', '邱桂雄', '广东科邦饲料科技有限公司', '01D230', '奇力宝（生长猪用）', '销售发货', '2000', '9.50', '19000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1041', '2023-03-23', '谭永昌', '广州市众望饲料有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '500', '13.00', '6500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1042', '2023-03-23', '袁小波', '成都德力元商贸有限公司（昆明铁骑）', '04D214', '奇力宝（仔猪用）', '销售发货', '2500', '10.37', '25925.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1043', '2023-03-23', '黎永华', '河北恒派生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '2000', '13.00', '26000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1044', '2023-03-23', '黎永华', '河北恒派生物科技有限公司', '01D272', '禽宝', '销售发货', '2000', '10.00', '20000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1045', '2023-03-23', '何海涛', '辽宁九州生物科技有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.49', '26980.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1046', '2023-03-23', '何海涛', '辽宁鑫祥跃生物科技有限公司', '01D402', '抗氧灵', '销售发货', '2000', '13.20', '26400.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1047', '2023-03-23', '李军勇', '福建昌琪生物工程有限公司', '01D295', '奇力宝-甲鱼用', '销售发货', '3000', '3.50', '10500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1048', '2023-03-23', '李军勇', '东莞市普洲饲料有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '500', '12.50', '6250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1049', '2023-03-23', '黄坤', '扬州海丰生物科技有限公司', '01D781', '维稳宝', '销售发货', '2000', '30.00', '60000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1050', '2023-03-23', '黄坤', '徐州中远生物科技有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '700', '11.80', '8260.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1051', '2023-03-23', '曲光', '山东鑫粮农牧科技有限公司', '01D420', '不来霉', '销售发货', '200', '8.50', '1700.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1052', '2023-03-23', '纳生', '临沂众客饲料有限公司', '01D464', '赛肥素I', '销售发货', '400', '58.50', '23400.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1053', '2023-03-23', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D779', '澳美维', '销售发货', '40', '30.80', '1232.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1054', '2023-03-23', '罗辉', '罗辉（上海保斯利）', '01D402', '抗氧灵', '销售发货', '550', '9.50', '5225.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1055', '2023-03-24', '邱桂雄', '广州市天河联华农业科技有限公司', '02D328', '虫净', '销售发货', '140', '38.00', '5320.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1056', '2023-03-24', '邱桂雄', '广州市天河联华农业科技有限公司', '02D328', '虫净', '销售发货', '200', '38.00', '7600.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1057', '2023-03-24', '邱桂雄', '广州市天河联华农业科技有限公司', '02D328', '虫净', '销售发货', '260', '38.00', '9880.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1058', '2023-03-24', '邱桂雄', '广州市天河联华农业科技有限公司', '01D771', '澳美维', '销售发货', '225', '18.20', '4095.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1059', '2023-03-24', '何海涛', '鞍山市精艺农牧科技有限公司', '01D757', '天籁', '销售发货', '120', '30.00', '3600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1060', '2023-03-24', '王林', '旺记药店', '01D309', '奇力宝海水甲壳动物用', '销售发货', '1000', '4.45', '4450.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1061', '2023-03-24', '王林', '湛江海先锋生物科技有限公司', '01D453', '天科宝（A-09-410）', '销售发货', '500', '25.00', '12500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1062', '2023-03-24', '王林', '广州市恒健饲料有限公司', '01D402', '抗氧灵', '销售发货', '300', '10.50', '3150.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1063', '2023-03-24', '王林', '中山苏的良', '01E720', '肝美多', '销售发货', '75', '10.00', '750.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1064', '2023-03-24', '黄坤', '小彭水产', '04D337', '一村多矿', '销售发货', '600', '9.00', '5400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1065', '2023-03-24', '曲光', '山东同德大和农牧有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1500', '13.60', '20400.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1066', '2023-03-25', '谭永昌', '广州市汇鑫动物药业有限公司', '01D005', '甘氨酸铁17%', '销售发货', '400', '22.00', '8800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1067', '2023-03-25', '张明', '广东诚吉邦生物科技有限公司', '01D333', '微速强', '销售发货', '500', '9.00', '4500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1068', '2023-03-25', '杨锋', '杨锋（漳州蔡开心）', '01D324', '微速强', '销售发货', '25', '8.90', '222.50', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1069', '2023-03-25', '何翔飞', '广州市和生堂动物药业有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '400', '23.00', '9200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1070', '2023-03-25', '鲁刚', '长春市环农饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '6000', '12.00', '72000.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1071', '2023-03-25', '鲁刚', '长春市环农饲料有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '2000', '28.80', '57600.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1072', '2023-03-25', '鲁刚', '长春市环农饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '19500', '7.20', '140400.00', '市场销售-部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1073', '2023-03-25', '王林', '珠海杨华彬', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1074', '2023-03-25', '王林', '珠海杨华彬', '01D334', '微速强', '销售发货', '100', '11.00', '1100.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1075', '2023-03-27', '高碧', '温氏食品集团股份有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '25000', '9.05', '226250.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1076', '2023-03-27', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '5625', '9.05', '50906.25', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1077', '2023-03-27', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '21875', '9.05', '197968.75', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1078', '2023-03-27', '袁小波', '成都德力元商贸有限公司（绥化御咖牧业）', '04D214', '奇力宝（仔猪用）', '销售发货', '3000', '10.37', '31110.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1079', '2023-03-27', '黎永华', '河北维尔利动物药业集团有限公司', '04D061', '甘氨酸锌21%', '销售发货', '550', '14.00', '7700.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1080', '2023-03-27', '黎永华', '河北维尔利动物药业集团有限公司', '01D005', '甘氨酸铁17%', '销售发货', '550', '8.60', '4730.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1081', '2023-03-27', '龚波', '正大预混料（沈阳）有限公司', '01D403', '抗氧灵', '销售发货', '500', '12.10', '6050.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1082', '2023-03-27', '刘云', '湖南金牌昊天生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '300', '7.90', '2370.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1083', '2023-03-27', '何海涛', '沈阳波音饲料有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '500', '19.00', '9500.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1084', '2023-03-27', '何海涛', '沈阳波音饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '8.50', '8500.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1085', '2023-03-27', '何海涛', '英联饲料(辽宁)有限公司', '01D255', '奇力宝－蛋禽用', '销售退货', '-600', '25.00', '-15000.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1086', '2023-03-27', '何海涛', '英联饲料(辽宁)有限公司', '01D255', '奇力宝－蛋禽用', '销售发货', '600', '26.00', '15600.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1087', '2023-03-27', '何海涛', '大连韩伟养鸡有限公司旅顺分公司', '01D409', '抗氧灵-T', '销售发货', '25', '20.00', '500.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1088', '2023-03-27', '鲁刚', '哈尔滨远大牧业有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '3000', '21.10', '63300.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1089', '2023-03-27', '张树军', '河南安信畜牧科技发展有限公司', '01D777', '澳美维', '销售发货', '300', '35.50', '10650.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1090', '2023-03-27', '张树军', '河南安信畜牧科技发展有限公司', '01D618', '美多-II（畜禽用）', '销售发货', '500', '22.00', '11000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1091', '2023-03-27', '纳生', '新沂益客大地饲料有限公司', '01D464', '赛肥素I', '销售发货', '150', '58.50', '8775.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1092', '2023-03-27', '杨小乐', '内蒙古博鑫农牧业科技有限公司（君羊牧业）', '01D624', '美多-CR', '销售发货', '520', '18.40', '9568.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1093', '2023-03-28', '龚波', '正大预混料（天津）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '2300', '20.70', '47610.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1094', '2023-03-28', '龚波', '正大预混料（天津）有限公司', '01D403', '抗氧灵', '销售发货', '3000', '12.10', '36300.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1095', '2023-03-28', '龚波', '正大预混料（柳州）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '800', '20.70', '16560.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1096', '2023-03-28', '龚波', '正大预混料（柳州）有限公司', '01D403', '抗氧灵', '销售发货', '1000', '12.10', '12100.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1097', '2023-03-28', '刘云', '刘云（沈阳良宇饲料）', '01D267', '奇力宝（蛋壳素）', '销售发货', '125', '10.00', '1250.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1098', '2023-03-28', '何翔飞', '江门市西瑞动物保健科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '3000', '10.10', '30300.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1099', '2023-03-28', '王林', '珠海孙宝宝', '01D781', '维稳宝', '销售发货', '25', '28.00', '700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1100', '2023-03-28', '杨平', '五莲县鑫福饲料有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售发货', '500', '14.00', '7000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1101', '2023-03-29', '邱桂雄', '广州市天河联华农业科技有限公司', '01E578', '美多', '销售发货', '400', '23.50', '9400.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1102', '2023-03-29', '谭永昌', '广州农信饲料有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '15.50', '7750.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1103', '2023-03-29', '谭永昌', '广州农信饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '8.80', '17600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1104', '2023-03-29', '谭永昌', '广州市丰收饲料添加剂有限公司', '01D621', '美多-II（禽用）', '销售发货', '200', '20.30', '4060.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1105', '2023-03-29', '谭永昌', '广州金水动物保健品有限公司', '04D061', '甘氨酸锌21%', '销售发货', '1000', '22.70', '22700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1106', '2023-03-29', '谭永昌', '广州金水动物保健品有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '300', '26.00', '7800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1107', '2023-03-29', '谭永昌', '广州金水动物保健品有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '500', '38.50', '19250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1108', '2023-03-29', '谭永昌', '广州金水动物保健品有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1500', '16.70', '25050.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1109', '2023-03-29', '何海涛', '辽宁众友饲料有限公司', '01D402', '抗氧灵', '销售发货', '1000', '13.50', '13500.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1110', '2023-03-29', '何翔飞', '广州容大水产科技有限公司', '01D317', '微速达（水产用）', '销售发货', '500', '20.00', '10000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1111', '2023-03-29', '王林', '珠海市德海生物科技有限公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '1000', '34.00', '34000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1112', '2023-03-29', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '2000', '10.60', '21200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1113', '2023-03-29', '黄坤', '常州科鑫生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '18.00', '18000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1114', '2023-03-29', '黄坤', '上海市奉贤区汪瑞水产服务部', '01D343', '微速强', '销售发货', '1040', '11.50', '11960.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1115', '2023-03-29', '黄坤', '上海市奉贤区汪瑞水产服务部', '01D343', '微速强', '销售发货', '1000', '11.50', '11500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1116', '2023-03-29', '黄坤', '江苏合纵思远生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '2000', '18.00', '36000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1117', '2023-03-29', '黄坤', '江苏合纵思远生物科技有限公司（赠送）', '01D317', '微速达（水产用）', '销售发货', '100', '14.00', '1400.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1118', '2023-03-29', '杨小乐', '内蒙古博鑫农牧业科技有限公司（现代商河）', '01D620', '美多（反刍动物用）', '销售发货', '1500', '16.90', '25350.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1119', '2023-03-29', '杨小乐', '内蒙古博鑫农牧业科技有限公司（现代商河）', '01D616', '美多(反刍)', '销售发货', '3000', '14.40', '43200.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1120', '2023-03-30', '谭永昌', '鹤山市科牧源动物药业有限公司', '01D782', '澳美维（水产用）', '销售发货', '15', '27.80', '417.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1121', '2023-03-30', '谭永昌', '鹤山市科牧源动物药业有限公司', '01D334', '微速强', '销售发货', '20', '11.00', '220.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1122', '2023-03-30', '谭永昌', '四会市惠诚动物保健有限公司', '01D779', '澳美维', '销售发货', '120', '31.00', '3720.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1123', '2023-03-30', '谭永昌', '广东华红饲料科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.40', '6700.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1124', '2023-03-30', '谭永昌', '广东华红饲料科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '8.80', '17600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1125', '2023-03-30', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D226', '奇力宝（母猪用）', '销售发货', '300', '13.00', '3900.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1126', '2023-03-30', '袁小波', '成都德力元商贸有限公司（西安铁骑力士）', '04D214', '奇力宝（仔猪用）', '销售发货', '4000', '10.37', '41480.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1127', '2023-03-30', '黎永华', '邢台熙轩商贸有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '200', '11.50', '2300.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1128', '2023-03-30', '黎永华', '小巨蛋(山东)生物科技有限公司', '01E634', '微速达（畜禽用）', '销售发货', '50', '14.00', '700.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1129', '2023-03-30', '林雪', '乌拉圭', '01D460', '天科宝  Tankebaal天馨乐', '销售发货', '700', '72.70', '50886.50', '营销事业四部', '科技', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1130', '2023-03-30', '杨锋', '厦门渔雄越生物科技有限公司', '01E802', '速达康', '销售发货', '500', '9.50', '4750.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1131', '2023-03-30', '何海涛', '辽宁牧华合美生物科技有限公司', '01D409', '抗氧灵-T', '销售发货', '1000', '22.00', '22000.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1132', '2023-03-30', '何翔飞', '佛山市顺德区勒流镇南祥饲料有限公司', '01D297', '奇力宝（甲鱼用-)', '销售发货', '5000', '3.85', '19250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1133', '2023-03-30', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D011', '奇力铁预混料', '销售发货', '1000', '7.60', '7600.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1134', '2023-03-30', '刘志诚', '潍坊爱菲德饲料有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '200', '11.60', '2320.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1135', '2023-03-30', '黄坤', '徐州中远生物科技有限公司', '01D219', '奇力宝（仔猪用-颗粒型）', '销售退货', '-700', '11.80', '-8260.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1136', '2023-03-30', '杨平', '临沂盛泉油脂化工有限公司江泉饲料厂', '01D608', '美多E', '销售发货', '200', '55.00', '11000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1137', '2023-03-30', '杨平', '山东东方顶顺生物科技有限公司', '01D364', '奇力宝(毛皮动物用)', '销售发货', '500', '21.00', '10500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1138', '2023-03-30', '杨小乐', '内蒙古博鑫农牧业科技有限公司（长春博瑞科技）', '01D616', '美多(反刍)', '销售发货', '200', '15.00', '3000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1139', '2023-03-31', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '12000', '13.00', '156000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1140', '2023-03-31', '袁小波', '成都德力元商贸有限公司（四川巨星）', '01D215', '奇力宝（仔猪用有机矿精）', '销售发货', '20000', '12.60', '252000.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1141', '2023-03-31', '袁小波', '成都德力元商贸有限公司（宁夏铁骑）', '04D374', '奇力宝（肉牛.羊用）', '销售发货', '1000', '9.35', '9350.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1142', '2023-03-31', '张明', '佛山市顺德区旺海饲料实业有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '3000', '14.80', '44400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1143', '2023-03-31', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '200', '15.50', '3100.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1144', '2023-03-31', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '2000', '14.80', '29600.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1145', '2023-03-31', '罗辉', '淮安美标饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '1500', '7.50', '11250.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1146', '2023-03-31', '杨锋', '福州江炜（天赐饲料）', '01D402', '抗氧灵', '销售发货', '625', '9.50', '5937.50', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1147', '2023-03-31', '王林', '新会于荣文', '01D782', '澳美维（水产用）', '销售发货', '225', '26.20', '5895.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1148', '2023-03-31', '王林', '新会于荣文', '01D317', '微速达（水产用）', '销售发货', '375', '14.10', '5287.50', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1149', '2023-03-31', '王林', '广东基联水产科技有限公司', '01D317', '微速达（水产用）', '销售发货', '250', '18.00', '4500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1150', '2023-03-31', '李军勇', '百达（广州）生物科技有限公司', '01E720', '肝美多', '销售发货', '1000', '13.00', '13000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1151', '2023-03-31', '李军勇', '百达（广州）生物科技有限公司', '01D333', '微速强', '销售发货', '500', '11.00', '5500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1152', '2023-03-31', '黄坤', '张源顺虾稻共作家庭农场', '01D333', '微速强', '销售发货', '225', '10.00', '2250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1153', '2023-03-31', '杨平', '山东菲亚宠物食品有限公司', '01D420', '不来霉', '销售发货', '1000', '12.30', '12300.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1154', '2023-03-31', '杨平', '山东菲亚宠物食品有限公司', '01D402', '抗氧灵', '销售发货', '1000', '12.00', '12000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1155', '2023-03-31', '杨平', '山东菲亚（杨平赠送）', '01D420', '不来霉', '销售发货', '25', '7.50', '187.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1156', '2023-03-31', '杨平', '山东菲亚（杨平赠送）', '01D402', '抗氧灵', '销售发货', '25', '9.50', '237.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1157', '2023-03-31', '杨平', '山东澜泊湾宠物食品有限公司', '01D420', '不来霉', '销售发货', '50', '12.30', '615.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1158', '2023-03-31', '杨平', '山东澜泊湾宠物食品有限公司', '01D402', '抗氧灵', '销售发货', '50', '12.00', '600.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1159', '2023-04-01', '谭永昌', '广州市汇邦动物药业有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '14.00', '350.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1160', '2023-04-01', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D226', '奇力宝（母猪用）', '销售发货', '500', '13.00', '6500.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1161', '2023-04-01', '袁小波', '成都德力元商贸有限公司(四川铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '25000', '10.37', '259250.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1162', '2023-04-01', '龚波', '正大预混料（杭州）有限公司', '01D403', '抗氧灵', '销售发货', '2000', '12.10', '24200.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1163', '2023-04-01', '刘云', '湖南喜来高动物保健品股份有限公司', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '500', '27.00', '13500.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1164', '2023-04-01', '刘云', '湖南喜来高动物保健品股份有限公司（赠送)', '01D009', '奇力铁（甘氨酸铁185）', '销售发货', '50', '21.50', '1075.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1165', '2023-04-01', '王林', '中山市东升镇渔乐圈饲料经销店', '01D782', '澳美维（水产用）', '销售发货', '150', '26.20', '3930.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1166', '2023-04-01', '张树军', '郑州博大牧业科技有限公司', '01D277', '奇力宝（蛋壳素）', '销售发货', '500', '10.80', '5400.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1167', '2023-04-01', '张树军', '郑州博大牧业科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '200', '7.90', '1580.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1168', '2023-04-01', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '1000', '10.60', '10600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1169', '2023-04-01', '黄坤', '扬州绿多邦生物有限公司　　', '01E754', '微速达®（水产用）', '销售发货', '100', '18.00', '1800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1170', '2023-04-01', '黄坤', '扬州绿多邦生物有限公司', '01D317', '微速达（水产用）', '销售发货', '900', '18.00', '16200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1171', '2023-04-03', '邱桂雄', '广东科邦饲料科技有限公司', '01D230', '奇力宝（生长猪用）', '销售发货', '500', '9.50', '4750.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1172', '2023-04-03', '邱桂雄', '广东科邦饲料科技有限公司', '01D213', '天籁（仔猪用）', '销售发货', '300', '24.50', '7350.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1173', '2023-04-03', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '11125', '9.05', '100681.25', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1174', '2023-04-03', '高碧', '北票温氏农牧有限公司', '01D201', '奇力宝--生长猪用', '销售发货', '16375', '9.05', '148193.75', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1175', '2023-04-03', '龚波', '正大预混料（天津）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '7700', '20.70', '159390.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1176', '2023-04-03', '罗辉', '嘉吉动物营养（南宁）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '18.00', '1800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1177', '2023-04-03', '何海涛', '四川大成农牧科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '12.14', '24280.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1178', '2023-04-03', '何海涛', '北京波尔莱特饲料有限公司', '01D402', '抗氧灵', '销售发货', '500', '13.50', '6750.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1179', '2023-04-03', '何翔飞', '佛山市顺德区广添饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '1000', '3.00', '3000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1180', '2023-04-03', '何翔飞', '佛山市顺德区广添饲料有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '4000', '2.85', '11400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1181', '2023-04-03', '鲁刚', '哈尔滨帝汉贸易有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '200', '14.00', '2800.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1182', '2023-04-03', '鲁刚', '凯恩格生物科技（沈阳）有限公司', '01D624', '美多-CR', '销售发货', '300', '21.80', '6540.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1183', '2023-04-03', '王林', '中山官廉', '01D782', '澳美维（水产用）', '销售发货', '450', '26.20', '11790.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1184', '2023-04-03', '王林', '中山官廉', '01D324', '微速强', '销售发货', '825', '8.80', '7260.00', '营销事业二部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1185', '2023-04-03', '王林', '广东渔夫宝水产科技有限公司', '01D317', '微速达（水产用）', '销售发货', '600', '16.00', '9600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1186', '2023-04-03', '李军勇', '福建昌琪生物工程有限公司', '04D290', '奇力宝（鳗鱼用）', '销售发货', '3000', '3.40', '10200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1187', '2023-04-03', '李军勇', '海一（厦门）生物科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '100', '21.00', '2100.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1188', '2023-04-03', '黄坤', '扬州龙牌生物有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '14.50', '14500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1189', '2023-04-03', '纳生', '宿迁益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '800', '58.50', '46800.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1190', '2023-04-03', '纳生', '邢台益客饲料有限公司', '01D464', '赛肥素I', '销售发货', '150', '58.50', '8775.00', '市场销售二部', '纳生', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1191', '2023-04-04', '邱桂雄', '广州市天河联华农业科技有限公司', '02D413', '环丙氨嗪预混剂1%', '销售发货', '190', '7.50', '1425.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1192', '2023-04-04', '邱桂雄', '武汉民族科技饲料有限公司', '01E756', '奇力宝（蛋禽用）', '销售发货', '2000', '13.30', '26600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1193', '2023-04-04', '邱桂雄', '武汉民族科技饲料有限公司', '01E755', '奇力宝（蛋禽用-颗粒型）', '销售发货', '8000', '9.85', '78800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1194', '2023-04-04', '谭永昌', '广州德沅生物饲料有限公司', '01D420', '不来霉', '销售发货', '500', '11.00', '5500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1195', '2023-04-04', '谭永昌', '广州德沅生物饲料有限公司', '01D401', '抗氧灵（Ⅱ）', '销售发货', '300', '17.00', '5100.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1196', '2023-04-04', '张明', '佛山市顺德区兄弟德力饲料实业有限公司', '01D202', '奇力宝-生长猪矿精', '销售发货', '2000', '10.50', '21000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1197', '2023-04-04', '罗辉', '淮安美标饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '2500', '7.50', '18750.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1198', '2023-04-04', '杨锋', '安徽和明生农贸有限公司（安徽华盟）', '01D611', '美多－Ⅱ（禽用）', '销售发货', '200', '20.00', '4000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1199', '2023-04-04', '杨锋', '福建省漳州市华龙饲料有限公司', '01D402', '抗氧灵', '销售发货', '2500', '15.00', '37500.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1200', '2023-04-04', '杨锋', '安徽和明生农贸有限公司（合肥华仁郎溪分公司）', '01D611', '美多－Ⅱ（禽用）', '销售发货', '200', '20.00', '4000.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1201', '2023-04-04', '王林', '江西新良基宏通生物科技有限公司', '01D420', '不来霉', '销售发货', '500', '9.00', '4500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1202', '2023-04-04', '黄正月', '云南阳大饲料有限公司', '01D005', '甘氨酸铁17%', '销售发货', '500', '8.20', '4100.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1203', '2023-04-04', '曲光', '姚元勇', '01D611', '美多－Ⅱ（禽用）', '销售发货', '500', '21.00', '10500.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1204', '2023-04-04', '何海涛', '辽宁正成农牧科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '300', '17.50', '5250.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1205', '2023-04-06', '邱桂雄', '广州市天河联华农业科技有限公司', '02D345', '亚硒酸钠维生素E预混剂', '销售发货', '1250', '4.00', '5000.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1206', '2023-04-06', '邱桂雄', '广州市天河联华农业科技有限公司', '02D330', '盐酸多西环素可溶性粉', '销售发货', '20', '63.00', '1260.00', '市场管理部', '动保', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1207', '2023-04-06', '邱桂雄', '广州市天河联华农业科技有限公司', '01D772', '水溶性电解多维预混饲料', '销售发货', '450', '18.00', '8100.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1208', '2023-04-06', '邱桂雄', '肇庆端州张建辉', '04E026', '酸满宝', '销售发货', '30', '14.00', '420.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1209', '2023-04-06', '邱桂雄', '广东茂名王海', '04E026', '酸满宝', '销售发货', '990', '14.00', '13860.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1210', '2023-04-06', '袁小波', '成都德力元商贸有限公司(广元铁骑)', '04D226', '奇力宝（母猪用）', '销售发货', '200', '13.00', '2600.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1211', '2023-04-06', '龚波', '潍坊天普阳光饲料科技有限公司', '01D610', '美多-II（畜禽用）', '销售发货', '1000', '17.30', '17300.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1212', '2023-04-06', '龚波', '正大预混料（杭州）有限公司', '01E744', '美多-II(畜禽用-颗粒型)', '销售发货', '1000', '20.70', '20700.00', '营销事业一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1213', '2023-04-06', '罗辉', '嘉吉饲料（吉林）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1214', '2023-04-06', '罗辉', '宿迁市立华牧业有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '5000', '24.50', '122500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1215', '2023-04-06', '何海涛', '沈阳新华康饲料有限公司', '01D505', '幼添宝', '销售发货', '200', '12.50', '2500.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1216', '2023-04-06', '何海涛', '沈阳新华康饲料有限公司', '01D501', '强味酸(B）', '销售发货', '1000', '22.00', '22000.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1217', '2023-04-06', '何海涛', '沈阳新华康饲料有限公司', '01D011', '奇力铁预混料', '销售发货', '100', '8.50', '850.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1218', '2023-04-06', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '04D061', '甘氨酸锌21%', '销售发货', '400', '12.50', '5000.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1219', '2023-04-06', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '300', '14.50', '4350.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1220', '2023-04-06', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '01D031', '奇力铜（甘氨酸铜210）', '销售发货', '25', '28.80', '720.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1221', '2023-04-06', '何海涛', '艾地盟动物保健及营养（大连）有限公司天津分公司', '01D011', '奇力铁预混料', '销售发货', '1500', '7.60', '11400.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1222', '2023-04-06', '鲁刚', '哈尔滨波音绿洲生物技术有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '600', '18.50', '11100.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1223', '2023-04-06', '鲁刚', '哈尔滨熙瑞生物技术有限公司', '01D402', '抗氧灵', '销售发货', '1000', '9.70', '9700.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1224', '2023-04-06', '李军勇', '福州德远水产有限公司', '01E720', '肝美多', '销售发货', '250', '13.00', '3250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1225', '2023-04-06', '黄坤', '江苏创渔水产科技有限公司', '01D456', '天科宝（F-05-120）', '销售发货', '20', '24.00', '480.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1226', '2023-04-06', '黄坤', '江苏创渔水产科技有限公司', '01D455', '天科宝（A-09-05）', '销售发货', '20', '24.00', '480.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1227', '2023-04-06', '杨平', '江苏久久和牧农牧科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '10000', '12.00', '120000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1228', '2023-04-06', '杨平', '山东九牧饲料有限公司', '01D402', '抗氧灵', '销售发货', '1000', '11.00', '11000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1229', '2023-04-06', '杨平', '山东九牧饲料有限公司（赠送）', '01D402', '抗氧灵', '销售发货', '25', '9.50', '237.50', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1230', '2023-04-06', '杨小乐', '内蒙古博鑫农牧业科技有限公司（微悦装饰）', '01D779', '澳美维', '销售发货', '20', '30.50', '610.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1231', '2023-04-06', '杨小乐', '内蒙古博鑫农牧业科技有限公司（微悦装饰）', '01D624', '美多-CR', '销售发货', '60', '18.40', '1104.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1232', '2023-04-07', '袁小波', '成都德力元商贸有限公司(广元铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '4000', '10.37', '41480.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1233', '2023-04-07', '袁小波', '成都德力元商贸有限公司(重庆铁骑)', '04D214', '奇力宝（仔猪用）', '销售发货', '5000', '10.37', '51850.00', '市场管理部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1234', '2023-04-07', '黎永华', '北京同力兴科农业科技有限公司', '01D065', '奇力锌（蛋氨酸锌190）', '销售发货', '60', '35.00', '2100.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1235', '2023-04-07', '张明', '广东腾骏生物营养科技有限公司', '01D060', 'G/Zn-210', '销售发货', '200', '28.00', '5600.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1236', '2023-04-07', '罗辉', '嘉吉动物营养（南宁）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '15000', '12.30', '184500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1237', '2023-04-07', '罗辉', '嘉吉动物营养（南宁）有限公司', '01D014', '奇力铁', '销售发货', '2000', '7.80', '15600.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1238', '2023-04-07', '罗辉', '嘉吉饲料（陕西）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1239', '2023-04-07', '罗辉', '嘉吉饲料(佛山)有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '50', '18.00', '900.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1240', '2023-04-07', '罗辉', '嘉吉饲料（抚顺）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '25', '18.00', '450.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1241', '2023-04-07', '罗辉', '嘉吉动物营养（开封）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '18.00', '1800.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1242', '2023-04-07', '罗辉', '嘉吉饲料（泰安）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '75', '18.00', '1350.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1243', '2023-04-07', '罗辉', '嘉吉饲料（漳州）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '400', '18.00', '7200.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1244', '2023-04-07', '杨锋', '厦门渔雄越生物科技有限公司', '01E802', '速达康', '销售发货', '500', '9.50', '4750.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1245', '2023-04-07', '杨锋', '杨锋（漳州蔡开心）', '01D317', '微速达（水产用）', '销售发货', '500', '14.10', '7050.00', '市场销售三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1246', '2023-04-07', '王林', '湛江粤海预混料科技有限公司', '01D074', '奇力锌', '销售发货', '5000', '37.00', '185000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1247', '2023-04-07', '王林', '湛江粤海预混料科技有限公司', '01D013', '奇力铁', '销售发货', '5000', '16.00', '80000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1248', '2023-04-07', '张树军', '河南邦农饲料有限公司', '01E734', '奇力宝（种猪用）', '销售发货', '2000', '9.80', '19600.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1249', '2023-04-07', '李军勇', '钦州市锐创生物科技有限公司', '01D339', '锐创微矿', '销售发货', '2000', '10.60', '21200.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1250', '2023-04-07', '黄坤', '南通快诚生物科技有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '500', '12.50', '6250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1251', '2023-04-07', '黄坤', '徐州中远生物科技有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '12.30', '12300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1252', '2023-04-07', '黄坤', '山西鲸航生物科技有限公司', '01D324', '微速强', '销售发货', '1000', '10.80', '10800.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1253', '2023-04-07', '曲光', '邹平市永强牧业有限公司', '01D274', '奇力宝（蛋禽用）', '销售发货', '1000', '14.00', '14000.00', '市场销售二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1254', '2023-04-07', '杨小乐', '内蒙古博鑫农牧业科技有限公司（郑州嘉吉）', '01D616', '美多(反刍)', '销售发货', '50', '16.80', '840.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1255', '2023-04-07', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D402', '抗氧灵', '销售发货', '1000', '9.70', '9700.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1256', '2023-04-07', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D226', '奇力宝（母猪有机矿精）', '销售发货', '1000', '14.00', '14000.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1257', '2023-04-07', '鲁刚', '哈尔滨相成饲料制造有限公司', '01D214', '奇力宝(仔猪用)', '销售发货', '1000', '12.30', '12300.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1258', '2023-04-07', '罗辉', '嘉吉饲料（哈尔滨）有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '250', '18.00', '4500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1259', '2023-04-08', '谭永昌', '广州市猪王饲料有限公司', '01D211', '奇力宝猪用', '销售发货', '1000', '6.30', '6300.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1260', '2023-04-08', '张明', '阳江市旺海生物科技有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '200', '15.50', '3100.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1261', '2023-04-08', '张明', '阳江市旺海生物科技有限公司', '04D303', '奇力宝（海水鱼用）', '销售发货', '500', '14.80', '7400.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1262', '2023-04-08', '罗辉', '嘉吉饲料（宜春）有限公司', '04D061', '甘氨酸锌21%', '销售发货', '25000', '12.30', '307500.00', '市场管理部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1263', '2023-04-08', '何海涛', '大成万达(天津)有限公司', '01D005', '甘氨酸铁17%', '销售发货', '2000', '12.14', '24280.00', '市场销售一部', '九益', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1264', '2023-04-08', '何海涛', '辽宁波尔莱特农牧实业有限公司', '01D402', '抗氧灵', '销售发货', '5000', '13.50', '67500.00', '市场销售一部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1265', '2023-04-08', '何翔飞', '广州昭品饲料有限公司', '01E720', '肝美多', '销售发货', '25', '10.00', '250.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1266', '2023-04-08', '何翔飞', '佛山市南海禅泰动物药业有限公司', '01D042', '天锰乐（G/Mn-220）', '销售发货', '100', '25.00', '2500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1267', '2023-04-08', '王林', '广东丰信饲料有限公司', '04D310', '奇力宝（海水甲壳动物用）', '销售发货', '2000', '12.00', '24000.00', '营销事业二部', '有机宝', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1268', '2023-04-08', '黄坤', '徐州渔师傅生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '1000', '17.50', '17500.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1269', '2023-04-08', '黄坤', '扬州海丰生物科技有限公司', '01D317', '微速达（水产用）', '销售发货', '5000', '18.00', '90000.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1270', '2023-04-08', '黄坤', '江苏牧佳生物科技有限公司', '04D061', '甘氨酸锌21%', '销售发货', '500', '13.30', '6650.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1271', '2023-04-08', '黄坤', '江苏牧佳生物科技有限公司', '01D005', '甘氨酸铁17%', '销售发货', '1000', '7.90', '7900.00', '营销事业二部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `t_receivable_summary` VALUES ('1272', '2023-04-08', '杨小乐', '内蒙古博鑫农牧业科技有限公司（天津信华宏业）', '01D620', '美多（反刍动物用）', '销售发货', '1050', '16.90', '17745.00', '营销事业三部', '生物', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for t_sales_invoice
-- ----------------------------
DROP TABLE IF EXISTS `t_sales_invoice`;
CREATE TABLE `t_sales_invoice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `delivery_date` date NOT NULL COMMENT '发货日期',
  `salesman` varchar(50) NOT NULL COMMENT '业务员',
  `customer_name` varchar(100) NOT NULL COMMENT '客户全名',
  `document_type_code` varchar(50) NOT NULL COMMENT '单据类型代码',
  `product_code` varchar(50) NOT NULL COMMENT '产品代号',
  `product_name` varchar(100) NOT NULL COMMENT '名称',
  `delivery_quantity` int NOT NULL COMMENT '发货数量',
  `local_currency_price` decimal(10,2) NOT NULL COMMENT '本币含税单价',
  `local_currency_amount` decimal(10,2) NOT NULL COMMENT '本币含税金额',
  `region` varchar(50) DEFAULT NULL COMMENT '所属区域',
  `delivery_type_code` varchar(50) NOT NULL COMMENT '发货类型代码',
  `invoice_no` varchar(50) DEFAULT NULL COMMENT '发票号码',
  `invoice_date` date DEFAULT NULL COMMENT '开票日期',
  `send_time` date DEFAULT NULL COMMENT '发送时间',
  `is_tax_exempt` tinyint(1) DEFAULT '0' COMMENT '免税否',
  `invoice_tax_type_code` varchar(50) NOT NULL COMMENT '发票类型代码',
  `invoice_status_code` varchar(50) NOT NULL COMMENT '开票状态代码',
  `is_need_invoice` tinyint(1) DEFAULT '1' COMMENT '不需要开票',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `created_by` varchar(50) NOT NULL COMMENT '录入人',
  `created_at` datetime NOT NULL COMMENT '录入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of t_sales_invoice
-- ----------------------------
