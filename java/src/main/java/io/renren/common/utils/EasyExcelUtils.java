package io.renren.common.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

/**
 * @param <T>
 * @param <K>
 * @author yuanChen
 * @Date 2022-10-10
 * @Description 基于阿里巴巴easyExcel实现，实现基础常用功能，可根据需求参考文档进行优化
 *              功能：1.excel生成至本地 2.返回流供前端下载 3.读取本地excel文件 4.读取前端上传
 * 				excel文件
 *              参考文档https://easyexcel.opensource.alibaba.com/
 *              注意文件路径在windows下，“/”，“\”都可以识别，unix只识别“/”，所以路径的拼接，更推荐使用File.separator
 *              如：/excel/sampleData.xlsx 推荐使用 String path = File.separator + "excel" + File.separator + "sampleData.xlsx";
 */
@Component
public class EasyExcelUtils<T, K> {

        /**
         * 导出excel文件至浏览器下载
         *
         * @param list             list格式的数据源，模板中使用{.}
         * @param map              map格式的数据源，模板中使用{}，与list相比少了.
         * @param response         相应浏览器的请求
         * @param formWorkPathName 模板文件的路径 相对于resource文件夹，/excel/sampleData.xlsx，resource文件下excel文件夹下的sampleData文件
         * @param outPutFileName   输出的文件名
         */
        public void exportExcelToBrowser(T list, K map, HttpServletResponse response, String formWorkPathName, String outPutFileName) {
            //模板文件编译后的位置
            InputStream path = this.getClass().getResourceAsStream(formWorkPathName);
            String filename = null;
            try {
                // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
                filename = URLEncoder.encode(outPutFileName + System.currentTimeMillis(), "utf-8");
                //ExcelWriter excelWriter = EasyExcel.write(filename).withTemplate(templateFileName).build();
                WriteSheet writeSheet = EasyExcel.writerSheet()
                        .build();
                //使用response.getOutputStream()下载,并使用项目下的模板填充
                ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream())
                        .withTemplate(path)
                        .build();
                //数据源，此处使用的方法为easyExcel的模板填充功能，与write不一样
                if (map != null) {
                    excelWriter.fill(map, writeSheet);//存入map
                }
                if (list != null) {
                    excelWriter.fill(list, writeSheet);//存入list
                }
                //下载
                response.setContentType("application/vnd.ms-excel");
                response.setCharacterEncoding("utf-8");
                response.setHeader("Content-disposition", "attachment;filename=" + filename + ".xlsx");
                //必须要finish才会写入，不finish只会创建一个empty的文件。关闭外层流后，内层流也会关闭
                excelWriter.finish();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * 导出excel文件至本地
         * @param list list格式数据源，模板中使用{.}
         * @param map map格式数据源，模板中使用{}，与list相比少了.
         * @param formWorkPathName 模板文件的路径 相对于resource文件夹，/excel/sampleData.xlsx，resource文件下excel文件夹下的sampleData文件
         * @param outPutPathName 保存至本地的文件路径 D://excel/，注意末尾的”/“不要省略
         * @param outPutFileName 保存至本地的文件名
         */
        public void exportExcelToDisk(T list, K map, String formWorkPathName, String outPutPathName, String outPutFileName) {
            //模板文件编译后的位置
            InputStream path = this.getClass().getResourceAsStream(formWorkPathName);
            try {
                //excel文件保存至本地的路径
                FileOutputStream fileOutputStream = new FileOutputStream(outPutPathName + outPutFileName + System.currentTimeMillis() + ".xlsx");
                //writeSheet可以指定excel的哪一个sheet
                WriteSheet writeSheet = EasyExcel.writerSheet()
                        .build();
                //使用本地路径下载,并使用模板填充
                ExcelWriter excelWriter = EasyExcel.write(fileOutputStream)
                        .withTemplate(path)
                        .build();
                //数据源，此处使用的方法为easyExcel的模板填充功能，与write不一样
                if (map != null) {
                    excelWriter.fill(map, writeSheet);//存入map
                }
                if (list != null) {
                    excelWriter.fill(list, writeSheet);//存入list
                }
                //必须要finish才会写入，不finish只会创建一个empty的文件。关闭外层流后，内层流也会关闭
                excelWriter.finish();
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * 读取本地路径下的excel文件
         * 路径示例：D://excel/demoData.xlsx
         * @param filePathName 文件路径
         */
        public void readExcelFromDisk(String filePathName) {
          /*  // 这里 需要指定读用哪个class去读，然后默认读取第一个sheet 文件流会自动关闭
            // 直接new了监听器，lambda表达式进行数据后续处理
            EasyExcel.read(filePathName, DemoDataEntity.class, new PageReadListener<DemoDataEntity>(dataList -> {
                for (DemoDataEntity demoData : dataList) {
                    System.out.println(demoData);
                }
            })).sheet().doRead();//sheet()可指定读取哪一个sheet，支持名字和索引，索引起始为0*/

        }

        /**
         * 读取前端传输的excel文件
         *
         */
        public static void readExcelFromBrowser(MultipartFile file) {
       /*     try {
                InputStream excelFileInputStream = file.getInputStream();
                // 这里 需要指定读用哪个class去读，然后默认读取第一个sheet 文件流会自动关闭
                // 直接new了监听器，lambda表达式进行数据后续处理
                EasyExcel.read(excelFileInputStream, DemoDataEntity.class, new PageReadListener<DemoDataEntity>(dataList -> {
                    for (DemoDataEntity demoData : dataList) {
                        System.out.println(demoData);
                    }
                })).sheet().doRead();//sheet()可指定读取哪一个sheet，支持名字和索引，索引起始为0
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }
}
