package io.renren.modules.cw.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import io.renren.modules.cw.entity.InvoiceEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.cw.entity.DeliveryPaymentRecordEntity;
import io.renren.modules.cw.service.DeliveryPaymentRecordService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

import static io.renren.common.utils.ShiroUtils.getUserId;


/**
 * 发货回款记录表
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@RestController
@RequestMapping("cw/deliverypaymentrecord")
public class DeliveryPaymentRecordController {
    @Autowired
    private DeliveryPaymentRecordService deliveryPaymentRecordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cw:deliverypaymentrecord:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = deliveryPaymentRecordService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cw:deliverypaymentrecord:info")
    public R info(@PathVariable("id") Integer id){
		DeliveryPaymentRecordEntity deliveryPaymentRecord = deliveryPaymentRecordService.getById(id);

        return R.ok().put("deliveryPaymentRecord", deliveryPaymentRecord);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cw:deliverypaymentrecord:save")
    public R save(@RequestBody DeliveryPaymentRecordEntity deliveryPaymentRecord){
		deliveryPaymentRecordService.save(deliveryPaymentRecord);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cw:deliverypaymentrecord:update")
    public R update(@RequestBody DeliveryPaymentRecordEntity deliveryPaymentRecord){
		deliveryPaymentRecordService.updateById(deliveryPaymentRecord);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cw:deliverypaymentrecord:delete")
    public R delete(@RequestBody Integer[] ids){
		deliveryPaymentRecordService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }


    @PostMapping("/readFromExcel")
    public R importSave(@RequestParam("file") MultipartFile file) {
        try {
            InputStream excelFileInputStream = file.getInputStream();
            // 这里 需要指定读用哪个class去读，然后默认读取第一个sheet 文件流会自动关闭
            // 直接new了监听器，lambda表达式进行数据后续处理
            EasyExcel.read(excelFileInputStream, DeliveryPaymentRecordEntity.class, new PageReadListener<DeliveryPaymentRecordEntity>(dataList -> {
                for (DeliveryPaymentRecordEntity deliveryPaymentRecord : dataList) {
                    String userId = getUserId().toString();
                    deliveryPaymentRecord.setCreatedBy(userId);
                    deliveryPaymentRecord.setCreatedAt(new Date());
                    deliveryPaymentRecordService.save(deliveryPaymentRecord);
                }
            })).sheet().doRead();//sheet()可指定读取哪一个sheet，支持名字和索引，索引起始为0
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok();
    }
}
