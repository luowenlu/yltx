package io.renren.modules.cw.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import io.renren.modules.cw.entity.DeliveryPaymentRecordEntity;
import io.renren.modules.cw.entity.InvoiceEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.cw.entity.ReceiptsEntity;
import io.renren.modules.cw.service.ReceiptsService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

import static io.renren.common.utils.ShiroUtils.getUserId;


/**
 * 回款冲账表
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@RestController
@RequestMapping("cw/receipts")
public class ReceiptsController {
    @Autowired
    private ReceiptsService receiptsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cw:receipts:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = receiptsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{receiptId}")
    @RequiresPermissions("cw:receipts:info")
    public R info(@PathVariable("receiptId") Integer receiptId){
		ReceiptsEntity receipts = receiptsService.getById(receiptId);

        return R.ok().put("receipts", receipts);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cw:receipts:save")
    public R save(@RequestBody ReceiptsEntity receipts){
		receiptsService.save(receipts);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cw:receipts:update")
    public R update(@RequestBody ReceiptsEntity receipts){
		receiptsService.updateById(receipts);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cw:receipts:delete")
    public R delete(@RequestBody Integer[] receiptIds){
		receiptsService.removeByIds(Arrays.asList(receiptIds));

        return R.ok();
    }
    @PostMapping("/readFromExcel")
    public R importSave(@RequestParam("file") MultipartFile file) {
        try {
            InputStream excelFileInputStream = file.getInputStream();
            // 这里 需要指定读用哪个class去读，然后默认读取第一个sheet 文件流会自动关闭
            // 直接new了监听器，lambda表达式进行数据后续处理
            EasyExcel.read(excelFileInputStream, ReceiptsEntity.class, new PageReadListener<ReceiptsEntity>(dataList -> {
                for (ReceiptsEntity receiptsEntity : dataList) {
                    String userId = getUserId().toString();
                    receiptsEntity.setCreatedBy(userId);
                    //receiptsEntity.setCreatedAt(new Date());
                    receiptsService.save(receiptsEntity);
                }
            })).sheet().doRead();//sheet()可指定读取哪一个sheet，支持名字和索引，索引起始为0
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok();
    }

}
