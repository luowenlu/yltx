package io.renren.modules.cw.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import io.renren.modules.cw.entity.InvoiceEntity;
import io.renren.modules.cw.entity.ReceivableSummaryDTO;
import io.renren.modules.cw.entity.SumEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.cw.entity.ReceivableSummaryEntity;
import io.renren.modules.cw.service.ReceivableSummaryService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

import static io.renren.common.utils.ShiroUtils.getUserId;


/**
 * 应收汇总表
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-15 17:16:10
 */
@RestController
@RequestMapping("cw/receivablesummary")
public class ReceivableSummaryController {
    @Autowired
    private ReceivableSummaryService receivableSummaryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cw:receivablesummary:list")
    public R list(@RequestParam Map<String, Object> params){
        ReceivableSummaryEntity receivableSummaryEntity = BeanUtil.fillBeanWithMap(params, new ReceivableSummaryEntity(), true);
        PageUtils page = receivableSummaryService.queryPage(params);
        SumEntity sum = receivableSummaryService.getSum(receivableSummaryEntity);
        HashMap map = new HashMap<>();
        map.put("page", page);
        map.put("sum", sum);
        return R.ok(map);
    }


    /**
     * 列表
     */
    @RequestMapping("/alllist")
    @RequiresPermissions("cw:receivablesummary:list")
    public R alllist(@RequestParam Map<String, Object> params){
        List<ReceivableSummaryEntity> list = receivableSummaryService.list();
        return R.ok().put("list", list);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cw:receivablesummary:info")
    public R info(@PathVariable("id") Integer id){
		ReceivableSummaryEntity receivableSummary = receivableSummaryService.getById(id);

        return R.ok().put("receivableSummary", receivableSummary);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cw:receivablesummary:save")
    public R save(@RequestBody ReceivableSummaryEntity receivableSummary){
		receivableSummaryService.save(receivableSummary);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cw:receivablesummary:update")
    public R update(@RequestBody ReceivableSummaryEntity receivableSummary){
		receivableSummaryService.updateById(receivableSummary);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cw:receivablesummary:delete")
    public R delete(@RequestBody Integer[] ids){
		receivableSummaryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    @PostMapping("/readFromExcel")
    public R importSave(@RequestParam("file") MultipartFile file) {
        try {
            InputStream excelFileInputStream = file.getInputStream();
            // 这里 需要指定读用哪个class去读，然后默认读取第一个sheet 文件流会自动关闭
            // 直接new了监听器，lambda表达式进行数据后续处理
           // List<ReceivableSummaryDTO> list = EasyExcel.read(excelFileInputStream, ReceivableSummaryDTO.class, null).sheet(0).doReadSync();

            EasyExcel.read(excelFileInputStream, ReceivableSummaryDTO.class, new PageReadListener<ReceivableSummaryDTO>(dataList -> {
                for (ReceivableSummaryDTO receivableSummaryDTO : dataList) {
                    ReceivableSummaryEntity receivableSummary = new ReceivableSummaryEntity();
                    BeanUtils.copyProperties(receivableSummaryDTO,receivableSummary);
                    String userId = getUserId().toString();
                    //receivableSummary.setCreatedBy(userId);
                    //receivableSummary.setCreatedAt(new Date());
                    receivableSummaryService.save(receivableSummary);
                }
            })).sheet(0).doRead();//sheet()可指定读取哪一个sheet，支持名字和索引，索引起始为0
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok();
    }

}
