package io.renren.modules.cw.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import io.renren.modules.cw.entity.ReceiptsEntity;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.cw.entity.SalesInvoiceEntity;
import io.renren.modules.cw.service.SalesInvoiceService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import org.springframework.web.multipart.MultipartFile;

import static io.renren.common.utils.ShiroUtils.getUserId;


/**
 * 
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@RestController
@RequestMapping("cw/salesinvoice")
public class SalesInvoiceController {
    @Autowired
    private SalesInvoiceService salesInvoiceService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("cw:salesinvoice:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = salesInvoiceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("cw:salesinvoice:info")
    public R info(@PathVariable("id") Integer id){
		SalesInvoiceEntity salesInvoice = salesInvoiceService.getById(id);

        return R.ok().put("salesInvoice", salesInvoice);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("cw:salesinvoice:save")
    public R save(@RequestBody SalesInvoiceEntity salesInvoice){
		salesInvoiceService.save(salesInvoice);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("cw:salesinvoice:update")
    public R update(@RequestBody SalesInvoiceEntity salesInvoice){
		salesInvoiceService.updateById(salesInvoice);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("cw:salesinvoice:delete")
    public R delete(@RequestBody Integer[] ids){
		salesInvoiceService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }
    @PostMapping("/readFromExcel")
    public R importSave(@RequestParam("file") MultipartFile file) {
        try {
            InputStream excelFileInputStream = file.getInputStream();
            // 这里 需要指定读用哪个class去读，然后默认读取第一个sheet 文件流会自动关闭
            // 直接new了监听器，lambda表达式进行数据后续处理
            EasyExcel.read(excelFileInputStream, SalesInvoiceEntity.class, new PageReadListener<SalesInvoiceEntity>(dataList -> {
                for (SalesInvoiceEntity salesInvoice : dataList) {
                    String userId = getUserId().toString();
                    salesInvoice.setCreatedBy(userId);
                    //receiptsEntity.setCreatedAt(new Date());
                    salesInvoiceService.save(salesInvoice);
                }
            })).sheet().doRead();//sheet()可指定读取哪一个sheet，支持名字和索引，索引起始为0
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok();
    }

}
