package io.renren.modules.cw.dao;

import io.renren.modules.cw.entity.DeliveryPaymentRecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 发货回款记录表
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@Mapper
public interface DeliveryPaymentRecordDao extends BaseMapper<DeliveryPaymentRecordEntity> {
	
}
