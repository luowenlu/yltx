package io.renren.modules.cw.dao;

import io.renren.modules.cw.entity.InvoiceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-03-26 07:46:08
 */
@Mapper
public interface InvoiceDao extends BaseMapper<InvoiceEntity> {
	
}
