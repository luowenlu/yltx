package io.renren.modules.cw.dao;

import io.renren.modules.cw.entity.ReceivableSummaryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.cw.entity.SumEntity;
import io.renren.modules.sys.entity.SysUserTokenEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 应收汇总表
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-15 17:16:10
 */
@Mapper
public interface ReceivableSummaryDao extends BaseMapper<ReceivableSummaryEntity> {

	SumEntity getSum(ReceivableSummaryEntity receivableSummaryEntity);
}
