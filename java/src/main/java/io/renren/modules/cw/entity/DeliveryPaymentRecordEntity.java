package io.renren.modules.cw.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 发货回款记录表
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@Data
@TableName("t_delivery_payment_record")
public class DeliveryPaymentRecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId
	private Integer id;
	/**
	 * 发货日期
	 */
	private Date deliveryDate;
	/**
	 * 业务员
	 */
	private String salesperson;
	/**
	 * 客户名称
	 */
	private String customerName;
	/**
	 * 销售发货/退货
	 */
	private String salesDeliveryReturn;
	/**
	 * 编号
	 */
	private String number;
	/**
	 * 产品名称
	 */
	private String productName;
	/**
	 * 数量
	 */
	private Integer quantity;
	/**
	 * 单价
	 */
	private Float unitPrice;
	/**
	 * 金额（开票）
	 */
	private Float invoiceAmount;
	/**
	 * 所属区域
	 */
	private String region;
	/**
	 * 开票公司
	 */
	private String invoiceCompany;
	/**
	 * 结算价
	 */
	private Float settlementPrice;
	/**
	 * 指导价
	 */
	private Float guidePrice;
	/**
	 * 到期时间
	 */
	private Date expireDate;
	/**
	 * 超期天数
	 */
	private Integer overdueDays;
	/**
	 * 回款银行
	 */
	private String paymentBank;
	/**
	 * 回款日期
	 */
	private Date paymentDate;
	/**
	 * 对公回款金额
	 */
	private Float publicPaymentAmount;
	/**
	 * 单位应收款
	 */
	private Float unitReceivable;
	/**
	 * 回款日期（个人其他）
	 */
	private Date personalPaymentDate;
	/**
	 * 单位应收（个人）
	 */
	private Float personalUnitReceivable;
	/**
	 * 回款日期（业务员）
	 */
	private Date salespersonPaymentDate;
	/**
	 * 单位应收(业务员）
	 */
	private Float salespersonUnitReceivable;
	/**
	 * 汇率
	 */
	private Float exchangeRate;
	/**
	 * 美元
	 */
	private Float usd;
	/**
	 * 汇兑
	 */
	private Float exchangeRateConversion;
	/**
	 * 录入人
	 */
	private String createdBy;
	/**
	 * 录入时间
	 */
	private Date createdAt;
	/**
	 * 数据有效性
	 */
	private Integer isValid;

}
