package io.renren.modules.cw.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@Data
@TableName("t_dictionary")
public class DictionaryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 字典名称
	 */
	private String dictName;
	/**
	 * 代码
	 */
	private String code;
	/**
	 * 值
	 */
	private String value;
	/**
	 * 创建时间
	 */
	private Date createdAt;

}
