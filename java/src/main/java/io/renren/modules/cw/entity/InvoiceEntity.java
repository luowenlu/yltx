package io.renren.modules.cw.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-03-26 07:46:08
 */
@Data
@TableName("t_invoice")
public class InvoiceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 发货日期
	 */
	@ExcelProperty(value = "发货日期",index=1)
	private String deliveryDate;
	/**
	 * 业务员
	 */
	@ExcelProperty(value = "业务员",index=2)
	private String salesman;
	/**
	 * 客户全名
	 */
	@ExcelProperty(value = "客户全名",index=3)
	private String customerName;
	/**
	 * 单据类型
	 */
	@ExcelProperty(value = "发货/退货",index=4)
	private String documentType;
	/**
	 * 产品代号
	 */
	@ExcelProperty(value = "产品代号",index=5)
	private String productCode;
	/**
	 * 名称
	 */
	@ExcelProperty(value = "名称",index=6)
	private String productName;
	/**
	 * 发货数量
	 */
	@ExcelProperty(value = "发货数量",index=7)
	private String deliveryQuantity;
	/**
	 * 本币含税单价
	 */
	@ExcelProperty(value = "本币含税单价",index=8)
	private BigDecimal localCurrencyPrice;
	/**
	 * 本币含税金额
	 */
	@ExcelProperty(value = "本币含税金额",index=9)
	private BigDecimal localCurrencyAmount;
	/**
	 * 所属区域
	 */
	@ExcelProperty(value = "所属区域",index=10)
	private String region;
	/**
	 * 发货类型
	 */
	@ExcelProperty(value = "发货类型",index=11)
	private String deliveryType;
	/**
	 * 结算价
	 */
	@ExcelProperty(value = "结算价",index=12)
	private BigDecimal settlementPrice;
	/**
	 * 录入人
	 */
	private String createdBy;
	/**
	 * 录入时间
	 */
	private Date createdAt;
	/**
	 * 数据有效性
	 */
	private Integer isValid;

}
