package io.renren.modules.cw.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 回款冲账表
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@Data
@TableName("t_receipts")
public class ReceiptsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 回款ID
	 */
	@TableId
	private Integer receiptId;
	/**
	 * 回款日期
	 */
	private Date receiptDate;
	/**
	 * 业务员
	 */
	private String salesman;
	/**
	 * 汇款人
	 */
	private String remitter;
	/**
	 * 客户名称
	 */
	private String customerName;
	/**
	 * 当月回款
	 */
	private BigDecimal monthlyReceipt;
	/**
	 * 本年累计回款
	 */
	private BigDecimal yearToDateReceipt;
	/**
	 * 回款银行
	 */
	private String receiptBank;
	/**
	 * 待冲金额
	 */
	private BigDecimal toBeOffset;
	/**
	 * 上月待冲
	 */
	private BigDecimal lastMonthToBeOffset;
	/**
	 * 已冲金额
	 */
	private BigDecimal alreadyOffset;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 录入人
	 */
	private String createdBy;
	/**
	 * 录入时间
	 */
	private Date createdTime;
	/**
	 * 有效性验证
	 */
	private Integer isValid;

}
