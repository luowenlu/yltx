package io.renren.modules.cw.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 应收汇总表
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-15 17:16:10
 */
@Data
public class ReceivableSummaryDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 发货日期
	 */
	@ExcelProperty(value = "发货日期",index=0)
	private Date shippingDate;
	/**
	 * 业务员
	 */
	@ExcelProperty(value = "业务员",index=1)
	private String salesman;
	/**
	 * 客户名称
	 */
	@ExcelProperty(value = "客户名称",index=2)
	private String customerName;
	/**
	 * 单据类型
	 */
	@ExcelProperty(value = "发货/退货",index=3)
	private String documentType;
	/**
	 * 编号
	 */
	@ExcelProperty(value = "编号",index=4)
	private String number;
	/**
	 * 产品名称
	 */
	@ExcelProperty(value = "产品名称",index=5)
	private String productName;
	/**
	 * 数量
	 */
	@ExcelProperty(value = "数量",index=6)
	private Integer quantity;
	/**
	 * 单价
	 */
	@ExcelProperty(value = "单价",index=7)
	private BigDecimal unitPrice;
	/**
	 * 金额_开票
	 */
	@ExcelProperty(value = "金额（开票）",index=8)
	private BigDecimal invoiceAmount;
	/**
	 * 所属区域
	 */
	@ExcelProperty(value = "所属区域",index=9)
	private String region;
	/**
	 * 开票公司
	 */
	@ExcelProperty(value = "开票公司",index=10)
	private String invoiceCompany;

}
