package io.renren.modules.cw.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 应收汇总表
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-15 17:16:10
 */
@Data
@TableName("t_receivable_summary")
public class ReceivableSummaryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId
	private Integer id;
	/**
	 * 发货日期
	 */
	@ExcelProperty(value = "发货日期",index=1)
	private Date shippingDate;
	/**
	 * 业务员
	 */
	@ExcelProperty(value = "业务员",index=2)
	private String salesman;
	/**
	 * 客户名称
	 */
	@ExcelProperty(value = "客户名称",index=3)
	private String customerName;
	/**
	 * 单据类型
	 */
	@ExcelProperty(value = "发货/退货",index=4)
	private String documentType;
	/**
	 * 编号
	 */
	@ExcelProperty(value = "编号",index=5)
	private String number;
	/**
	 * 产品名称
	 */
	@ExcelProperty(value = "产品名称",index=6)
	private String productName;
	/**
	 * 数量
	 */
	@ExcelProperty(value = "数量",index=7)
	private Integer quantity;
	/**
	 * 单价
	 */
	@ExcelProperty(value = "单价",index=8)
	private BigDecimal unitPrice;
	/**
	 * 金额_开票
	 */
	@ExcelProperty(value = "金额（开票）",index=9)
	private BigDecimal invoiceAmount;
	/**
	 * 所属区域
	 */
	@ExcelProperty(value = "所属区域",index=10)
	private String region;
	/**
	 * 开票公司
	 */
	@ExcelProperty(value = "开票公司",index=11)
	private String invoiceCompany;
	/**
	 * 结算价
	 */
	private BigDecimal settlementPrice;
	/**
	 * 指导价
	 */
	private BigDecimal guidePrice;
	/**
	 * 提成
	 */
	private BigDecimal commission;
	/**
	 * 到期时间
	 */
	private Date dueDate;
	/**
	 * 超期天数
	 */
	private Integer overdueDays;
	/**
	 * 回款银行
	 */
	private String bank;
	/**
	 * 回款日期
	 */
	private Date paymentDate;
	/**
	 * 对公回款金额
	 */
	private BigDecimal publicPaymentAmount;
	/**
	 * 单位应收款
	 */
	private BigDecimal unitReceivable;
	/**
	 * 业务员回款日期
	 */
	private Date salesmanPaymentDate;
	/**
	 * 个人其他
	 */
	private String personalOthers;
	/**
	 * 业务员回款
	 */
	private BigDecimal salesmanPayment;
	/**
	 * 业务员应收
	 */
	private BigDecimal salesmanReceivable;
	/**
	 * 业务员代垫
	 */
	private BigDecimal salesmanAdvance;
	/**
	 * 冲账金额
	 */
	private BigDecimal chargeAmount;
	/**
	 * 代垫日期
	 */
	private Date advanceDate;
	/**
	 * 汇率
	 */
	private BigDecimal exchangeRate;
	/**
	 * 汇兑
	 */
	private BigDecimal exchange;
	/**
	 * 备注
	 */
	private String remark;

}
