package io.renren.modules.cw.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
@Data
@TableName("t_sales_invoice")
public class SalesInvoiceEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 发货日期
	 */
	private Date deliveryDate;
	/**
	 * 业务员
	 */
	private String salesman;
	/**
	 * 客户全名
	 */
	private String customerName;
	/**
	 * 单据类型代码
	 */
	private String documentTypeCode;
	/**
	 * 产品代号
	 */
	private String productCode;
	/**
	 * 名称
	 */
	private String productName;
	/**
	 * 发货数量
	 */
	private Integer deliveryQuantity;
	/**
	 * 本币含税单价
	 */
	private BigDecimal localCurrencyPrice;
	/**
	 * 本币含税金额
	 */
	private BigDecimal localCurrencyAmount;
	/**
	 * 所属区域
	 */
	private String region;
	/**
	 * 发货类型代码
	 */
	private String deliveryTypeCode;
	/**
	 * 发票号码
	 */
	private String invoiceNo;
	/**
	 * 开票日期
	 */
	private Date invoiceDate;
	/**
	 * 发送时间
	 */
	private Date sendTime;
	/**
	 * 免税否
	 */
	private Integer isTaxExempt;
	/**
	 * 发票类型代码
	 */
	private String invoiceTaxTypeCode;
	/**
	 * 开票状态代码
	 */
	private String invoiceStatusCode;
	/**
	 * 不需要开票
	 */
	private Integer isNeedInvoice;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 录入人
	 */
	private String createdBy;
	/**
	 * 录入时间
	 */
	private Date createdAt;

}
