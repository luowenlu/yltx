package io.renren.modules.cw.entity;

import lombok.Data;

@Data
public class SumEntity {
    private String quantitySum;

    private String unitPriceSum;

    private String invoiceAmountSum;


}
