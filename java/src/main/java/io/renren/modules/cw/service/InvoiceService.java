package io.renren.modules.cw.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.cw.entity.InvoiceEntity;

import java.util.Map;

/**
 * 
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-03-26 07:46:08
 */
public interface InvoiceService extends IService<InvoiceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

