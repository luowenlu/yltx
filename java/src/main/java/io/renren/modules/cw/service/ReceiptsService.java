package io.renren.modules.cw.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.cw.entity.ReceiptsEntity;

import java.util.Map;

/**
 * 回款冲账表
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
public interface ReceiptsService extends IService<ReceiptsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

