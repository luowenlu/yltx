package io.renren.modules.cw.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.cw.entity.ReceivableSummaryEntity;
import io.renren.modules.cw.entity.SumEntity;

import java.util.Map;

/**
 * 应收汇总表
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-15 17:16:10
 */
public interface ReceivableSummaryService extends IService<ReceivableSummaryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /***
     * 获取总数
     * @param receivableSummaryEntity
     * @return
     */
    SumEntity getSum(ReceivableSummaryEntity receivableSummaryEntity);
}

