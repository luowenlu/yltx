package io.renren.modules.cw.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.cw.entity.SalesInvoiceEntity;

import java.util.Map;

/**
 * 
 *
 * @author luowenlu
 * @email luowenlu@163.com
 * @date 2023-04-05 11:57:10
 */
public interface SalesInvoiceService extends IService<SalesInvoiceEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

