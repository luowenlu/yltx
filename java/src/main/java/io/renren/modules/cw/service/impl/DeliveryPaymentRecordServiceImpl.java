package io.renren.modules.cw.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.cw.dao.DeliveryPaymentRecordDao;
import io.renren.modules.cw.entity.DeliveryPaymentRecordEntity;
import io.renren.modules.cw.service.DeliveryPaymentRecordService;


@Service("deliveryPaymentRecordService")
public class DeliveryPaymentRecordServiceImpl extends ServiceImpl<DeliveryPaymentRecordDao, DeliveryPaymentRecordEntity> implements DeliveryPaymentRecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DeliveryPaymentRecordEntity> page = this.page(
                new Query<DeliveryPaymentRecordEntity>().getPage(params),
                new QueryWrapper<DeliveryPaymentRecordEntity>()
        );

        return new PageUtils(page);
    }

}