package io.renren.modules.cw.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.cw.dao.InvoiceDao;
import io.renren.modules.cw.entity.InvoiceEntity;
import io.renren.modules.cw.service.InvoiceService;


@Service("invoiceService")
public class InvoiceServiceImpl extends ServiceImpl<InvoiceDao, InvoiceEntity> implements InvoiceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String salesman =  (String)params.get("salesman");

        IPage<InvoiceEntity> page = this.page(
                new Query<InvoiceEntity>().getPage(params),
                new QueryWrapper<InvoiceEntity>().eq(StringUtils.isNotBlank(salesman),"salesman",salesman)
        );

        return new PageUtils(page);
    }

}