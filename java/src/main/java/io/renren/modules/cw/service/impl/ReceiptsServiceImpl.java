package io.renren.modules.cw.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.cw.dao.ReceiptsDao;
import io.renren.modules.cw.entity.ReceiptsEntity;
import io.renren.modules.cw.service.ReceiptsService;


@Service("receiptsService")
public class ReceiptsServiceImpl extends ServiceImpl<ReceiptsDao, ReceiptsEntity> implements ReceiptsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ReceiptsEntity> page = this.page(
                new Query<ReceiptsEntity>().getPage(params),
                new QueryWrapper<ReceiptsEntity>()
        );

        return new PageUtils(page);
    }

}