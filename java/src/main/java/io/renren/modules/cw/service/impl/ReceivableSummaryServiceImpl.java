package io.renren.modules.cw.service.impl;

import cn.hutool.core.bean.BeanUtil;
import io.renren.modules.cw.entity.SumEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.cw.dao.ReceivableSummaryDao;
import io.renren.modules.cw.entity.ReceivableSummaryEntity;
import io.renren.modules.cw.service.ReceivableSummaryService;


@Service("receivableSummaryService")
public class ReceivableSummaryServiceImpl extends ServiceImpl<ReceivableSummaryDao, ReceivableSummaryEntity> implements ReceivableSummaryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String shippingDate =  (String)params.get("shippingDate");
        String salesman =  (String)params.get("salesman");
        String customerName =  (String)params.get("customerName");
        String documentType =  (String)params.get("documentType");
        String number =  (String)params.get("number");
        String productName =  (String)params.get("productName");
        String region =  (String)params.get("region");
        String invoiceCompany =  (String)params.get("invoiceCompany");
        QueryWrapper<ReceivableSummaryEntity> invoiceEntityQueryWrapper = new QueryWrapper<>();
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(shippingDate),"shippingDate",shippingDate);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(salesman),"salesman",salesman);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(customerName),"customerName",customerName);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(documentType),"document_type",documentType);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(number),"number",number);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(productName),"productName",productName);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(region),"region",region);
        invoiceEntityQueryWrapper.eq(StringUtils.isNotBlank(invoiceCompany),"invoiceCompany",invoiceCompany);

        IPage<ReceivableSummaryEntity> page = this.page(
                new Query<ReceivableSummaryEntity>().getPage(params),
                invoiceEntityQueryWrapper
        );
        return new PageUtils(page);
    }

    public SumEntity getSum(ReceivableSummaryEntity receivableSummaryEntity){
        return  baseMapper.getSum(receivableSummaryEntity);
    };

}