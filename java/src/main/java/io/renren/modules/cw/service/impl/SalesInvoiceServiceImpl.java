package io.renren.modules.cw.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.cw.dao.SalesInvoiceDao;
import io.renren.modules.cw.entity.SalesInvoiceEntity;
import io.renren.modules.cw.service.SalesInvoiceService;


@Service("salesInvoiceService")
public class SalesInvoiceServiceImpl extends ServiceImpl<SalesInvoiceDao, SalesInvoiceEntity> implements SalesInvoiceService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SalesInvoiceEntity> page = this.page(
                new Query<SalesInvoiceEntity>().getPage(params),
                new QueryWrapper<SalesInvoiceEntity>()
        );

        return new PageUtils(page);
    }

}